//
//  DeepLinkViewController.m
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 5/14/18.
//  Copyright © 2018 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "DeepLinkViewController.h"
#import "HomeViewController.h"

@interface DeepLinkViewController ()

@end

@implementation DeepLinkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToViewControllerForCurrentSession) name:@"userLoadedAfterDeepLink" object:nil];
//    _currentSession = [User currentUser].currentSession;
    
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    self.navigationController.navigationBarHidden = NO;
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
//    [self goToViewControllerForCurrentSession];
    
}

-(void)goToViewControllerForCurrentSession {
    _currentSession = [User currentUser].currentSession;
    
    NSLog(@"_currentSession deep link view controller:%@", _currentSession);
    [self.navigationController setViewControllers:[self arrayOfViewControllersForContinueTimesheetNumber:_currentSession.continueTimesheetNumber andSelectedService:_currentSession.service.serviceName] animated:YES];
}


-(NSArray *)arrayOfViewControllersForContinueTimesheetNumber:(NSNumber *)continueTimesheetNumber andSelectedService:(NSString *)selectedService {
    NSMutableArray *mutArr = [NSMutableArray arrayWithObjects:[LoginViewController new], [HomeViewController new], nil];
    UIBarButtonItem *blankBack = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    NSLog(@"array of vc %@, selected service %@", continueTimesheetNumber, selectedService);
    
    long contNumber = [continueTimesheetNumber longValue];
    if (contNumber >= 0 || contNumber >= 1) {
        [mutArr addObject:[SelectServiceViewController new]];
    }
    if (contNumber >= 2) {
        [mutArr addObject:[RatioSharedCareViewController new]];
    }
    if (contNumber >= 3) {
        [mutArr addObject:[ChooseUserViewController new]];
    }
    if (contNumber >= 4) {
        [mutArr addObject:[TimeInAndOutViewController new]];
    }
    if (contNumber >= 5 && ([self string:selectedService containsCaseInsensitiveString:@"Personal Care Service"] || [self string:selectedService containsCaseInsensitiveString:@"Homemaking"])) {
        [mutArr addObject:[ActivityInfoViewController new]];
        if (contNumber >= 6) {
            [mutArr addObject:[PDFPreviewViewController new]];
        }
    } else if (contNumber >= 5) {
        [mutArr addObject:[PDFPreviewViewController new]];
    }
    
    for (UIViewController *vc in mutArr) {
        if ([vc isKindOfClass:[UIViewController class]]) {
            vc.navigationItem.backBarButtonItem = blankBack;
        }
    }
    return mutArr.copy;
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        return NO;
    }
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
