//
//  ForgotPasswordViewController.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 5/10/18.
//  Copyright © 2018 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController <UITextFieldDelegate, UIWebViewDelegate> {
    UITextField *emailTextfield;
    bool isIpad;
    bool four;
    float width;
    float height;
    UIButton *nextScreenButton;

}


@end
