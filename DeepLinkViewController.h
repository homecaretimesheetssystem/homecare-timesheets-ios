//
//  DeepLinkViewController.h
//  Homecare Timesheet Demo
//
//  Created by Joey Kjornes on 5/14/18.
//  Copyright © 2018 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectServiceViewController.h"
#import "RatioSharedCareViewController.h"
#import "TimeInAndOutViewController.h"
#import "ChooseUserViewController.h"
#import "ActivityInfoViewController.h"
#import "PDFPreviewViewController.h"


@interface DeepLinkViewController : UIViewController

@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) User *currentUser;

@property (nonatomic, strong) NSNumber *contTimesheetNumber;
@property (nonatomic, strong) NSString *contTimesheetServiceName;


@end
