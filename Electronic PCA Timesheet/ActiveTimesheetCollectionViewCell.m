//
//  ActiveTimesheetCollectionViewCell.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/27/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "ActiveTimesheetCollectionViewCell.h"

@implementation ActiveTimesheetCollectionViewCell

-(void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor whiteColor];
}

-(void)setValuesForUI {
    _currentUser = [User currentUser];
//    NSLog(@"_currentUser.userType:%@", _currentUser);
//    NSLog(@"_currentSession.pca:%@", _currentSession.pca);
    if (_currentUser.userType == UserType_IsRecipient) {
        _nameLabel.text = [NSString stringWithFormat:@"Worker: %@ %@", _currentSession.pca.firstName, _currentSession.pca.lastName];
        _dateLabel.text = [self dateEndingString];
        
    } else {
        _nameLabel.text = [NSString stringWithFormat:@"Recipient: %@ %@", _currentSession.currentTimecard.recipient.firstName, _currentSession.currentTimecard.recipient.lastName];
        if (_currentSession.dateEnding) {
            _dateLabel.text = [self dateEndingString];
        } else {
            _dateLabel.text = _currentSession.service.serviceName;
        }
    }
    _timeInLabel.text = [NSString stringWithFormat:@"Time in: %@", _currentSession.timeIn.timeIn ? [[self shortTimeOnlyDateFormatter] stringFromDate:_currentSession.timeIn.timeIn] : @"Not entered"];
    _timeOutLabel.text = [NSString stringWithFormat:@"Time out: %@", _currentSession.timeOut.timeOut ? [[self shortTimeOnlyDateFormatter] stringFromDate:_currentSession.timeOut.timeOut] : @"Not entered"];
    _totalTimeLabel.text = [NSString stringWithFormat:@"Total: %@", _currentSession.timeOut.timeOut ? [self convertSecondsToString:_currentSession.timeOut.billableHours] : @""];
//    [_webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
    [_webView loadRequest:[NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:[self pdfNameForService:_currentSession.service.serviceName] withExtension:@"pdf"]]];
    _webView.userInteractionEnabled = NO;
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        //        NSLog(@"return NO");
        return NO;
    }
    return YES;
}

-(NSString *)pdfNameForService:(NSString *)service
{
    if ([self string:service containsCaseInsensitiveString:@"Personal Care Service"]) {
        return @"PCA";
    } else if ([self string:service containsCaseInsensitiveString:@"Homemaking"]) {
        return @"Homemaking";
    } else if ([self string:service containsCaseInsensitiveString:@"Respite"]) {
        return @"Respite";
    } else if ([self string:service containsCaseInsensitiveString:@"Personal Assistance"]) {
        return @"Personal Assistance";
    } else if ([self string:service containsCaseInsensitiveString:@"Personal Support"]) {
        return @"Personal Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Self-Direction Support"]) {
        return @"Self-Direction Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Treatment and Training"]) {
        return @"Treatment and Training";
    } else if ([self string:service containsCaseInsensitiveString:@"Caregiving Expense"]) {
        return @"Caregiving Expense";
    } else if ([self string:service containsCaseInsensitiveString:@"Consumer Support"]) {
        return @"Consumer Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Environmental Modifications"]) {
        return @"Environmental Modifications";
    }
    return @"Time and Activity 12.2.15";
}

-(NSString *)convertSecondsToString:(NSNumber *)totalSeconds {
    long hours, minutes;
    long accumulatedSeconds = [totalSeconds doubleValue];
    hours = accumulatedSeconds/3600;
    minutes = (accumulatedSeconds % 3600) / 60;
    return [NSString stringWithFormat:@"%2ld:%02ld", hours, minutes];
}

-(NSString *)dateEndingString {
    //        NSLog(@"pdf dateEnding:%@", currentSession.dateEnding);
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    NSDate *stringDate = [dateformat dateFromString:_currentSession.dateEnding];
    //        NSLog(@"dateEnding:%@", currentSession.dateEnding);
    
    //        NSLog(@"stringDate:%@", stringDate);
    NSDateFormatter *shortFormat = [NSDateFormatter new];
    [shortFormat setDateStyle:NSDateFormatterShortStyle];
    [shortFormat setTimeStyle:NSDateFormatterNoStyle];
    NSString *dateString = [shortFormat stringFromDate:stringDate];
    //        NSLog(@"dateString:%@", dateString);
    if (!stringDate) {
        return _currentSession.dateEnding;
    } else {
        return dateString;
    }
    
}

-(NSDateFormatter *)shortTimeOnlyDateFormatter {
    NSDateFormatter *shortFormatter = [[NSDateFormatter alloc] init];
    [shortFormatter setTimeStyle:NSDateFormatterShortStyle];
    return shortFormatter;
}

@end
