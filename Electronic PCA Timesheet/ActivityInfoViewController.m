//
//  ActivityInfoViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 11/20/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "ActivityInfoViewController.h"

@interface ActivityInfoViewController ()

@end

@implementation ActivityInfoViewController
@synthesize currentTimecard, currentSession;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _currentUser = [User currentUser];
    currentSession = _currentUser.currentSession;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
//    currentDay = [[Day alloc] init];
    currentTimecard = currentSession.currentTimecard;
//    NSLog(@"currentTimecard:%@", currentSession.currentTimecard);

    localCareOptionsArray = [currentSession.currentTimecard.careOptionsArray mutableCopy];
        if (!localCareOptionsArray) {
            localCareOptionsArray = [NSMutableArray new];
        }
    
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self createUI];
    self.navigationItem.backBarButtonItem.title = @"";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    long number = 6;
    currentSession.continueTimesheetNumber = [NSNumber numberWithLong:number - 1];
    [NetworkingHelper saveSessionDataWithSessionNumber:currentSession.idNumber andParams:@{@"continueTimesheetNumber":currentSession.continueTimesheetNumber} andsuccess:^(id responseObject) {
//            NSLog(@"responseObject:%@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"errorSavingSessionData:%@", error);
    }];
    
}

-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createUI {
    BOOL isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    if (isIpad) {
        titleLabel.frame = CGRectMake(0, 50, width, 100);
        titleLabel.font = [UIFont fontWithName:@"Helvetica" size:50];
    } else if ((MAX([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)) == 812.) {
        titleLabel.font = [UIFont fontWithName:@"Helvetica" size:28];
        titleLabel.frame = CGRectMake(5, 88, width - 10, 50);
    } else {
        titleLabel.frame = CGRectMake(5, 60, width - 10, 50);
        titleLabel.font = [UIFont fontWithName:@"Helvetica" size:28];
    }
    titleLabel.text = @"What did you do?";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:titleLabel];
    adls = [NSMutableArray new];
    iadls = [NSMutableArray new];
    
    if (currentSession.currentTimecard.recipient.CareOptionsArray.count > 0) {
        for (int i = 0; i < currentSession.currentTimecard.recipient.CareOptionsArray.count; i++) {
            CareOption *option = (CareOption *)currentSession.currentTimecard.recipient.CareOptionsArray[i];
            if (option.isIADL == 0) {
                [adls addObject:option];
            } else {
                [iadls addObject:option];
            }
        }
        
    }else{
        for (int i = 0; i < _currentUser.possibleCareOptions.count; i++) {
            CareOption *option = (CareOption *)_currentUser.possibleCareOptions[i];
            if (i < 10) {
                [adls addObject:option];
            } else {
                [iadls addObject:option];
            }
        }
    }
    UITableView *activityTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, titleLabel.frame.origin.y + titleLabel.frame.size.height, width, height - (titleLabel.frame.origin.y + titleLabel.frame.size.height)) style:UITableViewStyleGrouped];
    if (isIpad) {
        activityTableView.frame = CGRectMake(width/2 - (width/4), titleLabel.frame.origin.y + titleLabel.frame.size.height, width/2, height - (titleLabel.frame.origin.y + titleLabel.frame.size.height));
    }
    activityTableView.dataSource = self;
    activityTableView.delegate = self;
    [self.view addSubview:activityTableView];
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
    self.navigationItem.rightBarButtonItem = saveButton;
    if (localCareOptionsArray.count > 0) {
        saveButton.enabled = YES;
    } else {
        saveButton.enabled = NO;
    }
}

-(void)deleteExistingCareOptions {
    [NetworkingHelper deleteCareOptionsFromTimesheet:currentTimecard.idNumber andSuccess:^(id responseObject) {
        [self nextScreen];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self nextScreen];
    }];
}

-(void)nextScreen {
    NSLog(@"currentSession.currentTimecard.id:%@", currentSession.currentTimecard.idNumber);
    NSDictionary *params = @{@"timesheet":currentSession.currentTimecard.idNumber, @"careOptions":localCareOptionsArray};
//    NSLog(@"params:%@", params);
    LoadingView *lv = [[LoadingView alloc] initWithFrame:CGRectMake(0, 0, 150, 100) andText:@"Saving"];
    lv.center = self.view.center;
    [self.view addSubview:lv];
    [NetworkingHelper addCareOptionsToTimesheetWithParams:params andsuccess:^(id responseObject) {
        
        [lv hideActivityIndicator];
        NSLog(@"success saving the care options:%@", responseObject);
        PDFPreviewViewController *vc = [[PDFPreviewViewController alloc] init];
        currentTimecard.careOptionsArray = localCareOptionsArray;
        currentSession.currentTimecard = currentTimecard;
        _currentUser.currentSession = currentSession;
        [_currentUser save];
        [self.navigationController pushViewController:vc animated:YES];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failed to save the care options");
        [lv hideActivityIndicator];
    }];
    
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        //        NSLog(@"return NO");
        return NO;
    }
    return YES;
}

#pragma mark - UITableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self string:self.currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
        return 1;
    }
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if ([self string:self.currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
            return iadls.count;
        }
        return adls.count;
    } else {
        return iadls.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.frame = CGRectMake(15, 5, width - 30, 40);
    if (section == 0) {
        if ([self string:self.currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
            titleLabel.text = @"Instrumental Activities of Daily Living";
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
        } else {
            titleLabel.text = @"Activities of Daily Living";
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:20];
        }
    } else {
        titleLabel.text = @"Instrumental Activities of Daily Living";
        titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    }
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.minimumScaleFactor = 0.5;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.backgroundColor = [UIColor clearColor];
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, width, 40);
    [headerView addSubview:titleLabel];
    return headerView;
}

-(NSArray *)arrayForIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if ([self string:self.currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
            return iadls;
        } else {
            return adls;
        }
    }
    return iadls;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myCell"];
    cell.textLabel.text = ((CareOption *)[self arrayForIndexPath:indexPath][indexPath.row]).name;
    if ([localCareOptionsArray containsObject:((CareOption *)[self arrayForIndexPath:indexPath][indexPath.row]).idNumber]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.font = [UIFont systemFontOfSize:16.0];
    cell.selectionStyle = UITableViewCellStyleDefault;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (indexPath.section == 0) {
            CareOption *option = adls[indexPath.row];
            [localCareOptionsArray removeObject:option.idNumber];
        } else {
            CareOption *option = iadls[indexPath.row];
            [localCareOptionsArray removeObject:option.idNumber];
        }
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        if (indexPath.section == 0) {
            CareOption *option = adls[indexPath.row];
            [localCareOptionsArray addObject:option.idNumber];
        } else {
            CareOption *option = iadls[indexPath.row];
            [localCareOptionsArray addObject:option.idNumber];
        }
    }
    if (localCareOptionsArray.count > 0) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

@end
