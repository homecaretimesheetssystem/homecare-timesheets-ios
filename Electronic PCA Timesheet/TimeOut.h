//
//  TimeOut.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/27/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeOut : NSObject <NSCopying>

@property (strong, nonatomic) NSNumber *idNumber;
@property (strong, nonatomic) NSDate *timeOut;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSNumber *latitude;
@property (strong, nonatomic) NSNumber *longitude;
@property (strong, nonatomic) NSDate *pictureTimestamp;
@property (strong, nonatomic) NSNumber *totalHours;
@property (strong, nonatomic) NSNumber *billableHours;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
