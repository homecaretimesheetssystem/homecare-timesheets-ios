//
//  NetworkingHelper.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/26/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <AFOAuth2Manager/AFOAuth2Manager.h>

@interface NetworkingHelper : NSObject

@property (strong) AFOAuthCredential *credential;

/*
 *
 */
+(void)loginWithParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)resetPasswordWithParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)checkVerificationForCamera:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)setVerificationForCameraWithID:(NSNumber *)verifID andParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)saveVerificationPhotoWithVerificationID:(NSNumber *)verificationID andParams:(NSDictionary *)dictionary success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)getSessionDataWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


+(void)createNewTimesheetTableWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Saves to the timesheet
 */

+(void)saveTimesheetDataWithTimesheetID:(NSNumber *)timesheetID andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
/*
 * Create the SessionData Table
 */
+(void)createSessionDataTableWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Easily Save the continueTimesheetNumber, use this in view did appear.
 */
+(void)saveSessionDataWithSessionNumber:(NSNumber *)sessionNumber andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
/*
 * PATCH request to update the current Session
 */
+(void)saveTimesheetNumberWithTimesheetNumber:(NSNumber *)timesheetNumber andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Create empty file row.
 */
+(void)createSessionFileAndSuccess:(void(^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Save File to Server
 */
+(void)saveSessionFileWithFileIDNumber:(NSNumber *)fileID  andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Create empty file row.
 */
+(void)createFileAndSuccess:(void(^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Save File to Server
 */
+(void)saveFileWithFileIDNumber:(NSNumber *)fileID  andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Create Time In Table
 */

+(void)createTimeInTableWithSuccess:(void(^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Create Time Out Table
 */
+(void)createTimeOutTableWithSuccess:(void(^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Save Time In data to server
 */

+(void)saveTimeInWithTimeInIdNumber:(NSNumber *)timeInIdNumber andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Create and Save Verification Table
 */

+(void)createAndSaveVerificationWithParams:(NSDictionary *)params andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Save Time Out data to server
 */

+(void)saveTimeOutWithTimeOutIdNumber:(NSNumber *)timeOutIdNumber andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Save the care options for the currentTimecard
 */

+(void)addCareOptionsToTimesheetWithParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Delete the care options for the currentTimecard
 */

+(void)deleteCareOptionsFromTimesheet:(NSNumber *)timesheetID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Verify Recipient Password with server
 */

+(void)verifyRecipientPassword:(NSString *)password andPCAID:(NSNumber *)pcaID andRecipientID:(NSNumber *)recipientID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Create Table for PCA Signature
 */

+(void)createPCASignatureTableWithParams:(NSDictionary *)params andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Create Table for Recipient Signature
 */

+(void)createRecipientSignatureTableWithParams:(NSDictionary *)params andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Resets all session data, called when user completes all timesheets in a session
 */

+(void)finishSessionWithSessionID:(NSNumber *)sessionID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/*
 * Delete exisiting timecards cached in a session
 */

+(void)deleteTimecardsFromSessionWithID:(NSNumber*)sessionID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)addVerificatioinIDToTimesheetWithID:(NSNumber*)timesheetID andVerificationID:(NSNumber *)verificationID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(void)checkWorkerHoursForTheWeekWithWorkerID:(NSNumber *)workerID andEstimatedShiftLengthHours:(NSNumber *)hours andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+(NSString *)clientID;
+(void)setNetworkingHelperOAuthToken:(NSString *)token;

@end
