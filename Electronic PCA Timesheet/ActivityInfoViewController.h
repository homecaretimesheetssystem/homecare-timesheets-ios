//
//  ActivityInfoViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 11/20/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Timecard.h"
#import "AppDelegate.h"
#import "PDFPreviewViewController.h"
#import "CareOption.h"
@class Timecard,  Session;

@interface ActivityInfoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSCoding, UIGestureRecognizerDelegate> {
    bool four;
    long width;
    long height;
    NSMutableArray *adls;
    NSMutableArray *iadls;
    NSMutableArray *localCareOptionsArray;
    NSMutableArray *activitiesMutableArray;
    NSMutableArray *iadlMutableArray;
}

@property (nonatomic, strong) NSNumber *tag;
@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) Timecard *currentTimecard;



@end
