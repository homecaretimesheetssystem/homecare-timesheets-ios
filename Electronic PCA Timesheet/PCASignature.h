//
//  PCASignature.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/14/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCASignature : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSNumber *idNumber;
@property (nonatomic, strong) NSData *signatureImageData;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSDate *timeStamp;
@property (nonatomic, strong) NSDate *userImageTimestamp;
@property (nonatomic, strong) NSData *userImageData;
@property (nonatomic, strong) NSNumber *wasSigned;
@property (nonatomic, strong) NSNumber *lat;
@property (nonatomic, strong) NSNumber *lng;
@property (nonatomic, strong) NSString *urlExtension;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
