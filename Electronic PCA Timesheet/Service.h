//
//  Service.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/25/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, ServiceNumberDescr) {
    ServiceNumberDescr_None,
    ServiceNumberDescr_PCA,
    ServiceNumberDescr_Homemaking,
    ServiceNumberDescr_Respite,
    ServiceNumberDescr_CaregivingExpense,
    ServiceNumberDescr_EnvironmentalModifications,
    ServiceNumberDescr_PersonalSupport,
    ServiceNumberDescr_SelfDirectionSupport,
    ServiceNumberDescr_ConsumerSupport,
    ServiceNumberDescr_PersonalAssistance,
    ServiceNumberDescr_TreatmentAndTraining,
};

@interface Service : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSNumber *idNumber;
@property (nonatomic, strong) NSString *serviceName;
@property (nonatomic) ServiceNumberDescr serviceNumberdescr;

-(instancetype)initWithDictionary:(NSDictionary *)dict;

@end
