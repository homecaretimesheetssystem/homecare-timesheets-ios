//
//  AppDelegate.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/29/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LoginViewController.h"
#import "TimeInAndOutViewController.h"
#import "Session.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) CLLocationManager *appLocationManager;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navCont;
@property (strong, nonatomic) Session *currentSession;
@property (nonatomic) BOOL shouldRotate;

@end
