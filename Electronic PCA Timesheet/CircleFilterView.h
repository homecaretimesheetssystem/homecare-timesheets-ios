//
//  CircleFilterView.h
//  camera
//
//  Created by Scott Mahonis on 7/24/16.
//  Copyright © 2016 JMG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleFilterView : UIView

@property (nonatomic, retain) UIColor *fillColor;
@property (nonatomic, retain) NSArray *framesToCutOut;

@end
