//
//  User.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/26/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "User.h"

@implementation User

-(instancetype)initWithDictionary:(NSDictionary *)dict {
    if (self == [super init]) {
        NSDictionary *userDict = dict[@"user"];
        
        if ([userDict objectForKey:@"id"] != [NSNull null]) {
            _idNumber = userDict[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }

        
        
        if ([userDict objectForKey:@"email"] != [NSNull null]) {
            _email = userDict[@"email"];
        }else{
            _email = @"";
        }
        
        NSDictionary *pcaDict = dict[@"pca"];
        if (pcaDict) {
            _pca = [[PCA alloc] initWithDictionary:pcaDict];
        } else {
            _pca = [PCA new];
            
        }
        if ([userDict[@"userType"] isEqualToString:@"pca"]) {
            _userType = UserType_IsPCA;
        } else {
            _userType = UserType_IsRecipient;
            if (dict[@"sessionData"] && [dict[@"sessionData"] isKindOfClass:[NSDictionary class]]) {
                if ([dict objectForKey:@"sessionData"][@"pca"]) {
                    if (![[dict objectForKey:@"sessionData"][@"pca"] isEqual:[NSNull null]]) {
                        _pca = [[PCA alloc] initWithDictionary:dict[@"sessionData"][@"pca"]];
                    }
                }
            } else if (dict[@"sessionData"] && [dict[@"sessionData"] isKindOfClass:[NSArray class]]) {
                NSArray *sessionDataArray = dict[@"sessionData"];
                if (sessionDataArray.count > 0 && [sessionDataArray[0] isKindOfClass:[NSDictionary class]]) {
                    if (sessionDataArray[0][@"pca"] && ![sessionDataArray[0][@"pca"] isEqual:[NSNull null]]) {
                        _pca = [[PCA alloc] initWithDictionary:dict[@"sessionData"][0][@"pca"]];
                    }
                }
                
            }
        }
        
        _sessions = [NSMutableArray new];
        NSArray *array = dict[@"sessionData"];
        for (long i = 0; i < array.count; i++) {
            Session *session = [[Session alloc] initWithDictionary:dict andIndex:i andUserType:_userType];
//            if (session.timeIn.timeIn);
            [_sessions addObject:session];
        }
        NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                            sortDescriptorWithKey:@"timeIn.timeIn"
                                            ascending:NO];
        _sessions = [_sessions sortedArrayUsingDescriptors:@[dateDescriptor]].mutableCopy;
        [self signableSessionsFromAllSessions];
        
        if (![dict[@"agency"] isEqual:[NSNull null]]) {
            NSDictionary *agencyDict = dict[@"agency"];
            NSMutableDictionary *mutableAgencyDict = agencyDict.mutableCopy;
            if (mutableAgencyDict) {
                [mutableAgencyDict setValue:dict[@"amazonUrl"] forKey:@"amazonUrl"];
                _agency = [[Agency alloc] initWithDictionary:mutableAgencyDict.mutableCopy];
            } else {
                _agency = [Agency new];
            }
        } else {
            _agency = [Agency new];
        }
        
        NSArray *recipsArr= dict[@"recipients"];
        if (!_possibleRecipients) {
            _possibleRecipients = [NSMutableArray new];
        }
        if (recipsArr) {
            for (NSDictionary *rDict in recipsArr) {
                Recipient *rec = [[Recipient alloc] initWithDictionary:rDict];
                [_possibleRecipients addObject:rec];
            }
        }
        
        if  (dict[@"ratios"]) {
            NSArray *arr = dict[@"ratios"];
            NSMutableArray *mutArr = [NSMutableArray new];
            for (long i = 0; i < arr.count; i++) {
                NSDictionary *dict = arr[i];
                Ratio *ratio = [[Ratio alloc] initWithDictionary:dict];
                [mutArr addObject:ratio];
            }
            
            _possibleRatios = mutArr.mutableCopy;
        }
        if (dict[@"sharedCareLocations"]) {
            NSArray *arr = dict[@"sharedCareLocations"];
            NSMutableArray *mutArr = [NSMutableArray new];
            for (NSDictionary *dict in arr) {
                CareLocation *location = [[CareLocation alloc] initWithDictionary:dict];
                [mutArr addObject:location];
            }
            _possibleCareLocations = mutArr.mutableCopy;
        }
        
        if (dict[@"careOptions"]) {
            NSArray *arr = dict[@"careOptions"];
            NSMutableArray *mutArr = [NSMutableArray new];
            for (NSDictionary *dict in arr) {
                CareOption *careOption = [[CareOption alloc] initWithDictionary:dict];
                [mutArr addObject:careOption];
            }
            _possibleCareOptions = [mutArr mutableCopy];
        }
        
        
        if (dict[@"recipients"]) {
            NSArray *arr = dict[@"recipients"];
            NSMutableArray *mutArr = [NSMutableArray new];
            for (NSDictionary *dict in arr) {
                Recipient *recipient = [[Recipient alloc] initWithDictionary:dict];
                [mutArr addObject:recipient];
            }
            _recipientsArray = [mutArr mutableCopy];
        }
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    User *copy = [[User allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.email = _email;
    copy.sessions = _sessions;
    copy.userType = _userType;
    copy.currentSession = _currentSession;
    copy.agency = _agency;
    copy.possibleRatios = self.possibleRatios;
    copy.possibleRecipients = self.possibleRecipients;
    copy.possibleCareOptions = self.possibleCareOptions;
    copy.pca = self.pca;
    copy.signableSessions = self.signableSessions;
    copy.recipientsArray = self.recipientsArray;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _email = [aDecoder decodeObjectForKey:@"email"];
        _sessions = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"sessions"]];
        _userType = [aDecoder decodeIntegerForKey:@"userType"];
        _currentSession = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"currentSession"]];
        self.agency = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"agency"]];
        self.possibleRecipients = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"possibleRecipients"]];
        self.possibleRatios = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"possibleRatios"]];
        self.possibleCareLocations = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"possibleCareLocations"]];
        self.possibleCareOptions = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"possibleCareOptions"]];
        self.pca = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"pca"]];
        self.signableSessions = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"signableSessions"]];
     //   self.recipientsArray = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"recipients"]];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.idNumber forKey:@"idNumber"];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.sessions] forKey:@"sessions"];
    [aCoder encodeInteger:self.userType forKey:@"userType"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.currentSession] forKey:@"currentSession"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.agency] forKey:@"agency"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.possibleRecipients] forKey:@"possibleRecipients"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.possibleRatios] forKey:@"possibleRatios"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.possibleCareLocations] forKey:@"possibleCareLocations"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.possibleCareOptions] forKey:@"possibleCareOptions"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.pca] forKey:@"pca"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.signableSessions] forKey:@"signableSessions"];
  //  [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.recipient] forKey:@"recipients"];

}

-(NSString *)description {
    return [NSString stringWithFormat:@"USER: {\r   ID Number:%@,\r    Email:%@,\r    UserType:%ld,\r    PCA:%@,\r   Agency: %@,\r   Current Session:%@,\r   Sessions:%@\r   SignableSessions:%@\r    PossibleRatios:%@\r Recipients:%@\r }", self.idNumber, self.email, (unsigned long)self.userType, self.pca, self.agency, self.currentSession, self.sessions, self.signableSessions, self.possibleRatios, self.recipientsArray];
}

-(void)assignCurrentSession:(Session *)session {
    self.currentSession = session;
}

+(User *)currentUser {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"currentUser"];
    User *currentUser = [NSKeyedUnarchiver unarchiveObjectWithFile:appFile];
    return currentUser;
}

-(void)save {
    if (self.currentSession) {
        for (long i = 0; i < self.sessions.count; i++) {
            Session *session = self.sessions[i];
            if ([session.idNumber isEqualToNumber:self.currentSession.idNumber]) {
                [self.sessions replaceObjectAtIndex:i withObject:self.currentSession];
                break;
            }
        }
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"currentUser"];
    [NSKeyedArchiver archiveRootObject:self toFile:appFile];
}

-(void)removeSessionWithID:(NSNumber *)idNumber {
    for (Session *session in self.sessions) {
        if ([session.idNumber isEqualToNumber:idNumber]) {
            [self.sessions removeObject:session];
            break;
        }
    }
    [self signableSessionsFromAllSessions];
    self.currentSession = nil;
    [self save];
}

-(void)overWriteSessionWithID:(NSNumber *)idNumber withSession:(Session *)newSession {
    for (int i = 0; i < self.sessions.count; i++) {
        Session *session = self.sessions[i];
        if ([session.idNumber isEqualToNumber:idNumber]) {
            [self.sessions replaceObjectAtIndex:i withObject:newSession];
            break;
        }
    }
    [self signableSessionsFromAllSessions];
    [self save];
}

-(void)signableSessionsFromAllSessions {
    _signableSessions = [NSMutableArray new];
    if (_userType == UserType_IsRecipient) {
        for (Session *session in _sessions) {
            if ([session.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES]) {
                [_signableSessions addObject:session];
            }
        }
    }
}

@end
