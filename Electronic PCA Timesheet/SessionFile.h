//
//  SessionFile.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/28/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SessionFile : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSNumber *idNumber;
@property (nonatomic, strong) NSString *timeInPicture;
@property (nonatomic, strong) NSString *verificationPicture;
@property (nonatomic, strong) NSString *timeOutPicture;
@property (nonatomic, strong) NSNumber *verificationID;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
