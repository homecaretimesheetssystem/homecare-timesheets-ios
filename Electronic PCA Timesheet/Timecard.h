//
//  Timecard.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/30/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Recipient.h"
#import "PCASignature.h"
#import "RecipientSignature.h"
#import "File.h"


@class  Recipient, PCA, PCASignature, RecipientSignature, File;

@interface Timecard : NSObject <NSCopying, NSCoding>

@property (strong, nonatomic) NSNumber *idNumber;
@property (strong, nonatomic) NSNumber *timecardIdNumber;
@property (strong, nonatomic) NSArray *careOptionsArray;
@property (strong, nonatomic) Recipient *recipient;
@property (strong, nonatomic) RecipientSignature *recipientSignature;
@property (strong, nonatomic) PCASignature *pcaSignature;
@property (strong, nonatomic) File *file;
@property (strong, nonatomic) NSNumber *finished;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
