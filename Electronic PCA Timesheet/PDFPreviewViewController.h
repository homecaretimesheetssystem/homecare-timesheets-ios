//
//  PDFPreviewViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 12/5/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "SignatureView.h"
#import <CoreLocation/CoreLocation.h>
#import "Session.h"
@class SignatureView, Session;

typedef NS_ENUM(NSUInteger, SignatureIsFor) {
    SignatureIsFor_Recipient,
    SignatureIsFor_PCA,
};

@interface PDFPreviewViewController : UIViewController <MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate, sigViewDelegate, CLLocationManagerDelegate, UITextFieldDelegate> {
    bool isIpad; bool four; int width; int height;
    UIBarButtonItem *emailButton; UIBarButtonItem *back;
    
    // PDF vars
    UIWebView *webView; NSDate *dateEnding; NSString *weekDay;  NSData *csvDataString; UIImage *recSignature; UIImage *pcaSignature;
    
    // Signature Vars
    SignatureView *sigView; UIButton *sigButton; UIButton *sendPDFButton; UIButton *saveAndExitButton; UIButton *acceptSignatureButton; UIButton *clearSignature; UIButton *deleteSignature; UIButton *acceptFWButton; UIButton *cancelFWButotn; bool mouseSwiped; CGPoint lastPoint; UIBezierPath *bezPath; UILabel *xLab; CALayer *bottomLine; UIView *fraudView; CALayer *fraudLine; CALayer *line1; CALayer *line2; CALayer *line3; CALayer *line4; CALayer *line5; UIView *blackView; CLLocationManager *locManager; UIView *recipientPasswordView; UITextField *passwordTextField;
}

@property (strong, nonatomic) Session *currentSession;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSArray *timeCardArray;

@end

