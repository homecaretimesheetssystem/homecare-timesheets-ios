//
//  WorkerSessionView.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/1/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol WorkerSessionViewDelegate <NSObject>

-(void)chooseSessionAtIndex:(long)index;
-(void)overwriteSessionAtIndex:(long)index;

@end
typedef NS_ENUM(NSUInteger, ChooseTo) {
    ChooseTo_Continue,
    ChooseTo_Delete,
};
#import "SessionTableViewCell.h"
#import "ActiveTimesheetCollectionViewCell.h"

@interface WorkerSessionView : UIView <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *sessionsArray;
@property (nonatomic, strong) IBOutlet UIButton *dismissSelfButton;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic) id<WorkerSessionViewDelegate>delegate;
@property (nonatomic) ChooseTo chooseTo;

@end
