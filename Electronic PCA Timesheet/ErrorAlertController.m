//
//  ErrorAlertController.m
//  Kindu
//
//  Created by Scott Mahonis on 10/21/15.
//  Copyright © 2015 The Jed Mahonis Group. All rights reserved.
//

#import "ErrorAlertController.h"

@implementation ErrorAlertController

+(UIAlertController *)alertControllerWithMessage:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
    return alertController;
}

@end
