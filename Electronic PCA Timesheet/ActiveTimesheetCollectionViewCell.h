//
//  ActiveTimesheetCollectionViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/27/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Session.h"

@interface ActiveTimesheetCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeInLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeOutLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalTimeLabel;

@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) User *currentUser;

-(void)setValuesForUI;

@end
