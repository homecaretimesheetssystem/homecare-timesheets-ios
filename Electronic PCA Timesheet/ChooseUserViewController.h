//
//  ChooseUserViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/22/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Session.h"
#import "ActivityInfoViewController.h"
#import "Recipient.h"
#import "PCA.h"
#import "User.h"
#import "JMGCameraViewController.h"

@class Session;

@interface ChooseUserViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, JMGCameraViewControllerDelegate> {
    bool isIpad; bool four; int width; int height;
    UIPickerView *recipientPicker;
    NSArray *recipientArray;
    UITextField *recipientTextField;
    long arrayRowRecipient;
    UIButton *nextScreenButton;
    UIView *createRecipientView;
    
    UITextField *recipientNameTextField;
    UITextField *recipientLastNameTextField;
    UITextField *maNumberTextField;
    UIScrollView *scroller;
    UIButton *saveRecipientButton;
}

@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic) BOOL shouldStartNewTimesheet;

@end
