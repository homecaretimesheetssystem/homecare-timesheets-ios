//
//  Timecard.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/30/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "Timecard.h"

@implementation Timecard

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        _idNumber = dictionary[@"id"];
        NSDictionary *recDict = dictionary[@"recipient"];
        if (!recDict) {
            _recipient = [Recipient new];
        } else {
            _recipient = [[Recipient alloc] initWithDictionary:recDict];
        }
        NSDictionary *recSigDict = dictionary[@"recipient_signature"];
        if (!recSigDict) {
            _recipientSignature = [[RecipientSignature alloc] initWithDictionary:@{}];
        } else {
            _recipientSignature = [[RecipientSignature alloc] initWithDictionary:recSigDict];
        }
        
        NSDictionary *pcaSigDict = dictionary[@"pca_signature"];
        if (!pcaSigDict) {
            _pcaSignature = [[PCASignature alloc] initWithDictionary:@{}];
        } else {
            _pcaSignature = [[PCASignature alloc] initWithDictionary:pcaSigDict];
        }
        NSMutableArray *mutArr = [NSMutableArray new];
        NSArray *coarray = dictionary[@"careOptionTimesheets"];
        for (NSDictionary *dictionary in coarray) {
            [mutArr addObject:dictionary[@"careOption"][@"id"]];
        }
        _careOptionsArray = mutArr.mutableCopy;
        if (!_careOptionsArray) {
            _careOptionsArray = [NSArray new];
        }
        if (dictionary[@"finished"]) {
            _finished = dictionary[@"finished"];
        } else {
            _finished = @0;
        }
        if (dictionary[@"file"]) {
            _file = [[File alloc] initWithDictionary:dictionary[@"file"]];
        } else {
            _file = [File new];
        }
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    Timecard *timecardCopy = [[Timecard allocWithZone:zone] init];
    timecardCopy.idNumber = _idNumber;
    timecardCopy.careOptionsArray = _careOptionsArray;
    timecardCopy.recipient = self.recipient;
    timecardCopy.recipientSignature = self.recipientSignature;
    timecardCopy.pcaSignature = self.pcaSignature;
    timecardCopy.file = _file;
    return timecardCopy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _careOptionsArray = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"careOptionsArray"]];
        _recipient = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"recipient"]];
        _recipientSignature = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"recipientSignature"]];
        _pcaSignature = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"pcaSignature"]];
        _file = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"file"]];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:_careOptionsArray] forKey:@"careOptionsArray"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:_recipient] forKey:@"recipient"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:_recipientSignature] forKey:@"recipientSignature"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:_file] forKey:@"file"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:_pcaSignature] forKey:@"pcaSignature"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Timecard ID :%@, CareOptionsArray: %@, Recipient:%@, RecipientSignature:%@, PCASignature:%@, File:%@", self.idNumber, self.careOptionsArray, self.recipient, self.recipientSignature, self.pcaSignature, _file];
}

@end
