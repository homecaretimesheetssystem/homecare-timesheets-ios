//
//  ActiveTimesheetsCollectionViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/25/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignatureView.h"
#import "FraudView.h"
#import <CoreLocation/CoreLocation.h>

@interface ActiveTimesheetsCollectionViewController : UICollectionViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, sigViewDelegate, CLLocationManagerDelegate> {
    float width; float height; bool iPad; NSData *csvDataString;
}

@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) NSData *recipientSignatureForAllTimesheets;
@property (nonatomic, strong) CLLocationManager *locationManager;

@property (nonatomic, weak) IBOutlet FraudView *fraudView;
@property (nonatomic, weak) IBOutlet UIView *signatureContainerView;
@property (nonatomic, weak) IBOutlet SignatureView *signatureView;
@property (nonatomic, weak) IBOutlet SignatureView *iPadSignatureView;
@property (nonatomic, weak) IBOutlet UIButton *acceptSignatureButton;
@property (nonatomic, weak) IBOutlet UIView *sigViewSubViewsContainerView;
@property (nonatomic, weak) IBOutlet UIView *iPadSigViewSubViewsContainerView;

@end
