//
//  WorkerSessionView.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/1/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "WorkerSessionView.h"

@implementation WorkerSessionView

-(instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        _sessionsArray = [User currentUser].sessions.copy;
        [self createUI];
//        [self animateHeight];
    }
    return self;
}

-(void)createUI {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [layout setSectionInset:UIEdgeInsetsMake(10, 10, 10, 10)];
    layout.minimumLineSpacing = 10.0;
    layout.minimumInteritemSpacing = 10.0;
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, [UIApplication sharedApplication].statusBarFrame.size.height + 44, self.frame.size.width, self.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height - 44) collectionViewLayout:layout];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [collectionView registerNib:[UINib nibWithNibName:@"ActiveTimesheetCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"timesheetCell"];
    collectionView.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self addSubview:collectionView];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _sessionsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ActiveTimesheetCollectionViewCell *cell = (ActiveTimesheetCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"timesheetCell" forIndexPath:indexPath];
    Session *session = _sessionsArray[indexPath.row];
    cell.currentSession = session;
    cell.tag = indexPath.row;
    [cell setValuesForUI];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.frame.size.width - 16, 124);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.chooseTo == ChooseTo_Continue) {
        [self.delegate chooseSessionAtIndex:indexPath.row];
    } else if (self.chooseTo == ChooseTo_Delete){
        [self.delegate overwriteSessionAtIndex:indexPath.row];
    }
}

@end
