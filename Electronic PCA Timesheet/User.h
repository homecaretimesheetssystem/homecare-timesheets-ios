//
//  User.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/26/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Recipient.h"
#import "Session.h"
#import "Agency.h"
#import "PCA.h"

typedef NS_ENUM(NSUInteger, UserType) {
    UserType_IsPCA,
    UserType_IsRecipient,
};

@class Session, PCA;

@interface User : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSNumber *idNumber;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSMutableArray *sessions;
@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) Agency *agency;
@property (strong, nonatomic) NSMutableArray *possibleRecipients;
@property (strong, nonatomic) NSArray *possibleCareOptions;
@property (strong, nonatomic) NSArray *possibleRatios;
@property (strong, nonatomic) NSArray *possibleCareLocations;
@property (strong, nonatomic) NSMutableArray *signableSessions;
@property (strong, nonatomic) NSMutableArray *recipientsArray;

@property (nonatomic) UserType userType;
@property (strong, nonatomic) PCA *pca;

-(instancetype)initWithDictionary:(NSDictionary *)dict;
-(void)assignCurrentSession:(Session *)session;
+(User *)currentUser;
-(void)save;
-(void)removeSessionWithID:(NSNumber *)idNumber;
-(void)overWriteSessionWithID:(NSNumber *)idNumber withSession:(Session *)newSession;

@end
