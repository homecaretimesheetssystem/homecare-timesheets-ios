//
//  SelectServiceViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/30/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Timecard.h"
#import "RatioSharedCareViewController.h"
#import "Service.h"
#import "NetworkingHelper.h"

@class Session, Service;

@interface SelectServiceViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIGestureRecognizerDelegate> {
    bool isIpad; bool four;
    int width;
    int height;
    long arrayRow;
    NSArray *serviceArray;
    UITextField *serviceTextField;
    UIButton *nextScreenButton;
}

@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) NSArray *possibleServicesArray;
@property (nonatomic, strong) Service *selectedService;
@property (nonatomic, strong) User *currentUser;

@end
