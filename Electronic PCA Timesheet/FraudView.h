//
//  FraudView.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/30/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FraudView : UIView

@property (nonatomic, weak) IBOutlet UITextView *warningTV;


@end
