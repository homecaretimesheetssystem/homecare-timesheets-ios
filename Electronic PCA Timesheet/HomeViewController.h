//
//  HomeViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/29/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Session.h"
#import "WorkerSessionView.h"
#import "SelectServiceViewController.h"
#import "RatioSharedCareViewController.h"
#import "TimeInAndOutViewController.h"
#import "ChooseUserViewController.h"
#import "ActivityInfoViewController.h"
#import "PDFPreviewViewController.h"

@class Session;

@interface HomeViewController : UIViewController <UIAlertViewDelegate, UITextFieldDelegate, WorkerSessionViewDelegate> {
    bool isIpad;
    int width;
    int height;
    bool four;
    UIActivityIndicatorView *activityInd;
    BOOL onDemandEnabled;
}

@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) User *currentUser;

@end
