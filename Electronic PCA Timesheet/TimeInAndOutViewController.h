//
//  TimeInAndOutViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 8/4/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Session.h"
#import "ChooseUserViewController.h"
#import "NSDate + Compare.h"
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "User.h"

@class Session;

@interface TimeInAndOutViewController : UIViewController <UITextFieldDelegate, UIScrollViewDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate, CLLocationManagerDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
    bool isIpad; bool four;
    int width; int height; long accumulatedSeconds; bool automaticTimeOut;
    UIButton *timeInButton; UIButton *timeOutButton;
    UILabel *timerLabel; UILabel *billableTimeLabel; UIView *estmiatedShiftView;
    NSArray *hoursArray; NSArray *minutesArray; NSString *hourString; NSString *minutesString;
}

@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) UITextField *editTimeTextField;

@end
