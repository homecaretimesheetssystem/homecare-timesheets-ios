//
//  Ratio.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/14/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "Ratio.h"

@implementation Ratio

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"ratio"] != [NSNull null]) {
            _ratio = dictionary[@"ratio"];
        }else{
            _ratio = @"";
        }

    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    Ratio *copy = [[Ratio allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.ratio = _ratio;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _ratio = [aDecoder decodeObjectForKey:@"ratio"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_ratio forKey:@"ratio"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"ID:%@, Ratio:%@", _idNumber, _ratio];
}

@end
