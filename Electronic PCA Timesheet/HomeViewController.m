//
//  HomeViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/29/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "HomeViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <Crashlytics/Crashlytics.h>

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _currentUser = [User currentUser];
    if (_currentUser.sessions.count > 0) {
        _currentSession = _currentUser.sessions[0];
    }
//    onDemandEnabled = YES;   <----- TEST CODE FOR ON DEMAND FRAMES
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
    if (isIpad) {
//        width = 1024;
//        height = 768;
    }
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self addLogo];
    [self addButtons];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    _currentUser = [User currentUser];
    if (_currentUser.sessions.count > 0) {
        _currentSession = _currentUser.sessions[0];
    } else {
        self.navigationItem.leftBarButtonItem.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-(void)logoExists {
    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(75, self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height + 20, 200, 200)];
    [self.view addSubview:logoView];
    [logoView setImageWithURL:_currentSession.agency.logoURL];
    logoAdded = YES;
    CGPoint centerImageView = logoView.center;
    centerImageView.x = self.view.center.x;
    logoView.center = centerImageView;
}*/

-(void)addLogo
{
    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height + 20, height * 0.4166666667, height * 0.4166666667)];
    [self.view addSubview:logoView];
    CGPoint centerImageView = logoView.center;
    centerImageView.x = self.view.center.x;
    logoView.center = centerImageView;
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.hidesWhenStopped = YES;
    [activityIndicator startAnimating];
    activityIndicator.center = logoView.center;
    [self.view addSubview:activityIndicator];
    __weak typeof(UIImageView *) weakLogoImageView = logoView;
    [logoView setImageWithURLRequest:[NSURLRequest requestWithURL:_currentUser.agency.logoURL] placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        weakLogoImageView.image = image;
        [activityIndicator removeFromSuperview];
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        NSLog(@"error setting profile image:%@", error);
        weakLogoImageView.image = [UIImage imageNamed:@"Icon.png"];
        [activityIndicator removeFromSuperview];
    }];
}

-(void)addButtons {
    UIImageView *imageView;
    for (UIImageView *imgVw in self.view.subviews) {
        if ([imgVw isKindOfClass:[UIImageView class]]) {
            imageView = imgVw;
        }
    }
    
    if (_currentUser.agency.name && ![_currentUser.agency.name isEqual:[NSNull null]] && ![_currentUser.agency.name isEqualToString:@""]) {
        self.title = _currentUser.agency.name;
    } else {
        self.title = @"HomeCare Timesheets";
    }
    
    self.navigationItem.backBarButtonItem.title = @"";
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(chooseSessionToDelete)];
    CGFloat createYOriginMultiplier = onDemandEnabled ? 0.5 : 0.62;
    
    UIButton *createButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (isIpad) {
        [createButton setFrame:CGRectMake(self.view.center.x - width * 0.25, height * 0.65, width * 0.5, 100)];
        [createButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:36]];
        
        [createButton setTitle:@"Create New\rTime Entry" forState:UIControlStateNormal];
    } else {
        [createButton setTitle:@"Create New Time Entry" forState:UIControlStateNormal];
        [createButton setFrame:CGRectMake(40, height * createYOriginMultiplier, width * 0.8, 60)];
        [createButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
        CGPoint centerImageView = createButton.center;
        centerImageView.x = self.view.center.x;
        createButton.center = centerImageView;

    }
    
    createButton.titleLabel.numberOfLines = 0;
    createButton.layer.cornerRadius = createButton.frame.size.height/2;
    createButton.layer.masksToBounds = YES;
    createButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [createButton addTarget:self action:@selector(createNewSession) forControlEvents:UIControlEventTouchUpInside];
    [createButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [createButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [[createButton layer] setBorderWidth:2.0f];
    [[createButton layer] setBorderColor:[UIColor whiteColor].CGColor];
    [self.view addSubview:createButton];
    
    
    
    UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (isIpad) {
        [continueButton setFrame:CGRectMake(self.view.center.x - width * 0.25, height * 0.85, width * 0.5, 100)];
        [continueButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:36]];
        [continueButton setTitle:@"Continue\nTime Entry" forState:UIControlStateNormal];
    } else {
        [continueButton setFrame:CGRectMake(40, height * 0.8, width * 0.8, 60)];
        [continueButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
        [continueButton setTitle:@"Continue Time Entry" forState:UIControlStateNormal];
        CGPoint centerImageView = continueButton.center;
        centerImageView.x = self.view.center.x;
        continueButton.center = centerImageView;

    }
    continueButton.titleLabel.numberOfLines = 0;
    continueButton.layer.cornerRadius = continueButton.frame.size.height/2.;
    continueButton.layer.masksToBounds = YES;
    [continueButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [continueButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    continueButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [continueButton addTarget:self action:@selector(continueTimeSheetButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [[continueButton layer] setBorderWidth:2.0f];
    [[continueButton layer] setBorderColor:[UIColor whiteColor].CGColor];
    [self.view addSubview:continueButton];
    
}

#pragma mark - Update Server 

-(void)overwriteSessionAtIndex:(long)index {
    [self showOverwriteWarningWithIndex:index];
}

-(void)chooseSessionToDelete {
    [self showWorkerSessionViewWithChooseTo:ChooseTo_Delete];
}

-(void)deleteSessionFromServer:(Session *)session {
    if (session.idNumber) {
        LoadingView *lv = [LoadingView addLoadingViewWithText:@"Deleting"];
        [NetworkingHelper deleteTimecardsFromSessionWithID:session.idNumber andSuccess:^(id responseObject) {
            [lv removeFromSuperview];
            [self removeWorkerSessionsView];
            [_currentUser removeSessionWithID:session.idNumber];
            if (self.currentUser.sessions.count == 0) {
                self.navigationItem.leftBarButtonItem.enabled = NO;
            }
//            [self createNewSession];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [lv removeFromSuperview];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error deleting the time entry." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self deleteSessionFromServer:session];
            }]];
            NSLog(@"failed to delete timecards from session:%@", error.localizedDescription);
        }];
    }
}

-(void)updateSession:(Session *)session withTimesheetID:(NSNumber *)timesheetID {
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterShortStyle;
    [NetworkingHelper saveSessionDataWithSessionNumber:session.idNumber andParams:@{@"pca":_currentUser.pca.idNumber} andsuccess:^(id responseObject) {
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failed to updated session data:%@", error.localizedDescription);
    }];
}

#pragma mark - New Session Logic

-(void)showOverwriteWarningWithIndex:(long)index {
    Session *sessionToDelete = _currentUser.sessions[index];
    NSString *message = @"Are you sure you want to delete a time entry?";
    if (sessionToDelete.timesheetIDsArray.count > 1) {
        message = [NSString stringWithFormat:@"Are you sure you want to delete %ld time entries from %@", (unsigned long)sessionToDelete.timesheetIDsArray.count, [self dateEndingStringForSession:sessionToDelete]];
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        [self deleteSessionFromServer:sessionToDelete];
//        [self overwriteSessionAtIndex:0];
    }]];
    [self presentViewController:alert animated:YES completion:NULL];
}

-(NSString *)dateEndingStringForSession:(Session *)currentSession {
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    NSDate *stringDate = [dateformat dateFromString:currentSession.dateEnding];
    NSDateFormatter *shortFormat = [NSDateFormatter new];
    [shortFormat setDateStyle:NSDateFormatterShortStyle];
    [shortFormat setTimeStyle:NSDateFormatterNoStyle];
    NSString *dateString = [shortFormat stringFromDate:stringDate];
    if (!stringDate) {
        return currentSession.dateEnding;
    } else {
        return dateString;
    }

}

-(void)createNewSession {
    LoadingView *loadingView = [LoadingView addLoadingViewWithText:@"Creating"];
    loadingView.center = self.view.center;
    [self.view addSubview:loadingView];
    Session *newSession = [Session new];
    [newSession resetLocalValues];
    [NetworkingHelper createSessionDataTableWithSuccess:^(id responseObject) {
        newSession.idNumber = responseObject[@"createdSessionData"][@"id"];
        [NetworkingHelper createNewTimesheetTableWithSuccess:^(id responseObject) {
            newSession.currentTimecard.idNumber = responseObject[@"createdTimesheet"][@"id"];
            newSession.currentTimesheetID = responseObject[@"createdTimesheet"][@"id"];
            newSession.currentTimesheetNumber = @1;
            [self updateSession:newSession withTimesheetID:newSession.currentTimecard.idNumber];
            newSession.timesheetIDsArray = [NSMutableArray arrayWithObject:newSession.currentTimesheetID];
            [NetworkingHelper saveTimesheetDataWithTimesheetID:newSession.currentTimecard.idNumber andParams:@{@"sessionData":newSession.idNumber, } andsuccess:^(id responseObject) {
                [loadingView hideActivityIndicator];
                [_currentUser.sessions addObject:newSession];
                _currentUser.currentSession = newSession;
                _currentSession = newSession;
                [_currentUser save];
                [self nextViewController];
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                [loadingView hideActivityIndicator];
                NSLog(@"failed to update timesheet:%@", error);
            }];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [loadingView hideActivityIndicator];
            id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"%@", responseObject[@"error"]]] animated:YES completion:NULL];

//            [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"Unable to create a new time entry:%@", error.localizedDescription]] animated:YES completion:NULL];
            NSLog(@"Failed to create new Timesheet:%@", error);
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [loadingView hideActivityIndicator];
        NSLog(@"failed to create session data table:%@", error);
    }];
    
}

-(void)nextViewController {
    
    if (_currentUser.pca.servicesArray.count == 1) {
        [self saveServiceToCurrentSession];
    } else {
        [self.navigationController showViewController:[SelectServiceViewController new] sender:self];
    }
}

-(void)saveServiceToCurrentSession {
    Service *selectedService = _currentUser.pca.servicesArray[0];
    [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:@{@"service":selectedService.idNumber, @"pca":_currentUser.pca.idNumber} andsuccess:^(id responseObject) {
        _currentUser.currentSession.service = selectedService;
        _currentUser.currentSession = _currentSession;
        [_currentUser save];
        if ([self shouldShowRatio] && _currentUser.possibleRatios.count > 0) {
            [self.navigationController showViewController:[RatioSharedCareViewController new] sender:self];
        } else {
            [self doesntNeedARatio];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"%@", responseObject[@"error"]]] animated:YES completion:NULL];
//
//        [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"Error Saving Time Entry Info:%@", error.localizedDescription]] animated:YES completion:NULL];
        NSLog(@"Home Screen Service Time Entry error:%@", error.localizedDescription);
    }];

}

- (BOOL)shouldShowRatio {
    Service *selectedService = _currentUser.pca.servicesArray[0];
    if (selectedService.serviceNumberdescr == ServiceNumberDescr_PCA || selectedService.serviceNumberdescr == ServiceNumberDescr_Respite || selectedService.serviceNumberdescr == ServiceNumberDescr_ConsumerSupport || selectedService.serviceNumberdescr == ServiceNumberDescr_PersonalAssistance || selectedService.serviceNumberdescr == ServiceNumberDescr_TreatmentAndTraining) {
        return YES;
    } else {
        
        return NO;
    }
}

-(void)doesntNeedARatio {
    if (_currentUser.possibleRecipients.count > 1) {
        [self.navigationController showViewController:[ChooseUserViewController new] sender:self];
    } else {
        if (_currentUser.possibleRecipients.count > 0) {
            return [self saveRecipientToCurrentSession];
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"You have no assigned recipients. Please contact system administrator."] animated:YES completion:NULL];
        }
    }
}

-(void)saveRecipientToCurrentSession {
    _currentSession.currentTimecard.recipient = (Recipient *)_currentUser.possibleRecipients[0];
    _currentUser.currentSession = _currentSession;
    [_currentUser save];
    [NetworkingHelper saveTimesheetDataWithTimesheetID:_currentSession.currentTimecard.idNumber andParams:@{@"recipient": _currentSession.currentTimecard.recipient.idNumber} andsuccess:^(id responseObject) {
        // The new call to check if verification comes in
        [NetworkingHelper checkVerificationForCamera:@{@"pcaid":_currentUser.pca.idNumber, @"recipientid":_currentSession.currentTimecard.recipient.idNumber} success:^(id responseObject) {
//            NSLog(@"thisResponse object:%@", responseObject);
            if ([self string:self.currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
                
                if ([responseObject[@"verification_needed"] boolValue] == YES) {
                    _currentSession.verificationID = responseObject[@"id"];
                    _currentSession.verificationIsRequiredForWorkerAndRecipient = @YES;
//                    NSLog(@"ThisVerificationID is :%@", _currentSession.verificationID);
                    _currentUser.currentSession = _currentSession;
                    [_currentUser save];
                    [self showTheCamera];
                }
                else
                {
//                    NSLog(@"currentTimesheetNumber:%@", _currentSession.currentTimesheetNumber);
                   [self showNextScreenThatIsntTheCamera];
                }
            }
            else
            {
                [self showNextScreenThatIsntTheCamera];
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"Failed to check verification:%@", error);
            [CrashlyticsKit recordError:error];
            [self showNextScreenThatIsntTheCamera];
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Failed to save recipient and pca:%@", error);
    }];
}

-(void)showNextScreenThatIsntTheCamera {
    id vc;
    if ([_currentSession.currentTimesheetNumber isEqualToNumber:@1]) {
        vc = [TimeInAndOutViewController new];
    }
    else
    {
        if ([self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"] || [self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
            vc = [ActivityInfoViewController new];
        } else {
            vc = [PDFPreviewViewController new];
        }
    }
    [self.navigationController showViewController:vc sender:self];;
}

-(void)showTheCamera {
    [self getCameraPermissions:nil];
}

-(void)showWorkerSessionViewWithChooseTo:(ChooseTo )chooseTo {
    WorkerSessionView *view = [[WorkerSessionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view.delegate = self;
    view.chooseTo = chooseTo;
    [self.view addSubview:view];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(removeWorkerSessionsView)];
}

#pragma mark - Continue Session Logic

-(void)continueTimeSheetButtonPressed {
    if (_currentUser.sessions.count > 1) {
        [self showWorkerSessionViewWithChooseTo:ChooseTo_Continue];
    } else if (_currentUser.sessions.count == 0) {
        [self createNewSession];
    } else {
        _currentUser.currentSession = _currentUser.sessions[0];
        [_currentUser save];
        [self goToViewControllerForCurrentSession];
    }
}

-(void)chooseSessionAtIndex:(long)index {
    
    _currentSession = _currentUser.sessions[index];
    _currentUser.currentSession = _currentSession;
    [_currentUser save];
    [self goToViewControllerForCurrentSession];
}

-(void)goToViewControllerForCurrentSession {
    [self.navigationController setViewControllers:[self arrayOfViewControllersForContinueTimesheetNumber:_currentSession.continueTimesheetNumber andSelectedService:_currentSession.service.serviceName] animated:YES];
    [self removeWorkerSessionsView];
}

-(void)removeWorkerSessionsView {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(chooseSessionToDelete)];
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[WorkerSessionView class]]) {
            [subView removeFromSuperview];
        }
    }
}

-(NSArray *)arrayOfViewControllersForContinueTimesheetNumber:(NSNumber *)continueTimesheetNumber andSelectedService:(NSString *)selectedService {
    NSMutableArray *mutArr = [NSMutableArray arrayWithObjects:[LoginViewController new], self, nil];
    UIBarButtonItem *blankBack = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    long contNumber = [continueTimesheetNumber longValue];
    if (contNumber >= 0 || contNumber >= 1) {
        [mutArr addObject:[SelectServiceViewController new]];
    }
    if (contNumber >= 2) {
        [mutArr addObject:[RatioSharedCareViewController new]];
    }
    if (contNumber >= 3) {
        [mutArr addObject:[ChooseUserViewController new]];
    }
    if (contNumber >= 4) {
        [mutArr addObject:[TimeInAndOutViewController new]];
    }
    if (contNumber >= 5 && ([self string:selectedService containsCaseInsensitiveString:@"Personal Care Service"] || [self string:selectedService containsCaseInsensitiveString:@"Homemaking"])) {
        [mutArr addObject:[ActivityInfoViewController new]];
        if (contNumber >= 6) {
            [mutArr addObject:[PDFPreviewViewController new]];
        }
    } else if (contNumber >= 5) {
        [mutArr addObject:[PDFPreviewViewController new]];
    }
    
    for (UIViewController *vc in mutArr) {
        if ([vc isKindOfClass:[UIViewController class]]) {
            vc.navigationItem.backBarButtonItem = blankBack;
        }
    }
    return mutArr.copy;
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        return NO;
    }
    return YES;
}

#pragma mark - Camera Methods

-(void)openProfilePictureCamera {
    
    JMGCameraViewController *camera = [JMGCameraViewController new];
    camera.parentNavBarStyle = self.navigationController.navigationBar.barStyle;
    camera.parentStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
    camera.cameraMode = CameraMode_square;
    camera.faceFrontOnLaunch = YES;
    // If no value for File type is specified, default will be png. Other available fileType is jpg
    camera.fileType = FileType_jpg;
    camera.showWhiteCircleInSquareMode = NO;
    camera.showPictureFromCameraRollForChooseFromPhotoLibraryButton = NO;
    
    // Add some text if you want to add a title above the square crop camera. If nil, no label will be added.
    camera.titleAboveCamera = @"Verification Picture";
    
    camera.cameraBackgroundColor = [UIColor colorWithWhite:0.05 alpha:1.0];
    
    // cameraMode Property must be set to custom for this to work.
    //    camera.fileSize = CGSizeMake(512., 1024.);
    if (!self.navigationController) {
        [self presentViewController:camera animated:YES completion:NULL];
    } else {
        [self.navigationController showViewController:camera sender:self];
    }
}

-(void)getCameraPermissions:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openProfilePictureCamera];
            //            [self getPhotoLibraryPermissions];
        });
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getCameraPermissions:sender];
                });
                
            }
            
        }];
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined) {
        if  (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [self getPhotoLibraryPermissions];
        }
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) {
        [self showCameraDeniedAlert];
    } else {
        [self showCameraDeniedAlert];
    }
    
}

-(void)showCameraDeniedAlert {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Camera Access Denied" message:[NSString stringWithFormat:@"We can't access your camera. To give us access, go to Settings -> %@ and allow camera permission to use this feature.", [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"]] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goToPhoneSettings];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)getPhotoLibraryPermissions {
    
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openProfilePictureCamera];
        });
    } else if (status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            } else if ([self cantAccessCameraOrPhotoLibrary]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showAccessAlert];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            }
        }];
    } else if ([self cantAccessCameraOrPhotoLibrary]) {
        [self showAccessAlert];
    }
}

-(BOOL)cantAccessCameraOrPhotoLibrary {
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) && (status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)) {
        return YES;
    }
    return false;
}

-(void)showAccessAlert {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Photo Library Access Denied" message:[NSString stringWithFormat:@"We can't access your camera or photo library. To give us access, go to Settings -> %@ and allow access to camera or photos to use this feature.", [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey]] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goToPhoneSettings];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}

@end
