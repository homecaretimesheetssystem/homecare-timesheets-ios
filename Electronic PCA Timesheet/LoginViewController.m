//
//  LoginViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/12/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "LoginViewController.h"
#import <AFOAuth2Manager/AFOAuth2Manager.h>
#import "HomeViewController.h"
#import "User.h"
#import "ActiveTimesheetsCollectionViewController.h"
#import "ForgotPasswordViewController.h"
#import "AppDelegate.h"

@interface LoginViewController () 

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   // tokenIsLoaded = NO;
   // [self getToken];
    self.title = @"Sign In";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setWidthAndHeight];
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    
    [self createUI];
    self.navigationController.navigationBarHidden = NO;
    
    
}

-(void)setWidthAndHeight {
    isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
}

-(void)createUI {
    UILabel *loginLabel = [[UILabel alloc] init];
    loginLabel.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    loginLabel.text = @"Time Entry System";
    if (isIpad) {
        loginLabel.font = [UIFont fontWithName:@"Helvetica" size:50];
        loginLabel.frame = CGRectMake(0, 120, width, 70);
    } else {
        if (four) {
            loginLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
            loginLabel.frame = CGRectMake(0, 62, width, 70);
        } else {
            loginLabel.font = [UIFont fontWithName:@"Helvetica" size:28];
            loginLabel.frame = CGRectMake(0, 70, width, 75);
        }
    }

    loginLabel.textColor = [UIColor whiteColor];
    loginLabel.numberOfLines = 2;
    loginLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:loginLabel];
    
    usernameTextField = [[UITextField alloc] init];
    if (isIpad) {
        usernameTextField.frame = CGRectMake(10, 230, width/2 - 20, 75);
        usernameTextField.font = [UIFont fontWithName:@"Helvetica" size:40];
    } else {
        if (four) {
            usernameTextField.frame = CGRectMake(10, 135, width - 20, 50);
        } else {
            usernameTextField.frame = CGRectMake(10, 155, width - 20, 50);
        }
        usernameTextField.font = [UIFont fontWithName:@"Helvetica" size:24];
    }
    usernameTextField.placeholder = @"Username";
    usernameTextField.backgroundColor = [UIColor whiteColor];
    usernameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    usernameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    usernameTextField.borderStyle = UITextBorderStyleBezel;
    usernameTextField.textAlignment = NSTextAlignmentCenter;
    usernameTextField.delegate = self;
    [usernameTextField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:usernameTextField];
    
    passwordTextField = [[UITextField alloc] init];
    if (isIpad) {
        passwordTextField.frame = CGRectMake(width/2 + 10, 230, width/2 -20, 75);
        passwordTextField.font = [UIFont fontWithName:@"Helvetica" size:40];
    } else {
        if (four) {
            passwordTextField.frame = CGRectMake(10, 200, width - 20, 50);
        } else {
            passwordTextField.frame = CGRectMake(10, 230, width - 20, 50);
        }
        passwordTextField.font = [UIFont fontWithName:@"Helvetica" size:24];
    }
    
    passwordTextField.placeholder = @"Password";
    passwordTextField.backgroundColor = [UIColor whiteColor];
    passwordTextField.delegate = self;
    passwordTextField.borderStyle = UITextBorderStyleBezel;
    passwordTextField.textAlignment = NSTextAlignmentCenter;
    passwordTextField.secureTextEntry = YES;
    [passwordTextField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:passwordTextField];
    
    //    passwordTextField.text = @"(651) 330-2550";
    nextScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (isIpad) {
        nextScreenButton.frame = CGRectMake(width * 0.4, 340, width * 0.2, 50);
        [nextScreenButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:50]];
    } else {
        if (four) {
            nextScreenButton.frame = CGRectMake(width * 0.33, 290, width * 0.33, 50);
        } else {
            nextScreenButton.frame = CGRectMake(width * 0.33, 290, width * 0.33, 50);
        }
        [nextScreenButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:28]];
    }
    [nextScreenButton addTarget:self action:@selector(verifyWithServer) forControlEvents:UIControlEventTouchUpInside];
    [nextScreenButton setTitle:@"Login" forState:UIControlStateNormal];
    [nextScreenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextScreenButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    nextScreenButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    nextScreenButton.alpha = 0.0;
    nextScreenButton.layer.borderWidth = 1;
    nextScreenButton.layer.borderColor = [UIColor whiteColor].CGColor;
    nextScreenButton.layer.cornerRadius = nextScreenButton.frame.size.height/2;
    [self.view addSubview:nextScreenButton];
    
    UIButton *forgotPasswordButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (isIpad) {
        forgotPasswordButton.frame = CGRectMake(width/2 + 10, 300, width/2 -20, 75);
        // passwordTextField.font = [UIFont fontWithName:@"Helvetica" size:40];
    } else {
        if (four) {
            forgotPasswordButton.frame = CGRectMake(10, 350, width - 20, 50);
        } else {
            forgotPasswordButton.frame = CGRectMake(10, 350, width - 20, 50);
        }
        // passwordTextField.font = [UIFont fontWithName:@"Helvetica" size:24];
    }
    
    [forgotPasswordButton setTitle:@"Forgot Your Password?" forState:UIControlStateNormal];
    [forgotPasswordButton addTarget:self action:@selector(forgotPasswordButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:forgotPasswordButton];
    
    UIBarButtonItem *loginBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Login" style:UIBarButtonItemStylePlain target:self action:@selector(verifyWithServer)];
    self.navigationItem.rightBarButtonItem = loginBarButton;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (kDebug) {
        UIBarButtonItem *rob = [[UIBarButtonItem alloc] initWithTitle:@"RB" style:UIBarButtonItemStylePlain target:self action:@selector(loginAsRob)];
        UIBarButtonItem *paige = [[UIBarButtonItem alloc] initWithTitle:@"PR" style:UIBarButtonItemStylePlain target:self action:@selector(loginAsPaige)];
        UIBarButtonItem *everest = [[UIBarButtonItem alloc] initWithTitle:@"EA" style:UIBarButtonItemStylePlain target:self action:@selector(loginAsEverest)];
        UIBarButtonItem *zk = [[UIBarButtonItem alloc] initWithTitle:@"ZK" style:UIBarButtonItemStylePlain target:self action:@selector(loginAsZack)];
        self.navigationItem.leftBarButtonItems = @[rob, paige, everest, zk];
    }
}

-(void)loginAsRob {
    usernameTextField.text = @"rbentley";
    passwordTextField.text = @"rbentley";
    [self verifyWithServer];
}

-(void)loginAsPaige {
    usernameTextField.text = @"prunyon";
    passwordTextField.text = @"prunyon";
    [self verifyWithServer];
}

-(void)loginAsEverest {
    usernameTextField.text = @"eausterman";
    passwordTextField.text = @"eausterman";
    [self verifyWithServer];
}
                               
-(void)loginAsZack {
    usernameTextField.text = @"zkillingbech";
    passwordTextField.text = @"zkillingbech";
    [self verifyWithServer];
}

-(void)forgotPasswordButtonPressed{
    
    [self.navigationController pushViewController:[ForgotPasswordViewController new] animated:YES];

}

-(void)verifyWithServer {
    NSDictionary *params = @{@"username":usernameTextField.text, @"password":passwordTextField.text, @"client_id":kClientID};
    LoadingView *loadingView = [LoadingView addLoadingViewWithText:@"Loading"];
    [NetworkingHelper loginWithParams:params success:^(id responseObject) {
        [NetworkingHelper setNetworkingHelperOAuthToken:responseObject[@"access_token"]];
        [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"expires_in"] forKey:@"credExpiration"];
        [NetworkingHelper getSessionDataWithSuccess:^(id responseObject) {
            NSLog(@"responseObject:%@", responseObject);
            User *user = [[User alloc] initWithDictionary:responseObject];
            [user save];
            NSString *errorNumber = responseObject[@"error"];
            NSString *errorMessage = responseObject[@"error"];
            [loadingView hideActivityIndicator];
            
                if ([errorNumber isEqualToString:@"2"]) {
                    
                    [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Password is incorrect. Please try again."] animated:YES completion:NULL];
                    
                } else if (![errorNumber isEqualToString:@"1"]) {
                    [self nextScreenWithUser:user];
                } else if ([errorNumber isEqualToString:@"1"]) {
                    [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"Error: %@", errorMessage]] animated:YES completion:NULL];
                } else if (errorNumber) {
                    [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Unable to connect to the server. Please check your internet connection and try again."] animated:YES completion:NULL];
                }

            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                NSLog(@"failed to login because :%@", error);
                id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
                [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"%@", responseObject[@"error"]]] animated:YES completion:NULL];
                [loadingView hideActivityIndicator];
            }];

        
      } failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"error:%@", error);
          id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
          [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"%@", responseObject[@"error"]]] animated:YES completion:NULL];
          [loadingView hideActivityIndicator];
          NSLog(@"failed oAuth:%@", error.localizedDescription);
    }];
    
}

-(void)verifyFromDeepLinkingWithToken:(NSString *)token{
    [NetworkingHelper setNetworkingHelperOAuthToken:token];
    LoadingView *loadingView = [LoadingView addLoadingViewWithText:@"Loading"];
    [NetworkingHelper getSessionDataWithSuccess:^(id responseObject) {
        NSLog(@"responseObject:%@", responseObject);
        User *user = [[User alloc] initWithDictionary:responseObject];
        [user save];
        NSString *errorNumber = responseObject[@"error"];
        NSString *errorMessage = responseObject[@"error"];
        [loadingView hideActivityIndicator];
        if ([errorNumber isEqualToString:@"2"]) {
            
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Password is incorrect. Please try again."] animated:YES completion:NULL];
            
        } else if (![errorNumber isEqualToString:@"1"]) {
            [self nextScreenWithUser:user];
        } else if ([errorNumber isEqualToString:@"1"]) {
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"Error: %@", errorMessage]] animated:YES completion:NULL];
        } else if (errorNumber) {
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Unable to connect to the server. Please check your internet connection and try again."] animated:YES completion:NULL];
        }
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failed to login because :%@", error);
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"%@", responseObject[@"error"]]] animated:YES completion:NULL];
        [loadingView hideActivityIndicator];
    }];


}

-(void)nextScreenWithUser:(User *)currentUser {
    if (currentUser.userType == UserType_IsPCA) {
        if (currentUser.possibleRecipients.count == 0) {
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"You have no assigned recipients. Please contact system administrator."] animated:YES completion:NULL];
        } else {
            [self.navigationController pushViewController:[HomeViewController new] animated:YES];
        }
    } else {
        if (currentUser.signableSessions.count == 0) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Outstanding Time Entries" message:@"You have no time entries to sign at this time" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }]];
            
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];
        } else if (currentUser.signableSessions.count > 1) {
            ActiveTimesheetsCollectionViewController *vc = [[UIStoryboard storyboardWithName:@"RecipientTimesheets" bundle:nil] instantiateViewControllerWithIdentifier:@"ActiveTimesheetsCollectionViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            currentUser.currentSession = currentUser.signableSessions[0];
            [currentUser save];
            [self.navigationController pushViewController:[PDFPreviewViewController new] animated:YES];
        }
    }
}

#pragma mark - UITextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (usernameTextField.isFirstResponder) {
        [passwordTextField becomeFirstResponder];
    } else {
        if (usernameTextField.text.length == 0) {
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"You need to supply a username to be able to login."] animated:YES completion:NULL];
        } else if (passwordTextField.text.length == 0) {
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"You need to supply a password to be able to login."] animated:YES completion:NULL];
        } else {
            [self verifyWithServer];
        }
    }
    return YES;
}

-(void)textFieldDidChange {
    if (usernameTextField.text.length > 0 && passwordTextField.text.length > 0) {
        nextScreenButton.alpha = 1.0;
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        nextScreenButton.alpha = 0.0;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

@end
