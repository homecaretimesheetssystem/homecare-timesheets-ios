//
//  ErrorAlertController.h
//  Kindu
//
//  Created by Scott Mahonis on 10/21/15.
//  Copyright © 2015 The Jed Mahonis Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ErrorAlertController : UIAlertController

+(UIAlertController *)alertControllerWithMessage:(NSString *)message;

@end
