//
//  PDFCollectionViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/28/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PDFCollectionViewCell.h"

@interface PDFCollectionViewController : UICollectionViewController <CLLocationManagerDelegate, PDFCollectionViewCellDelegate>

@property (nonatomic, strong) User *currentUser;
@property (nonatomic) long startingIndex;
@property (nonatomic, strong) CLLocationManager *locationManager;

@end
