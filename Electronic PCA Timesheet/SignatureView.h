//
//  SignatureView.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/8/14.
//  Copyright (c) 2014 The Jed Mahonis Group, LLC. All rights reserved.
//
@protocol sigViewDelegate <NSObject>

-(void)enableButton;

@end

#import <UIKit/UIKit.h>

@interface SignatureView : UIView {
    id<sigViewDelegate>delegate;
}

@property (strong, nonatomic) id<sigViewDelegate>delegate;

@end
