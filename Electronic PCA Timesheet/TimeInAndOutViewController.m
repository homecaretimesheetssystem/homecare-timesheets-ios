//
//  TimeInAndOutViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 8/4/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "TimeInAndOutViewController.h"

@interface TimeInAndOutViewController () {
    NSDateFormatter *dateFormatter; BOOL shouldRetryRetrivingLocation; BOOL isForTimeInRetry;
}

@end

@implementation TimeInAndOutViewController

#pragma mark - View Delegate

- (void)viewDidLoad
{
    [super viewDidLoad];
    _timer = [NSTimer new];
    dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:kDateFormat];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(askForLocationPermission) name:@"askLocationPermission" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createSessionAfterVWA) name:UIApplicationDidBecomeActiveNotification object:nil];
    self.navigationItem.backBarButtonItem.title = @"";
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self createSessionAfterVWA];

}

-(void)createSessionAfterVWA{
    _currentUser = [User currentUser];
    _currentSession = _currentUser.currentSession;
    for (UIView *subViews in [self.view subviews]) {
        [subViews removeFromSuperview];
    }
    automaticTimeOut = NO;
    self.title = @"Timecard";
    isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    
    
    [self createUI];
    if (_currentSession.timeIn.timeIn && _currentSession.timeOut.timeOut && !self.navigationItem.rightBarButtonItem) {
        UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
        self.navigationItem.rightBarButtonItem = nextButton;
    }
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    long number = 5;
    NSLog(@"current session %@", _currentSession);
    _currentSession.continueTimesheetNumber = [NSNumber numberWithLong:number - 1];
    [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:@{@"continueTimesheetNumber":_currentSession.continueTimesheetNumber} andsuccess:^(id responseObject) {
                    NSLog(@"responseObject Timesheet:%@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"errorSavingSessionData:%@", error);
    }];
    if ([self.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES] || self.currentUser.possibleRecipients.count == 0) {
        self.locManager = [CLLocationManager new];
        self.locManager.delegate = self;
        self.locManager.desiredAccuracy = kCLLocationAccuracyBest;
        [self askForLocationPermission];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - User Interface

-(void)createUI {
    UIScrollView *scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [scroller setContentSize:CGSizeMake(width, scroller.frame.size.height + 250)];
    scroller.delegate = self;
    scroller.userInteractionEnabled = YES;
    scroller.delaysContentTouches = NO;
    scroller.showsHorizontalScrollIndicator = NO;
    scroller.showsVerticalScrollIndicator = NO;
    scroller.scrollEnabled = NO;
    scroller.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    [self.view addSubview:scroller];
    scroller.contentOffset = CGPointMake(0, -64);
    scroller.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"Timecard";
    titleLabel.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    if (isIpad) {
        titleLabel.frame = CGRectMake(0, 34, width, 50);
        titleLabel.font = [UIFont fontWithName:@"Helvetica" size:50];
    } else {
        if (four) {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
            titleLabel.frame = CGRectMake(0, 9, width, 28);
        } else {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:28];
            titleLabel.frame = CGRectMake(0, 14, width, 32);
        }
    }
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
//    [scroller addSubview:titleLabel];
    UIFont *font;
    
    timeInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    timeInButton.frame = CGRectMake(width * 0.1, titleLabel.frame.origin.y + titleLabel.frame.size.height + 50, width * 0.8, 60);
    if (isIpad) {
        font = [UIFont fontWithName:@"Helvetica" size:40];
        timeInButton.frame = CGRectMake((self.view.frame.size.width/2) - (self.view.frame.size.width/4), titleLabel.frame.origin.y + titleLabel.frame.size.height + 50, self.view.frame.size.width/2, 60);
    } else {
        font = [UIFont fontWithName:@"Helvetica" size:24];
    }
    [timeInButton addTarget:self action:@selector(saveTimeInButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [timeInButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    timeInButton.titleLabel.font = font;
    timeInButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    timeInButton.layer.borderWidth = 1;
    timeInButton.layer.borderColor = [UIColor whiteColor].CGColor;
    timeInButton.layer.cornerRadius = timeInButton.frame.size.height/2;
    [scroller addSubview:timeInButton];
    
    timeOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    timeOutButton.frame = CGRectMake(timeInButton.frame.origin.x, timeInButton.frame.origin.y + timeInButton.frame.size.height + 50, timeInButton.frame.size.width, timeInButton.frame.size.height);
    [timeOutButton addTarget:self action:@selector(verifyTimeOut) forControlEvents:UIControlEventTouchUpInside];
    timeOutButton.titleLabel.font = font;
    [timeOutButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    timeOutButton.alpha = 0.0;
    timeOutButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    timeOutButton.layer.borderWidth = 1;
    timeOutButton.layer.borderColor = [UIColor whiteColor].CGColor;
    timeOutButton.layer.cornerRadius = timeOutButton.frame.size.height/2;
    [scroller addSubview:timeOutButton];
    
    timerLabel = [UILabel new];
    timerLabel.frame = CGRectMake(20, timeOutButton.frame.origin.y + timeOutButton.frame.size.height + 50, scroller.frame.size.width - 40, 50);
    timerLabel.font = font;
    timerLabel.textColor = [UIColor whiteColor];
    timerLabel.alpha = 0.0;
    timerLabel.textAlignment = NSTextAlignmentCenter;
    [scroller addSubview:timerLabel];
    
    billableTimeLabel = [self billableTimeLabel];
    [scroller addSubview:billableTimeLabel];
    //    NSLog(@"_currentSession.timeIn:%@", _currentSession.timeIn);
    if (_currentSession.timeIn.timeIn) {
        self.navigationItem.hidesBackButton = YES;
        NSDateFormatter *format = [NSDateFormatter new];
        [format setTimeStyle:NSDateFormatterMediumStyle];
        NSString *buttonTitleString = [format stringFromDate:_currentSession.timeIn.timeIn];
        [timeInButton setTitle:[NSString stringWithFormat:@"Time In:  %@", buttonTitleString] forState:UIControlStateNormal];
        timeOutButton.alpha = 1.0;
        timeInButton.userInteractionEnabled = NO;
        timeInButton.layer.borderWidth = 0;
        //        NSLog(@"timeIn:%@", _currentSession.timeIn.timeIn);
        accumulatedSeconds = [[NSDate date] timeIntervalSinceDate:_currentSession.timeIn.timeIn];
        timerLabel.alpha = 1.0;
        if (!_currentSession.timeOut.timeOut && !_timer) {
            _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(updateTimerLabel:) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
        }
    } else {
        [timeInButton setTitle:@"Time In" forState:UIControlStateNormal];
    }
    [self checkForAutoTimeOut];
    if (_currentSession.timeOut.timeOut) {
        NSDateFormatter *format = [NSDateFormatter new];
        [format setTimeStyle:NSDateFormatterMediumStyle];
        NSString *buttonTitleString = [format stringFromDate:_currentSession.timeOut.timeOut];
        timeOutButton.userInteractionEnabled = NO;
        timeOutButton.layer.borderWidth = 0;
        [timeOutButton setTitle:[NSString stringWithFormat:@"Time Out:  %@", buttonTitleString] forState:UIControlStateNormal];
        [_timer invalidate];
        billableTimeLabel.alpha = 1.0;
        [self calculateTotalTime];
    } else {
        [timeOutButton setTitle:@"Time Out" forState:UIControlStateNormal];
    }
    //    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 479 - 64, width, 1)];
    //    line.backgroundColor = [UIColor whiteColor];
    //    [scroller addSubview:line];
    
    if (_currentSession.timeIn.timeIn && _currentSession.timeOut.timeOut) {
        UIBarButtonItem *next = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
        self.navigationItem.rightBarButtonItem = next;
    }
}

-(void)checkForAutoTimeOut {
    if (_currentSession.timeIn.timeIn && _currentSession.timeIn.estimatedShiftLenght) {
        NSTimeInterval interval = [_currentSession.timeIn.estimatedShiftLenght intValue];
        NSDate *timeOutDate = [_currentSession.timeIn.timeIn dateByAddingTimeInterval:interval];
        if ([[NSDate date] isLaterThanOrEqualTo:timeOutDate] && !_currentSession.timeOut.timeOut) {
            automaticTimeOut = YES;
            NSLog(@"should save time out");
            [self saveTimeOut];
        }
    }
}

-(UILabel *)billableTimeLabel {
    UILabel *newLabel = [UILabel new];
    newLabel.frame = CGRectMake(timerLabel.frame.origin.x, timerLabel.frame.origin.y + timerLabel.frame.size.height + 4, timerLabel.frame.size.width, timerLabel.frame.size.height);
    newLabel.font = timerLabel.font;
    newLabel.alpha = 0.0;
    newLabel.textColor = timerLabel.textColor;
    newLabel.textAlignment = timerLabel.textAlignment;
    return newLabel;
}

-(UIView *)estView {
    hoursArray = [self hoursArray];
    minutesArray = [NSArray arrayWithObjects:@"15", @"30", @"45", nil];
    hourString = hoursArray[0];
    minutesString = minutesArray[0];
    UIView *estView = [[UIView alloc] initWithFrame:self.view.frame];
    estView.backgroundColor = self.view.backgroundColor;
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"Shift Length";
    titleLabel.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    if (isIpad) {
        titleLabel.frame = CGRectMake(0, 98, width, 50);
        titleLabel.font = [UIFont fontWithName:@"Helvetica" size:50];
    } else {
        if (four) {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
            titleLabel.frame = CGRectMake(0, 73, width, 28);
        } else if ((MAX([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)) == 812.) {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
            titleLabel.frame = CGRectMake(0, 101, width, 28);
        } else {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:28];
            titleLabel.frame = CGRectMake(0, 78, width, 32);
        }
    }
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [estView addSubview:titleLabel];
    UITextField *tf = [UITextField new];
    if (isIpad) {
        tf.frame = CGRectMake(width * 0.2, 250, width * 0.6, 75);
        tf.font = [UIFont fontWithName:@"Helvetica" size:40];
    } else {
        if (four) {
            tf.frame = CGRectMake(width * 0.05, 140, width * 0.9, 50);
        } else {
            tf.frame = CGRectMake(width * 0.05, 175, width * 0.9, 50);
        }
        tf.font = [UIFont fontWithName:@"Helvetica" size:24];
    }
    UIPickerView *picker = [[UIPickerView alloc] init];
    picker.delegate = self;
    picker.dataSource = self;
    picker.backgroundColor = [UIColor whiteColor];
    UIView *aboveTFView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 44)];
    aboveTFView.backgroundColor = picker.backgroundColor;
    NSAttributedString *hrs = [[NSAttributedString alloc] initWithString:@"Hours" attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:18]}];
    NSAttributedString *mns = [[NSAttributedString alloc] initWithString:@"Minutes" attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:18]}];
    UILabel *hoursLab = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x - hrs.size.width + 2, 0, hrs.size.width, aboveTFView.frame.size.height)];
    hoursLab.attributedText = hrs;
    [picker addSubview:hoursLab];
    UILabel *minutesLab = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x + 10, 0, mns.size.width, aboveTFView.frame.size.height)];
    minutesLab.attributedText = mns;
    [picker addSubview:minutesLab];
    UIButton *nextB = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextB setTitle:@"Done" forState:UIControlStateNormal];
    [nextB setTitleColor:[UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0] forState:UIControlStateNormal];
    float bWidth = 50;
    [nextB setFrame:CGRectMake(aboveTFView.frame.size.width - bWidth - 10, 0, bWidth, aboveTFView.frame.size.height)];
    [nextB addTarget:self action:@selector(saveEstimatedTimeAndOpenCamera) forControlEvents:UIControlEventTouchUpInside];
    [aboveTFView addSubview:nextB];
    UIView *blackLine = [[UIView alloc] initWithFrame:CGRectMake(0, aboveTFView.frame.size.height - 1, aboveTFView.frame.size.width, 0.5)];
    blackLine.backgroundColor = [UIColor blackColor];
    [aboveTFView addSubview:blackLine];
    tf.inputView = picker;
    tf.inputAccessoryView = aboveTFView;
    tf.delegate = self;
    tf.textAlignment = NSTextAlignmentCenter;
    tf.backgroundColor = [UIColor whiteColor];
    tf.borderStyle = UITextBorderStyleBezel;
    tf.tag = 68;
    [estView addSubview:tf];
    [tf becomeFirstResponder];
    tf.text = [NSString stringWithFormat:@"%@:%@", hoursArray[0], minutesArray[0]];
    return estView;
}

#pragma mark - Data methods

-(NSArray *)hoursArray {
    NSMutableArray *arr = [NSMutableArray new];
    for (long i = 0; i < 25; i++) {
        NSString *string = [NSString stringWithFormat:@"%ld", i];
        [arr addObject:string];
    }
    return [NSArray arrayWithArray:arr];
}

-(void)saveCurrentSessionToCurrentUser {
    _currentUser.currentSession = _currentSession;
    [_currentUser save];
}

#pragma mark - Button Actions

-(void)saveTimeInButtonPressed {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeStyle:NSDateFormatterMediumStyle];
    
    NSDate *dateToSave = [NSDate date];
    NSString *dateString = [format stringFromDate:dateToSave];
    //    NSLog(@"dateString:%@ dateToSave:%@", dateString, dateToSave);
    accumulatedSeconds = 0;
    [UIView animateWithDuration:0.2 animations:^{
        timerLabel.alpha = 1.0;
        timeOutButton.alpha = 1.0;
        [timeInButton setTitle:dateString forState:UIControlStateNormal];
    }];
    _currentSession.timeIn.timeIn = [NSDate date];
    [self saveCurrentSessionToCurrentUser];
    timeInButton.userInteractionEnabled = NO;
    timeInButton.layer.borderWidth = 0;
    timeInButton.layer.borderColor = self.view.backgroundColor.CGColor;
    
    estmiatedShiftView = [self estView];
    [self.view addSubview:estmiatedShiftView];
    self.navigationItem.hidesBackButton = YES;
    //    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(saveEstimatedTimeAndOpenCamera)];
    //    self.navigationItem.rightBarButtonItem = rightItem;
}

-(void)saveEstimatedTimeAndOpenCamera {
    UITextField *tf = (UITextField *)[estmiatedShiftView viewWithTag:68];
    _currentSession.timeIn.estimatedShiftLenght = [self convertStringToSeconds:tf.text];
    [self saveCurrentSessionToCurrentUser];

    if ([_currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES] || _currentUser.possibleRecipients.count == 0) {
        [self geocodeCurrentLocationForTimeIn:YES];
    } else {
        self.currentSession.timeIn.address = @"";
        self.currentSession.timeIn.latitude = @0.0;
        self.currentSession.timeIn.longitude = @0.0;
        [self updateCurrentSession:self.currentSession isForTimeIn:YES];
    }
    [estmiatedShiftView removeFromSuperview];
    
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        return NO;
    }
    return YES;
    
}

-(void)verifyTimeOut {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Time Out" message:@"Are you sure you want to record your time out?" preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self saveTimeOut];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)saveTimeOut {
    NSDate *dateToSave;
    NSTimeInterval interval = [_currentSession.timeIn.estimatedShiftLenght intValue];
    NSDate *timeOutDate = [_currentSession.timeIn.timeIn dateByAddingTimeInterval:interval];
    if ([[NSDate date] isLaterThanOrEqualTo:timeOutDate]) {
        NSLog(@"YES");
        automaticTimeOut = YES;
    }
    
    if (automaticTimeOut == YES) {
        [_timer invalidate];
        dateToSave = [_currentSession.timeIn.timeIn dateByAddingTimeInterval:[_currentSession.timeIn.estimatedShiftLenght intValue]];
        [self showAutoTimeOutAlertWithTimeOutDate:dateToSave];
    } else {
        dateToSave = [NSDate date];
        [self saveTimeOutValueToCurrentSession:dateToSave];
    }
    
}

-(NSDateFormatter *)shortDateFormatter {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    return formatter;
}

-(NSDateFormatter *)timeAndDateFormatter {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    return formatter;
}

-(void)showAutoTimeOutAlertWithTimeOutDate:(NSDate *)timeOutDate {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Time Out" message:[NSString stringWithFormat:@"You have been clocked out at %@.", [[self timeAndDateFormatter] stringFromDate:timeOutDate]] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self saveTimeOutValueToCurrentSession:timeOutDate];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Edit" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self editTimeOutWithAutoSavedTimeOut:timeOutDate];
    }]];
    [self presentViewController:alert animated:YES completion:NULL];
}

-(void)editTimeOutWithAutoSavedTimeOut:(NSDate *)autoSavedTimeOut {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Edit Time Out" message:@"Enter your actual time out." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self showAnotherDamnAlertControllerWithAutoSavedTimeOut:autoSavedTimeOut];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSDate *editedDate = [[self timeAndDateFormatter] dateFromString:_editTimeTextField.text];
        [self saveTimeOutValueToCurrentSession:editedDate];
    }]];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.inputView = [self datePicker:autoSavedTimeOut];
        _editTimeTextField = textField;
        _editTimeTextField.text = [[self timeAndDateFormatter] stringFromDate:autoSavedTimeOut];
    }];
    [self presentViewController:alert animated:YES completion:NULL];
}

-(void)showAnotherDamnAlertControllerWithAutoSavedTimeOut:(NSDate *)autoSavedTimeOut {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:[NSString stringWithFormat:@"Are you sure you want to save your time out at %@?", [[self timeAndDateFormatter] stringFromDate:autoSavedTimeOut]] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self saveTimeOutValueToCurrentSession:autoSavedTimeOut];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Edit" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self editTimeOutWithAutoSavedTimeOut:autoSavedTimeOut];
    }]];
    [self presentViewController:alert animated:YES completion:NULL];
}

-(void)saveTimeOutValueToCurrentSession:(NSDate *)timeOut {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeStyle:NSDateFormatterMediumStyle];
    _currentSession.timeOut.timeOut = timeOut;
    
    _currentSession.dateEnding = [[self shortDateFormatter] stringFromDate:timeOut];
    [self saveDateEndingToSession];
    [self saveCurrentSessionToCurrentUser];
    NSLog(@"dateEnding:%@", _currentSession.dateEnding);
    NSString *dateString = [format stringFromDate:timeOut];
    billableTimeLabel.alpha = 1.0;
    timeOutButton.userInteractionEnabled = NO;
    timeOutButton.layer.borderWidth = 0;
    timeOutButton.layer.borderColor = self.view.backgroundColor.CGColor;
    [_timer invalidate];
    [self calculateTotalTime];
    [UIView animateWithDuration:0.2 animations:^{
        [timeOutButton setTitle:dateString forState:UIControlStateNormal];
    }];
    if ([self.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES] || _currentUser.possibleRecipients.count == 0) {
        [self.locManager startUpdatingLocation];
        [self geocodeCurrentLocationForTimeIn:NO];
    } else {
        self.currentSession.timeOut.address = @"";
        self.currentSession.timeOut.latitude = @0.0;
        self.currentSession.timeOut.longitude = @0.0;
        [self updateCurrentSession:self.currentSession isForTimeIn:NO];
    }
}

-(void)saveDateEndingToSession {
    [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:@{@"dateEnding":_currentSession.dateEnding} andsuccess:^(id responseObject) {
        //        NSLog(@"success updating session data");
        _currentUser.currentSession = _currentSession;
        [_currentUser save];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"failed to updated session data:%@", error);
    }];
}

-(UIDatePicker *)datePicker:(NSDate *)startingDate {
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    picker.date = startingDate;
    picker.minimumDate = _currentSession.timeIn.timeIn;
    picker.maximumDate = [NSDate date];
    picker.datePickerMode = UIDatePickerModeDateAndTime;
    [picker addTarget:self action:@selector(updateTextFieldText:) forControlEvents:UIControlEventValueChanged];
    return picker;
}

-(void)updateTextFieldText:(UIDatePicker *)sender {
    _editTimeTextField.text = [[self timeAndDateFormatter] stringFromDate:sender.date];
}

#pragma mark - Time calculation methods

-(void)updateTimerLabel:(NSTimer *)timer {
    long hours, minutes, seconds;
    accumulatedSeconds++;
    hours = accumulatedSeconds/3600;
    minutes = (accumulatedSeconds % 3600) / 60;
    seconds = (accumulatedSeconds % 3600) % 60;
    timerLabel.text = [NSString stringWithFormat:@"Total: %02ld:%02ld:%02ld", hours, minutes, seconds];
    [self checkForAutoTimeOut];
    if (_currentSession.timeOut.timeOut) {
        [_timer invalidate];
    }
}

-(void)calculateTotalTime {
    long hours, minutes, seconds;
    NSDateFormatter *format = [NSDateFormatter new];
    [format setTimeStyle:NSDateFormatterMediumStyle];
    [format setDateStyle:NSDateFormatterMediumStyle];
    NSString *inString = [format stringFromDate:_currentSession.timeIn.timeIn];
    NSString *outString = [format stringFromDate:_currentSession.timeOut.timeOut];
    NSDate *timeInDate = [format dateFromString:inString];
    NSDate *timeOutDate = [format dateFromString:outString];
    accumulatedSeconds = [timeOutDate timeIntervalSinceDate:timeInDate];
    hours = accumulatedSeconds/3600;
    minutes = (accumulatedSeconds % 3600) / 60;
    seconds = (accumulatedSeconds % 3600) % 60;
    timerLabel.text = [NSString stringWithFormat:@"Total Time: %2ld:%02ld:%02ld", hours, minutes, seconds];
    _currentSession.timeOut.totalHours = [self convertStringToSeconds:[NSString stringWithFormat:@"%2ld:%02ld:%02ld", hours, minutes, seconds]];
    //    long newInterval = roundl(accumulatedSeconds/900.0)*900.0; **** rounds up or down appropriately
    long newInterval = floorl(accumulatedSeconds/900.0)*900.0; /////floorl always rounds down
    hours = newInterval/3600;
    minutes = (newInterval % 3600) / 60;
    seconds = (newInterval % 3600) % 60;
    billableTimeLabel.text = [NSString stringWithFormat:@"Billable Time: %2ld:%02ld:%02ld", hours, minutes, seconds];
    _currentSession.timeOut.billableHours = [self convertStringToSeconds:[NSString stringWithFormat:@"%2ld:%02ld:%02ld", hours, minutes, seconds]];
    
    
}

#pragma Estimated Time Methods

-(NSNumber *)convertStringToSeconds:(NSString *)string {
    NSArray *numbersSeperatedByColonArray = [string componentsSeparatedByString:@":"];
    long totalSeconds;
    if (numbersSeperatedByColonArray.count == 2) {
        //String shows hours and minutes.
        long hours = [[NSString stringWithFormat:@"%@", numbersSeperatedByColonArray[0]] doubleValue];
        hours = hours * 60 * 60;
        long minutes = [[NSString stringWithFormat:@"%@", numbersSeperatedByColonArray[1]] doubleValue];
        minutes = minutes * 60;
        totalSeconds = hours + minutes;
    } else {
        long hours = [[NSString stringWithFormat:@"%@", numbersSeperatedByColonArray[0]] doubleValue];
        hours = hours * 60 * 60;
        long minutes = [[NSString stringWithFormat:@"%@", numbersSeperatedByColonArray[1]] doubleValue];
        minutes = minutes * 60;
        long seconds = [[NSString stringWithFormat:@"%@", numbersSeperatedByColonArray[2]] doubleValue];
        totalSeconds = hours + minutes + seconds;
    }
    //    NSLog(@"totalSeconds:%@", [NSNumber numberWithLong:totalSeconds]);
    
    return [NSNumber numberWithLong:totalSeconds];
}


#pragma mark - THIS IS WHERE THE NETWORK CALLS ARE SINCE I CAN NEVER FIND THEM

-(void)updateCurrentSession:(Session *)session isForTimeIn:(BOOL)isForTimeIn {
    _currentSession = session;
    [self saveCurrentSessionToCurrentUser];
    //    NSLog(@"currentSession.currentTimecard:%@", _currentSession.currentTimecard);
    __block NSDictionary *params = [NSDictionary new];
    if (isForTimeIn == YES) {
        //        self.navigationItem.hidesBackButton = YES;
        
        [NetworkingHelper createTimeInTableWithSuccess:^(id responseObject) {
            //            NSLog(@"create time in responseObject:%@", responseObject);
            _currentSession.timeIn.idNumber = responseObject[@"createdTimeIn"][@"id"];
            NSString *timeInString = [dateFormatter stringFromDate:_currentSession.timeIn.timeIn];
            
            if (_currentSession.timeIn.pictureTimestamp) {
                NSString *pictureTimeString = [dateFormatter stringFromDate:_currentSession.timeIn.pictureTimestamp];
                params = @{@"timeInAddress":_currentSession.timeIn.address, @"estimatedShiftLength":_currentSession.timeIn.estimatedShiftLenght, @"latitude":_currentSession.timeIn.latitude, @"longitude":_currentSession.timeIn.longitude, @"timeIn":timeInString, @"timeInPictureTime":pictureTimeString};
            } else {
                params = @{@"timeInAddress":_currentSession.timeIn.address, @"estimatedShiftLength":_currentSession.timeIn.estimatedShiftLenght, @"latitude":_currentSession.timeIn.latitude, @"longitude":_currentSession.timeIn.longitude, @"timeIn":timeInString};
            }
            //            NSLog(@"params:%@", params);
            
            [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:@{@"timeIn":_currentSession.timeIn.idNumber} andsuccess:^(id responseObject) {
                //                    NSLog(@"success Saving Time In key to session");
                [NetworkingHelper saveTimeInWithTimeInIdNumber:_currentSession.timeIn.idNumber andParams:params andsuccess:^(id responseObject) {
                    NSLog(@"save time in responseObject:%@", responseObject);
                    NSLog(@"seconds as a float:%f", [responseObject[@"seconds"] floatValue]);
                    if ([responseObject[@"seconds"] floatValue] > 144000.0) {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"This estimated shift will exceed 40 hours for the work week." preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
                        [self presentViewController:alert animated:YES completion:NULL];
                    }
                    if (!_currentSession.timeIn.pictureTimestamp) {
                        for (UIView *subViews in [self.view subviews]) {
                            [subViews removeFromSuperview];
                        }
                        [self createUI];
                        self.navigationItem.hidesBackButton = YES;
                    }
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
                    NSLog(@"error saving time in :%@", error);
                }];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
                NSLog(@"error saving time in key to session:%@", error);
            }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
            NSLog(@"Failed to create Time In Table:%@", error);
        }];
        
    } else {
        LoadingView *lv = [LoadingView addLoadingViewWithText:@"Saving"];
//        _currentSession.timeOut.billableHours = @34;
        [NetworkingHelper createTimeOutTableWithSuccess:^(id responseObject) {
            //            NSLog(@"create Time out table Respose:%@", responseObject);
            _currentSession.timeOut.idNumber = responseObject[@"createdTimeOut"][@"id"];
            NSString *timeOutString = [dateFormatter stringFromDate:_currentSession.timeOut.timeOut];
            if (_currentSession.timeOut.pictureTimestamp) {
                NSString *timeOutPictureString = [dateFormatter stringFromDate:_currentSession.timeOut.pictureTimestamp];
                params = @{@"timeOutAddress":_currentSession.timeOut.address, @"latitude":_currentSession.timeOut.latitude, @"longitude":_currentSession.timeOut.longitude, @"timeOut":timeOutString, @"timeOutPictureTime":timeOutPictureString, @"totalHours":_currentSession.timeOut.totalHours, @"billableHours":_currentSession.timeOut.billableHours};
            } else {
                params = @{@"timeOutAddress":_currentSession.timeOut.address, @"latitude":_currentSession.timeOut.latitude, @"longitude":_currentSession.timeOut.longitude, @"timeOut":timeOutString, @"totalHours":_currentSession.timeOut.totalHours, @"billableHours":_currentSession.timeOut.billableHours};
            }
            [NetworkingHelper saveTimeOutWithTimeOutIdNumber:_currentSession.timeOut.idNumber andParams:params andsuccess:^(id responseObject) {
                //                NSLog(@"success saving time out data :%@", responseObject);
                [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:@{@"timeOut":_currentSession.timeOut.idNumber} andsuccess:^(id responseObject) {
                    //                    NSLog(@"success saving timeOut Table key");
                    [lv hideActivityIndicator];
                    if (!_currentSession.timeOut.pictureTimestamp) {
                        for (UIView *subViews in [self.view subviews]) {
                            [subViews removeFromSuperview];
                        }
                        [self createUI];
                        self.navigationItem.hidesBackButton = YES;
                    }
                    UIBarButtonItem *next = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
                    self.navigationItem.rightBarButtonItem = next;
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
                    NSLog(@"error saving saving time out key:%@", error);
                    [lv hideActivityIndicator];
                }];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
                NSLog(@"error saving timeout data:%@", error);
                [lv hideActivityIndicator];
            }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
            NSLog(@"error creating time out table:%@", error);
            [lv hideActivityIndicator];
        }];
    }
}

#pragma mark - Picker Delegate

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return hoursArray.count;
    } else {
        return minutesArray.count;
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        return hoursArray[row];
    } else {
        return minutesArray[row];
    }
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return 60;
    } else {
        return 50;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    UITextField *tf = (UITextField *)[estmiatedShiftView viewWithTag:68];
    if (component == 0) {
        if (row == 0) {
            long selectedMinRow;
            minutesArray = [NSArray arrayWithObjects:@"15", @"30", @"45", nil];
            if ([minutesString isEqualToString:@"00"]) {
                minutesString = @"15";
                selectedMinRow = 0;
            } else if ([minutesString isEqualToString:@"15"]) {
                selectedMinRow = 0;
            } else if ([minutesString isEqualToString:@"30"]) {
                selectedMinRow = 1;
            } else {
                selectedMinRow = 2;
            }
            [pickerView reloadComponent:1];
            [pickerView selectRow:selectedMinRow inComponent:1 animated:NO];
        } else if (row == 24) {
            minutesArray = [NSArray arrayWithObjects:@"00", nil];
            minutesString = @"00";
            [pickerView reloadComponent:1];
        } else {
            if (minutesArray.count != 4) {
                long selectedMinRow;
                minutesArray = [NSArray arrayWithObjects:@"00", @"15", @"30", @"45", nil];
                [pickerView reloadComponent:1];
                if ([minutesString isEqualToString:@"15"]) {
                    selectedMinRow = 1;
                } else if ([minutesString isEqualToString:@"30"]) {
                    selectedMinRow = 2;
                } else if ([minutesString isEqualToString:@"45"]) {
                    selectedMinRow = 3;
                } else {
                    selectedMinRow = 0;
                }
                [pickerView selectRow:selectedMinRow inComponent:1 animated:NO];
            }
        }
    }
    switch (component) {
        case 0:
            hourString = hoursArray[row];
            break;
        case 1:
            minutesString = minutesArray[row];
            break;
    }
    tf.text = [NSString stringWithFormat:@"%@:%@", hourString, minutesString];
}

#pragma mark - Navigation

-(void)nextScreen {
    id vc;
    if ([self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"] || [self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
        vc = [ActivityInfoViewController new];
    } else {
        vc = [PDFPreviewViewController new];
    }
    _currentUser.currentSession = _currentSession;
    [_currentUser save];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - Location Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.locManager = manager;
    if (locations.count > 0) {
        [self.locManager stopUpdatingLocation];
    }
    if (shouldRetryRetrivingLocation == YES) {
        shouldRetryRetrivingLocation = NO;
        [self geocodeCurrentLocationForTimeIn:isForTimeInRetry];
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (shouldRetryRetrivingLocation == YES) {
        [self noLocationAlert:isForTimeInRetry];
    }
    
    NSLog(@"locationManager didFailWithError:%@", error);
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"locRequest"];
        [self askForLocationPermission];
    } else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locManager requestWhenInUseAuthorization];
    } else if (status == kCLAuthorizationStatusDenied || kCLAuthorizationStatusRestricted) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"locRequest"];
//        NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location" message:[NSString stringWithFormat:@"Homecare Timesheets needs your location to know where the signature took place. Please go to your phone settings and enable location services."] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            //            AddressDetailViewController *vc = [[UIStoryboard storyboardWithName:@"JobDetail" bundle:nil] instantiateViewControllerWithIdentifier:@"AddressDetailViewController"];
            //            vc.office = _assignment.office;
            //            [self.navigationController showViewController:vc sender:self];
            [self goToPhoneSettings];
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
    }
//    if (status == 3 || status == 4 || status == 5) {
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"locRequest"];
//        if ([_currentUser.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
//            [self.locManager startUpdatingLocation];
//        }
//    } else {
//        [self askForLocationPermission];
//    }
}

-(void)askForLocationPermission {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if ([CLLocationManager locationServicesEnabled] && (status == kCLAuthorizationStatusAuthorizedWhenInUse)) {
        [self.locManager startUpdatingLocation];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"locRequest"];
    } else if (status == kCLAuthorizationStatusNotDetermined && [CLLocationManager locationServicesEnabled]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"locRequest"];
        [self.locManager requestWhenInUseAuthorization];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"locRequest"];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Disabled" message:@"Homecare Timesheets needs your location to know where the signature took place. Please go to your phone settings and enable location services." preferredStyle:UIAlertControllerStyleAlert];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]) {
            [alertController addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self goToPhoneSettings];
                });
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self askForLocationPermission];
            }]];
        } else {
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self askForLocationPermission];
            }]];
        }
        [alertController.view setTintColor:[UIColor colorWithRed:1 green:128.0f/255.0f blue:0 alpha:1]];
        [self presentViewController:alertController animated:YES completion:^{
            NULL;
        }];
        
    }
}

-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}



-(void)geocodeCurrentLocationForTimeIn:(BOOL)isForTimeIn {
    CLGeocoder *geoCoder = [CLGeocoder new];
    if (self.locManager.location) {
        [geoCoder reverseGeocodeLocation:self.locManager.location completionHandler:^(NSArray *placemarks, NSError *error) {
            CLPlacemark *placeMark = placemarks[0];
            if (error) {
                NSLog(@"geocoding error:%@", error);
            }
            if (isForTimeIn == YES) {
                _currentSession.timeIn.address = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", [placeMark subThoroughfare], [placeMark thoroughfare], [placeMark locality], [placeMark administrativeArea], [placeMark postalCode]];
                _currentSession.timeIn.latitude = [NSNumber numberWithFloat:self.locManager.location.coordinate.latitude];
                _currentSession.timeIn.longitude = [NSNumber numberWithFloat:self.locManager.location.coordinate.longitude];
                if (![_currentSession.service.serviceName isEqualToString:@"Personal Care Service"] || [_currentSession.verificationIsRequiredForWorkerAndRecipient isEqualToNumber:[NSNumber numberWithBool:NO]]) {
                    [self updateCurrentSession:self.currentSession isForTimeIn:YES];
                } else {
                    [self saveCurrentSessionToCurrentUser];
                }
            } else {
                _currentSession.timeOut.address = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", [placeMark subThoroughfare], [placeMark thoroughfare], [placeMark locality], [placeMark administrativeArea], [placeMark postalCode]];
                _currentSession.timeOut.latitude = [NSNumber numberWithFloat:self.locManager.location.coordinate.latitude];
                _currentSession.timeOut.longitude = [NSNumber numberWithFloat:self.locManager.location.coordinate.longitude];
                [self updateCurrentSession:self.currentSession isForTimeIn:NO];
                
                //                NSLog(@"time out location is: %f, %f", self.locManager.location.coordinate.latitude, self.locManager.location.coordinate.longitude);
                
            }
            
        }];
        
    } else {
        [self noLocationAlert:isForTimeIn];
    }
}

-(void)noLocationAlert:(BOOL)isForTimeIn {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Retry" message:@"Location not found, please retry." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        isForTimeInRetry = isForTimeIn;
        shouldRetryRetrivingLocation = YES;
        [self.locManager startUpdatingLocation];
    }]];
    //    [alert addAction:[UIAlertAction actionWithTitle:@"Continue Without Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    //        if (isForTimeIn) {
    //            _currentSession.timeIn.address = @"N/A";
    //            _currentSession.timeIn.latitude = @0.000000;
    //            _currentSession.timeIn.longitude = @0.000000;
    //        } else {
    //            _currentSession.timeOut.address = @"N/A";
    //            _currentSession.timeOut.latitude = @0.000000;
    //            _currentSession.timeOut.longitude = @0.000000;
    //        }
    //        [self saveCurrentSessionToCurrentUser];
    //        if (isForTimeIn) {
    //            if (![_currentSession.service.serviceName isEqualToString:@"Personal Care Service"] || [_currentSession.verificationIsRequiredForWorkerAndRecipient isEqualToNumber:[NSNumber numberWithBool:NO]]) {
    //                [self updateCurrentSession:self.currentSession fromPhotoType:PhotoIsFor_TimeIn];
    //            }
    //        } else {
    //            [self updateCurrentSession:self.currentSession fromPhotoType:PhotoIsFor_TimeOut];
    //        }
    //    }]];
    [self presentViewController:alert animated:YES completion:NULL];
}

-(void)noLocationsAlert:(BOOL)isForTimeOut {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Retry" message:@"Location not found, please retry." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        isForTimeInRetry = isForTimeOut;
        shouldRetryRetrivingLocation = YES;
        [self.locManager startUpdatingLocation];
    }]];

    [self presentViewController:alert animated:YES completion:NULL];
}

-(void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager {
    NSLog(@"didPause");
}

@end
