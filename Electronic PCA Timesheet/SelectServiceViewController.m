//
//  SelectServiceViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/30/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "SelectServiceViewController.h"

@interface SelectServiceViewController () {
    UIPickerView *servicePicker;
}

@end

@implementation SelectServiceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    _currentUser = [User currentUser];
    _currentSession = _currentUser.currentSession;
    isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    UIBarButtonItem *next = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
    self.navigationItem.rightBarButtonItem = next;
    [self createUI];
    if (serviceTextField.text.length == 0) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    long number = 2;
    _currentSession.continueTimesheetNumber = [NSNumber numberWithLong:number - 1];
    [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:@{@"continueTimesheetNumber":_currentSession.continueTimesheetNumber} andsuccess:^(id responseObject) {
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failed to saveTimesheetNumber:%@", error);
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createUI {
    _possibleServicesArray = _currentUser.pca.servicesArray.count > 0 ? _currentUser.pca.servicesArray : _currentUser.agency.servicesArray;
    
    UILabel *selectServiceLabel = [[UILabel alloc] init];
    selectServiceLabel.textColor = [UIColor whiteColor];
    selectServiceLabel.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    if (isIpad) {
        selectServiceLabel.frame = CGRectMake(0, 150, width, 50);
        selectServiceLabel.font = [UIFont fontWithName:@"Helvetica" size:50];
    } else {
        selectServiceLabel.font = [UIFont fontWithName:@"Helvetica" size:28];
        if (four) {
            selectServiceLabel.frame = CGRectMake(0, 75, width, 50);
        } else {
            selectServiceLabel.frame = CGRectMake(0, 100, width, 50);
        }
    }
    selectServiceLabel.text = @"Select Service";
    selectServiceLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:selectServiceLabel];
    
    
    servicePicker = [[UIPickerView alloc] init];
    servicePicker.delegate = self;
    servicePicker.dataSource = self;
    servicePicker.backgroundColor = [UIColor whiteColor];
    
    serviceTextField = [[UITextField alloc] init];
    if (isIpad) {
        serviceTextField.frame = CGRectMake(width * 0.2, 250, width * 0.6, 75);
        serviceTextField.font = [UIFont fontWithName:@"Helvetica" size:40];
    } else {
        if (four) {
            serviceTextField.frame = CGRectMake(width * 0.05, 140, width * 0.9, 50);
        } else {
        serviceTextField.frame = CGRectMake(width * 0.05, 175, width * 0.9, 50);
        }
        serviceTextField.font = [UIFont fontWithName:@"Helvetica" size:24];
    }
    serviceTextField.delegate = self;
    serviceTextField.inputView = servicePicker;
    serviceTextField.adjustsFontSizeToFitWidth = YES;
    serviceTextField.backgroundColor = [UIColor whiteColor];
    serviceTextField.borderStyle = UITextBorderStyleBezel;
    serviceTextField.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:serviceTextField];
    if (_currentSession.service.serviceName) {
        serviceTextField.text = _currentSession.service.serviceName;
        _selectedService = _currentSession.service;
        for (int i = 0; i < _currentUser.pca.servicesArray.count; i++) {
            Service *possService = (Service*)_currentUser.pca.servicesArray[i];
            if ([possService.idNumber isEqualToNumber:_selectedService.idNumber]) {
                arrayRow = i;
            }
        }
    }
    if (!arrayRow) {
        arrayRow = 0;
    }
    nextScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (isIpad) {
        nextScreenButton.frame = CGRectMake(width * 0.4, 400, width * 0.2, 50);
        [nextScreenButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:50]];
    } else {
        if (four) {
            nextScreenButton.frame = CGRectMake(width * 0.1, 220, width * 0.8, 60);
        } else {
        nextScreenButton.frame = CGRectMake(width * 0.1, 280, width * 0.8, 60);
        }
        [nextScreenButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:28]];
    }
    [nextScreenButton addTarget:self action:@selector(nextScreen) forControlEvents:UIControlEventTouchUpInside];
    [nextScreenButton setTitle:@"Next" forState:UIControlStateNormal];
    [nextScreenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextScreenButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    nextScreenButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    nextScreenButton.layer.borderWidth = 1;
    nextScreenButton.layer.borderColor = [UIColor whiteColor].CGColor;
    nextScreenButton.layer.cornerRadius = nextScreenButton.frame.size.height/2;
    [self.view addSubview:nextScreenButton];
    if (serviceTextField.text.length == 0) {
        nextScreenButton.alpha = 0.0;
    } else {
        nextScreenButton.alpha = 1.0;
    }
}

-(void)nextScreen {
    
    [self.view endEditing:YES];
    LoadingView *lv = [LoadingView addLoadingViewWithText:@"Saving"];
    [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:@{@"service":_selectedService.idNumber, @"pca":_currentUser.pca.idNumber} andsuccess:^(id responseObject) {
        [lv hideActivityIndicator];
        _currentSession.service = (Service*)_possibleServicesArray[arrayRow];
        _currentUser.currentSession = _currentSession;
        [_currentUser save];
            if (self.selectedService.serviceNumberdescr == ServiceNumberDescr_PCA || self.selectedService.serviceNumberdescr == ServiceNumberDescr_Respite || self.selectedService.serviceNumberdescr == ServiceNumberDescr_ConsumerSupport || self.selectedService.serviceNumberdescr == ServiceNumberDescr_PersonalAssistance || self.selectedService.serviceNumberdescr == ServiceNumberDescr_TreatmentAndTraining) {
                if (_currentUser.possibleRatios.count > 0) {
                    [self.navigationController showViewController:[RatioSharedCareViewController new] sender:self];
                } else {
                    [self doesntNeedARatio];
                }
                
            } else {
                [self doesntNeedARatio];
            }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [lv hideActivityIndicator];
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"%@", responseObject[@"error"]]] animated:YES completion:NULL];

//        [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"Error Saving Time Entry Info:%@", error.localizedDescription]] animated:YES completion:NULL];
        NSLog(@"Service Screen Time Entry error:%@", error.localizedDescription);
    }];
}

-(void)doesntNeedARatio {
    // If a ratio exists at this point, we reset it in case the service selected doesn't require one
    if (_currentUser.currentSession.ratio) {
        _currentUser.currentSession.ratio = [Ratio new];
        [_currentUser save];
    }
    if (_currentUser.possibleRecipients.count > 1) {
        ChooseUserViewController *vc2 = [[ChooseUserViewController alloc] init];
        [self.navigationController pushViewController:vc2 animated:YES];
    } else {
        if (_currentUser.possibleRecipients.count > 0) {
            return [self saveRecipientToCurrentSession];
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"You have no assigned recipients. Please contact system administrator."] animated:YES completion:NULL];
        }
    }
}

-(void)saveRecipientToCurrentSession {
    _currentSession.currentTimecard.recipient = (Recipient *)_currentUser.possibleRecipients[0];
    _currentUser.currentSession = _currentSession;
    [_currentUser save];
    [NetworkingHelper saveTimesheetDataWithTimesheetID:_currentSession.currentTimecard.idNumber andParams:@{@"recipient": _currentSession.currentTimecard.recipient.idNumber} andsuccess:^(id responseObject) {
        // The new call to check if verification comes in
        [NetworkingHelper checkVerificationForCamera:@{@"pcaid":_currentUser.pca.idNumber, @"recipientid":_currentSession.currentTimecard.recipient.idNumber} success:^(id responseObject) {
            if ([self string:self.currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
                if ([responseObject[@"verification_needed"] boolValue] == YES) {
                    _currentSession.verificationID = responseObject[@"id"];
                    _currentSession.verificationIsRequiredForWorkerAndRecipient = @YES;
                    _currentUser.currentSession = _currentSession;
                    [_currentUser save];
                    [self showTheCamera];
                }
                else
                {
                    [self showNextScreenThatIsntTheCamera];
                }
            }
            else
            {
                [self showNextScreenThatIsntTheCamera];
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"Failed to check verification:%@", error);
            [CrashlyticsKit recordError:error];
            [self showNextScreenThatIsntTheCamera];
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Failed to save recipient and pca:%@", error);
    }];
}

-(void)showNextScreenThatIsntTheCamera {
    id vc;
    if ([_currentSession.currentTimesheetNumber isEqualToNumber:@1]) {
        vc = [TimeInAndOutViewController new];
    }
    else
    {
        if ([self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"] || [self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
            vc = [ActivityInfoViewController new];
        } else {
            vc = [PDFPreviewViewController new];
        }
    }
    [self.navigationController showViewController:vc sender:self];;
}

-(void)showTheCamera {
    [self getCameraPermissions:nil];
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        return NO;
    }
    return YES;
}

#pragma mark - TextField

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [servicePicker selectRow:arrayRow inComponent:0 animated:NO];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    Service *service = _possibleServicesArray[arrayRow];
    textField.text = service.serviceName;
    _selectedService = service;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationItem.rightBarButtonItem.enabled = YES;
        nextScreenButton.alpha = 1.0;
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
/*
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITextView *textview in [self.view subviews]) {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
//            [textview resignFirstResponder];
        }
    }
    
} */

#pragma mark - PickerView

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_possibleServicesArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    Service *service = (Service *)_possibleServicesArray[row];
    return service.serviceName;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    arrayRow = row;
    Service *service = (Service *)_possibleServicesArray[row];
    serviceTextField.text = service.serviceName;
    _selectedService = service;
}

#pragma mark - Camera Methods

-(void)openProfilePictureCamera {
    
    JMGCameraViewController *camera = [JMGCameraViewController new];
    camera.parentNavBarStyle = self.navigationController.navigationBar.barStyle;
    camera.parentStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
    camera.cameraMode = CameraMode_square;
    camera.faceFrontOnLaunch = YES;
    // If no value for File type is specified, default will be png. Other available fileType is jpg
    camera.fileType = FileType_jpg;
    camera.showWhiteCircleInSquareMode = NO;
    camera.showPictureFromCameraRollForChooseFromPhotoLibraryButton = NO;
    
    // Add some text if you want to add a title above the square crop camera. If nil, no label will be added.
    camera.titleAboveCamera = @"Verification Picture";
    
    camera.cameraBackgroundColor = [UIColor colorWithWhite:0.05 alpha:1.0];
    
    // cameraMode Property must be set to custom for this to work.
    //    camera.fileSize = CGSizeMake(512., 1024.);
    if (!self.navigationController) {
        [self presentViewController:camera animated:YES completion:NULL];
    } else {
        [self.navigationController showViewController:camera sender:self];
    }
}

-(void)getCameraPermissions:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openProfilePictureCamera];
        });
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getCameraPermissions:sender];
                });
                
            }
            
        }];
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined) {
        if  (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [self getPhotoLibraryPermissions];
        }
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) {
        [self showCameraDeniedAlert];
    } else {
        [self showCameraDeniedAlert];
    }
    
}

-(void)showCameraDeniedAlert {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Camera Access Denied" message:[NSString stringWithFormat:@"We can't access your camera. To give us access, go to Settings -> %@ and allow camera permission to use this feature.", [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"]] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goToPhoneSettings];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)getPhotoLibraryPermissions {
    
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openProfilePictureCamera];
        });
    } else if (status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            } else if ([self cantAccessCameraOrPhotoLibrary]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showAccessAlert];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            }
        }];
    } else if ([self cantAccessCameraOrPhotoLibrary]) {
        [self showAccessAlert];
    }
}

-(BOOL)cantAccessCameraOrPhotoLibrary {
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) && (status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)) {
        return YES;
    }
    return false;
}

-(void)showAccessAlert {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Photo Library Access Denied" message:[NSString stringWithFormat:@"We can't access your camera or photo library. To give us access, go to Settings -> %@ and allow access to camera or photos to use this feature.", [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey]] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goToPhoneSettings];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}

@end
