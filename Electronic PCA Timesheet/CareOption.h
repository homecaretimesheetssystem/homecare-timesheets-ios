//
//  CareOption.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/2/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CareOption : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *idNumber;
@property (nonatomic) BOOL isIADL;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
