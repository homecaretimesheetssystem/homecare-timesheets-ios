//
//  ChooseUserViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/22/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "ChooseUserViewController.h"
#import "PDFPreviewViewController.h"
#import <Crashlytics/Crashlytics.h>

@interface ChooseUserViewController ()

@end

@implementation ChooseUserViewController
@synthesize currentSession;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _currentUser = [User currentUser];
    currentSession = _currentUser.currentSession;
    [self destroyUI];
    isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    self.title = @"Select Recipient";
    [self initializeTimecard];
    [self createUI];
    if (self.currentUser.currentSession.currentTimesheetNumber) {
        self.navigationItem.hidesBackButton = ![self.currentUser.currentSession.currentTimesheetNumber isEqualToNumber:@1];
    }
    
}

-(void)initializeTimecard {
    if (!currentSession.currentTimesheetNumber) {
        currentSession.currentTimesheetNumber = @1;
    }
}

-(void)destroyUI {
    for (UIView *subview in [self.view subviews]) {
        [subview removeFromSuperview];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    long number = 4;
    if (_shouldStartNewTimesheet == YES) {
        recipientTextField.text = nil;
        [NetworkingHelper createNewTimesheetTableWithSuccess:^(id responseObject) {
            currentSession.currentTimecard = [Timecard new];
            currentSession.currentTimecard.idNumber = responseObject[@"createdTimesheet"][@"id"];
            currentSession.currentTimesheetID = responseObject[@"createdTimesheet"][@"id"];
            if (![currentSession.timesheetIDsArray containsObject:currentSession.currentTimesheetID]) {
                [currentSession.timesheetIDsArray addObject:currentSession.currentTimesheetID];
            }
            [NetworkingHelper createFileAndSuccess:^(id responseObject) {
                currentSession.currentTimecard.file = [File new];
                currentSession.currentTimecard.file.idNumber = responseObject[@"createdFilesId"];
                [NetworkingHelper saveTimesheetDataWithTimesheetID:currentSession.currentTimesheetID andParams:@{@"sessionData":currentSession.idNumber} andsuccess:^(id responseObject) {
//                        NSLog(@"Successfully created and updated timesheet with new session data");
                    currentSession.continueTimesheetNumber = [NSNumber numberWithLong:number - 1];
                    [NetworkingHelper saveSessionDataWithSessionNumber:currentSession.idNumber andParams:@{@"continueTimesheetNumber":currentSession.continueTimesheetNumber, @"currentTimesheetNumber":currentSession.currentTimesheetNumber} andsuccess:^(id responseObject) {
//                            NSLog(@"responseObject:%@", responseObject);
                        _currentUser.currentSession = currentSession;
                        [_currentUser save];
                        
                    } failure:^(NSURLSessionDataTask *task, NSError *error) {
                        NSLog(@"errorSavingSessionData:%@", error);
                    }];
                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                    NSLog(@"failed to update new timesheet error:%@", error);
                }];
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                NSLog(@"failed to create file table for new timecard:%@", error);
            }];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"failed to create new timesheet error:%@", error);
        }];
    }
    if (arrayRowRecipient) {
        [recipientPicker reloadAllComponents];
        [recipientPicker selectRow:arrayRowRecipient inComponent:0 animated:NO];
    }
}

-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)createUI {
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"Select Recipient";
    if (isIpad) {
        titleLabel.frame = CGRectMake(0, 34, width, 50);
        titleLabel.font = [UIFont fontWithName:@"Helvetica" size:50];
    } else {
        if (four) {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
            titleLabel.frame = CGRectMake(0, 73, width, 28);
        } else if ((MAX([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)) == 812.) {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:28];
            titleLabel.frame = CGRectMake(0, 200, width, 32);
            NSLog(@"isIphoneX");
        } else {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:28];
            titleLabel.frame = CGRectMake(0, 78, width, 32);
        }
    }
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    // [scroller addSubview:titleLabel];
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:singleTap];
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, width, 50)];
    pickerToolbar.opaque = NO;
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissKeyboard)];
    NSArray *itemsArray = [NSArray arrayWithObjects:flex, doneButton, nil];
    pickerToolbar.items = itemsArray;
    
    recipientArray = _currentUser.possibleRecipients;
    recipientPicker = [[UIPickerView alloc] init];
    recipientPicker.delegate = self;
    recipientPicker.dataSource = self;
    
    recipientTextField = [[UITextField alloc] init];
    if (isIpad) {
        recipientTextField.frame = CGRectMake(width * 0.5, 139, width * 0.4, 75);
        recipientTextField.font = [UIFont fontWithName:@"Helvetica" size:40];
    } else {
        if (four) {
            recipientTextField.frame = CGRectMake(width * 0.15, 74, width * 0.7, 50);
        }  else if ((MAX([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)) == 812.) {
            recipientTextField.frame = CGRectMake(width * 0.15, 152, width * 0.7, 50);
        } else {
            recipientTextField.frame = CGRectMake(width * 0.15, 124, width * 0.7, 50);
        }
        recipientTextField.font = [UIFont fontWithName:@"Helvetica" size:24];
    }
    recipientTextField.delegate = self;
    recipientTextField.inputView = recipientPicker;
    recipientTextField.inputAccessoryView = pickerToolbar;
    recipientTextField.adjustsFontSizeToFitWidth = YES;
    recipientTextField.backgroundColor = [UIColor whiteColor];
    recipientTextField.borderStyle = UITextBorderStyleBezel;
    recipientTextField.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:recipientTextField];
    
//    NSLog(@"recipient:%@", currentSession.currentTimecard.recipient);
    if (currentSession.currentTimecard.recipient.idNumber && _shouldStartNewTimesheet == NO) {
        recipientTextField.text = [NSString stringWithFormat:@"%@ %@", currentSession.currentTimecard.recipient.firstName, currentSession.currentTimecard.recipient.lastName];
        Recipient *recipient = currentSession.currentTimecard.recipient;
        for (long i = 0; i < recipientArray.count; i++) {
            if ([recipient.idNumber isEqualToNumber:((Recipient *)recipientArray[i]).idNumber]) {
                arrayRowRecipient = i;
            }
        }
        
    }
    
    
    UILabel *recipientLabel = [[UILabel alloc] init];
    recipientLabel.backgroundColor = [UIColor clearColor];
    recipientLabel.text = @"Recipient:";
    if (isIpad) {
        recipientLabel.font = [UIFont fontWithName:@"Helvetica" size:40];
        recipientLabel.frame = CGRectMake(0, recipientTextField.frame.origin.y, recipientTextField.frame.origin.x - self.view.frame.size.width * 0.1, recipientTextField.frame.size.height);
    } else {
        if (four) {
            recipientLabel.font = [UIFont fontWithName:@"Helvetica" size:21];
        }else {
            recipientLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
        }
        recipientLabel.frame = CGRectMake(0, recipientTextField.frame.origin.y - 40, width, 30);
    }
    recipientLabel.textColor = [UIColor whiteColor];
    recipientLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:recipientLabel];
    
  
    
    recipientTextField.placeholder = @"Required";

    nextScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (isIpad) {
        nextScreenButton.frame = CGRectMake(width * 0.4, recipientTextField.frame.origin.y + recipientTextField.frame.size.height + 75, width * 0.2, 60);
        [nextScreenButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:50]];
    } else {
        if (four) {
            nextScreenButton.frame = CGRectMake(width * 0.1, recipientTextField.frame.origin.y + recipientTextField.frame.size.height + 35, width * 0.8, 60);
        } else {
            nextScreenButton.frame = CGRectMake(width * 0.1, recipientTextField.frame.origin.y + recipientTextField.frame.size.height + 35, width * 0.8, 60);
        }
        [nextScreenButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:28]];
    }
    [nextScreenButton addTarget:self action:@selector(nextScreen) forControlEvents:UIControlEventTouchUpInside];
    [nextScreenButton setTitle:@"Next" forState:UIControlStateNormal];
    [nextScreenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextScreenButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    nextScreenButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    nextScreenButton.layer.borderWidth = 1;
    nextScreenButton.layer.borderColor = [UIColor whiteColor].CGColor;
    nextScreenButton.layer.cornerRadius = nextScreenButton.frame.size.height/2;
    [self.view addSubview:nextScreenButton];
    if (recipientTextField.text.length == 0) {
        nextScreenButton.alpha = 0.0;
    } else {
        nextScreenButton.alpha = 1.0;
        UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
        self.navigationItem.rightBarButtonItem = nextButton;
    }
}



-(void)nextScreen {
    if (recipientTextField.text.length == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required" message:@"Please select a recipient." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
        [self presentViewController:alert animated:YES completion:NULL];
    } else {
        [self.view endEditing:YES];
//        NSLog(@"recipient:%@", (Recipient*)recipientArray[arrayRowRecipient]);
        LoadingView *lv = [LoadingView addLoadingViewWithText:@"Saving"];
        currentSession.currentTimecard.recipient = (Recipient*)recipientArray[arrayRowRecipient];
        
        [NetworkingHelper saveTimesheetDataWithTimesheetID:currentSession.currentTimecard.idNumber andParams:@{@"recipient": currentSession.currentTimecard.recipient.idNumber} andsuccess:^(id responseObject) {
//            NSLog(@"successfully saved recipient and pca:%@", responseObject);
            [lv hideActivityIndicator];
            // The new call to check if verification comes in
            [NetworkingHelper checkVerificationForCamera:@{@"pcaid":_currentUser.pca.idNumber, @"recipientid":currentSession.currentTimecard.recipient.idNumber} success:^(id responseObject) {
                NSLog(@"thisResponse object:%@", responseObject);
                if ([self string:self.currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
                    
                    if ([responseObject[@"verification_needed"] boolValue] == YES) {
                        currentSession.verificationID = responseObject[@"id"];
                        currentSession.verificationIsRequiredForWorkerAndRecipient = @YES;
                        NSLog(@"ThisVerificationID is :%@", currentSession.verificationID);
                        _currentUser.currentSession = currentSession;
                        [_currentUser save];
                        [self showTheCamera];
                    }
                    else
                    {
                        NSLog(@"currentTimesheetNumber:%@", currentSession.currentTimesheetNumber);
                        [self showNextScreenThatIsntTheCamera];
                    }
                }
                else
                {
                    [self showNextScreenThatIsntTheCamera];
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                NSLog(@"Failed to check verification:%@", error);
                [CrashlyticsKit recordError:error];
                [self showNextScreenThatIsntTheCamera];
                [lv hideActivityIndicator];
            }];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"Failed to save recipient and pca:%@", error);
            [lv hideActivityIndicator];
        }];
    }
    
}

-(void)showNextScreenThatIsntTheCamera {
    id vc;
    if ([currentSession.currentTimesheetNumber isEqualToNumber:@1]) {
        vc = [TimeInAndOutViewController new];
    }
    else
    {
        if ([self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"] || [self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
            vc = [ActivityInfoViewController new];
        } else {
            vc = [PDFPreviewViewController new];
        }
    }
    _currentUser.currentSession = currentSession;
    [_currentUser save];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)showTheCamera {
    [self getCameraPermissions:nil];
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
//        NSLog(@"return NO");
        return NO;
    }
    return YES;
}

#pragma mark - UITextField Delegate

-(void)prevTextField:(UITextField *)textField {
    if (recipientTextField.isFirstResponder) {
        [self.view endEditing:YES];
    } else if (recipientNameTextField.isFirstResponder) {
        [self dismissKeyboard];
    } else if (recipientLastNameTextField.isFirstResponder) {
        [recipientNameTextField becomeFirstResponder];
    }  else if (maNumberTextField.isFirstResponder) {
        [recipientLastNameTextField becomeFirstResponder];
    }
}

-(void)nextTextField {
    if (recipientTextField.isFirstResponder) {
        [self.view endEditing:YES];
    } else if (recipientNameTextField.isFirstResponder) {
        [recipientLastNameTextField becomeFirstResponder];
    } else if (recipientLastNameTextField.isFirstResponder) {
        [maNumberTextField becomeFirstResponder];
    } else if (maNumberTextField.isFirstResponder) {
        [self.view endEditing:YES];
    }else {
        [self.view endEditing:YES];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == recipientTextField) {
        if (recipientArray.count > 0) {
            if (!arrayRowRecipient) {
                arrayRowRecipient = 0;
            } else {
                [recipientPicker reloadAllComponents];
                [recipientPicker selectRow:arrayRowRecipient inComponent:0 animated:YES];
            }
            textField.text = [NSString stringWithFormat:@"%@ %@", ((Recipient*)[recipientArray objectAtIndex:arrayRowRecipient]).firstName, ((Recipient*)[recipientArray objectAtIndex:arrayRowRecipient]).lastName];
        } else {
            return NO;
        }
        
    }
    
    if (recipientTextField.text.length > 0 && /*pcaTextField.text.length > 0 && !createPCAView &&*/ !createRecipientView) {
        UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
        self.navigationItem.rightBarButtonItem = nextButton;
        nextScreenButton.alpha = 1.0;
    }
    
    if (textField == recipientLastNameTextField) {
        
        [UIView animateWithDuration:0.2 animations:^{
            if (four) {
                scroller.contentOffset = CGPointMake(0, 05);
            } else if (isIpad) {
                scroller.contentOffset = CGPointMake(0, 20);
            } else {
                scroller.contentOffset = CGPointMake(0, -40);
            }
            
        }];
    } else if (textField == maNumberTextField) {
        [UIView animateWithDuration:0.2 animations:^{
            if (four) {
                scroller.contentOffset = CGPointMake(0, 95);
            } else if (isIpad) {
                scroller.contentOffset = CGPointMake(0, 140);
            } else {
                scroller.contentOffset = CGPointMake(0, 50);
            }
        }];
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITextView *textview in [self.view subviews]) {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [textview resignFirstResponder];
        }
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == recipientTextField) {
        [self.view endEditing:YES];
    } else {
        [self nextTextField];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == maNumberTextField) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 8) ? NO : YES;
    } else {
        return YES;
    }
}

#pragma mark - PickerView Delegate 

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
        return recipientArray.count;
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title = [NSString stringWithFormat:@"%@ %@", ((Recipient*)[recipientArray objectAtIndex:row]).firstName, ((Recipient*)[recipientArray objectAtIndex:row]).lastName];
    return title;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView == recipientPicker) {
        recipientTextField.text = [NSString stringWithFormat:@"%@ %@", ((Recipient*)[recipientArray objectAtIndex:row]).firstName, ((Recipient*)[recipientArray objectAtIndex:row]).lastName];
        arrayRowRecipient = row;
    }
}

#pragma mark - Camera Methods

-(void)openProfilePictureCamera {
    
    JMGCameraViewController *camera = [JMGCameraViewController new];
    camera.parentNavBarStyle = self.navigationController.navigationBar.barStyle;
    camera.parentStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
    camera.cameraMode = CameraMode_square;
    camera.faceFrontOnLaunch = YES;
    // If no value for File type is specified, default will be png. Other available fileType is jpg
    camera.fileType = FileType_jpg;
    camera.showWhiteCircleInSquareMode = NO;
    camera.showPictureFromCameraRollForChooseFromPhotoLibraryButton = NO;
    
    // Add some text if you want to add a title above the square crop camera. If nil, no label will be added.
    camera.titleAboveCamera = @"Verification Picture";
    
    camera.cameraBackgroundColor = [UIColor colorWithWhite:0.05 alpha:1.0];
    
    // cameraMode Property must be set to custom for this to work.
    //    camera.fileSize = CGSizeMake(512., 1024.);
    if (!self.navigationController) {
        [self presentViewController:camera animated:YES completion:NULL];
    } else {
        [self.navigationController showViewController:camera sender:self];
    }
}

-(void)getCameraPermissions:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openProfilePictureCamera];
            //            [self getPhotoLibraryPermissions];
        });
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getCameraPermissions:sender];
                });
                
            }
            
        }];
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined) {
        if  (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [self getPhotoLibraryPermissions];
        }
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) {
        [self showCameraDeniedAlert];
    } else {
        [self showCameraDeniedAlert];
    }
    
}

-(void)showCameraDeniedAlert {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Camera Access Denied" message:[NSString stringWithFormat:@"We can't access your camera. To give us access, go to Settings -> %@ and allow camera permission to use this feature.", [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"]] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goToPhoneSettings];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)getPhotoLibraryPermissions {
    
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openProfilePictureCamera];
        });
    } else if (status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            } else if ([self cantAccessCameraOrPhotoLibrary]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showAccessAlert];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            }
        }];
    } else if ([self cantAccessCameraOrPhotoLibrary]) {
        [self showAccessAlert];
    }
}

-(BOOL)cantAccessCameraOrPhotoLibrary {
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) && (status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)) {
        return YES;
    }
    return false;
}

-(void)showAccessAlert {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Photo Library Access Denied" message:[NSString stringWithFormat:@"We can't access your camera or photo library. To give us access, go to Settings -> %@ and allow access to camera or photos to use this feature.", [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey]] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goToPhoneSettings];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}

@end
