//
//  CareLocation.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/27/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CareLocation : NSObject <NSCopying, NSCoding>

@property (strong, nonatomic) NSNumber *idNumber;
@property (strong, nonatomic) NSString *sharedCareLocation;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
