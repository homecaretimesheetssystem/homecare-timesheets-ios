//
//  PDFPreviewViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 12/5/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "PDFPreviewViewController.h"
#import "ErrorAlertController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/UIImage+AFNetworking.h>
#import "HomeViewController.h"

@interface PDFPreviewViewController () {
    NSString *rAddressString; NSString *pAddressString; CLGeocoder *geoCoder; UIImage *pcaSigImage;
    NSData *recipientSignature; NSString *recipientSignatureDate; BOOL shouldAutoSignRecip;
}

@end

@implementation PDFPreviewViewController
@synthesize timeCardArray, currentSession;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Review Time Entry";
    self.navigationItem.hidesBackButton = YES;
    
//    [self resetSignatureDefaults];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _currentUser = [User currentUser];
    currentSession = _currentUser.currentSession;
    [self createTimeCardFile];
    isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
    geoCoder = [[CLGeocoder alloc] init];
    [self figureOutDayOfWeekAndSetArray];
    
    if (!webView) {
        webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, width, height - 64 - 75)];
        webView.scalesPageToFit = YES;
        webView.scrollView.contentOffset = CGPointMake(0, 64);
        [webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
        [self.view addSubview:webView];
    }
    NSLog(@"b4 buttonLogic");
    [self buttonLogic];
    NSLog(@"after button logic");
}

-(void)recipientSignatureStartingPoint {
    [self showFraudWarningFor:SignatureIsFor_Recipient];
}

-(UIButton *)signatureButton {
    UIButton *signButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signButton.frame = [self signatureButtonFrame];
    [signButton setTitle:@"Add Signature" forState:UIControlStateNormal];
    [signButton setTitleColor:[UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0] forState:UIControlStateNormal];
    [signButton setTitleColor:[UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:0.5] forState:UIControlStateHighlighted];
    [signButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [signButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
    if (isIpad) {
        [signButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:40]];
    }
    if (_currentUser.userType == UserType_IsPCA) {
        [signButton addTarget:self action:@selector(addSignature:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        [signButton addTarget:self action:@selector(recipientSignatureStartingPoint) forControlEvents:UIControlEventTouchUpInside];
    }
    return signButton;
}

-(CGRect)signatureButtonFrame {
    NSLog(@"x\r\rpcaSignature.wasSigned:%@\rIsRecipient:%d\r\rx", currentSession.currentTimecard.pcaSignature.wasSigned, _currentUser.userType == UserType_IsRecipient);
//    if ([currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@NO] || (_currentUser.userType == UserType_IsRecipient && [currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@NO])) {
//        return CGRectMake(0, height - 75, width, 75);
//    }
    return CGRectMake(width * 0.5, height - 75, width * 0.5, 75);
}

-(void)buttonLogic {
    
    if (currentSession.currentTimecard.file.recipientSignature) {
        currentSession.currentTimecard.recipientSignature.wasSigned = @YES;
    } else {
        currentSession.currentTimecard.recipientSignature.wasSigned = @NO;
    }
    
    if (currentSession.currentTimecard.file.pcaSignature) {
        currentSession.currentTimecard.pcaSignature.wasSigned = @YES;
    } else {
        currentSession.currentTimecard.pcaSignature.wasSigned = @NO;
    }
//    if (currentSession.userType == UserType_IsPCA) {
    
    
    if (!sigButton) {
        sigButton = [self signatureButton];
        [self.view addSubview:sigButton];
    }
    
    sendPDFButton.frame = [self signatureButtonFrame];
    
    if (!sendPDFButton) {
        sendPDFButton = [UIButton buttonWithType:UIButtonTypeCustom];
        sendPDFButton.frame = [self signatureButtonFrame];
        
        [sendPDFButton setTitleColor:[UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0] forState:UIControlStateNormal];
        [sendPDFButton setTitleColor:[UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:0.5] forState:UIControlStateHighlighted];
        [sendPDFButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [sendPDFButton.titleLabel setFont:sigButton.titleLabel.font];
        
        [self.view addSubview:sendPDFButton];
        
        
        
        if ([currentSession.ratio.ratio isEqualToString:@"1:1"]) {
            [self formatButtonsToSendTimesheet];
        } else if ([currentSession.ratio.ratio isEqualToString:@"1:2"]) {
            if ([self.currentSession.currentTimesheetNumber isEqualToNumber:@1]) {
                [self formatButtonsForNextTimecard];
            } else if ([self.currentSession.currentTimesheetNumber isEqualToNumber:@2]) {
                [self formatButtonsToSendTimesheet];
            }
        } else if ([currentSession.ratio.ratio isEqualToString:@"1:3"]) {
            if ([self.currentSession.currentTimesheetNumber isEqualToNumber:@1] || [self.currentSession.currentTimesheetNumber isEqualToNumber:@2]) {
                [self formatButtonsForNextTimecard];
            } else {
                [self formatButtonsToSendTimesheet];
            }
        } else {
            [self formatButtonsToSendTimesheet];
        }

    }
    
    if (!saveAndExitButton) {
        saveAndExitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        saveAndExitButton.frame = CGRectMake(0, [self signatureButtonFrame].origin.y, width * 0.5, [self signatureButtonFrame].size.height);
        [saveAndExitButton addTarget:self action:@selector(saveAndExitPressed:) forControlEvents:UIControlEventTouchUpInside];
        [saveAndExitButton setTitle:@"Save & Exit" forState:UIControlStateNormal];
        [saveAndExitButton setTitleColor:[UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0] forState:UIControlStateNormal];
        saveAndExitButton.titleLabel.font = sigButton.titleLabel.font;
        [saveAndExitButton setTitleColor:[UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:0.5] forState:UIControlStateHighlighted];
        [self.view addSubview:saveAndExitButton];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(width * 0.5 - 0.5, saveAndExitButton.frame.origin.y, 1, saveAndExitButton.frame.size.height)];
        line.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        [self.view addSubview:line];
    }
        
    if (!self.navigationItem.rightBarButtonItem) {
        self.navigationItem.rightBarButtonItem = emailButton;
    }
    
//    back = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
//    self.navigationItem.leftBarButtonItem = back;
    
    
   
    if ([currentSession.currentTimecard.recipientSignature.wasSigned isEqualToNumber:@YES] && [currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES]) {
        emailButton.enabled = YES;
        sendPDFButton.alpha = 1.0;
        sigButton.alpha = 0.0;
        saveAndExitButton.alpha = 1.0;
    } else {
        sendPDFButton.alpha = 0.0;
        sigButton.alpha = 1.0;
        saveAndExitButton.alpha = 1.0;
        emailButton.enabled = NO;
    }
    
    if ([currentSession.currentTimecard.recipientSignature.wasSigned isEqualToNumber:@YES] || [currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES]) {
        back.enabled = NO;
    } else {
        back.enabled = YES;
    }
    CALayer *horizLine = [CALayer layer];
    horizLine.frame = CGRectMake(0, sigButton.frame.origin.y - 0.5, width, 1);
    horizLine.backgroundColor = [UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0].CGColor;
    [self.view.layer addSublayer:horizLine];

//    }//where if user is pca statement ends
//    else{
//        [sigButton addTarget:self action:@selector(userIsRecipient) forControlEvents:UIControlEventTouchUpInside];
//        
//    }
    
    
}

-(void)saveAndExitPressed:(id)sender {
    [self finishSessionAndShouldRemoveSession:NO];
}

-(void)formatButtonsToSendTimesheet {
    [sendPDFButton setTitle:@"Send Time Entry" forState:UIControlStateNormal];
    [sendPDFButton addTarget:self action:@selector(sendTimesheetAndFinish) forControlEvents:UIControlEventTouchUpInside];
    emailButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(sendTimesheetAndFinish)];
}

-(void)formatButtonsForNextTimecard {
    [sendPDFButton setTitle:@"Next Time Entry" forState:UIControlStateNormal];
    [sendPDFButton addTarget:self action:@selector(nextTimecard) forControlEvents:UIControlEventTouchUpInside];
    emailButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextTimecard)];
}

-(void)userIsRecipient {
 
    [self showFraudWarningFor:SignatureIsFor_Recipient];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    currentSession.continueTimesheetNumber = [NSNumber numberWithLong:6];
    NSLog(@"continueTimesheetNumber:%@", currentSession.continueTimesheetNumber);
    [NetworkingHelper saveSessionDataWithSessionNumber:currentSession.idNumber andParams:@{@"continueTimesheetNumber":currentSession.continueTimesheetNumber} andsuccess:^(id responseObject) {
//        NSLog(@"responseObject:%@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"errorSavingSessionData:%@", error);
    }]; 
    if ([_currentUser.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
        AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
        locManager = del.appLocationManager;
        locManager.delegate = self;
        locManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locManager startUpdatingLocation];
    }
}

-(void)updateCurrentSessionWithParams:(NSDictionary *)params {
    [NetworkingHelper saveSessionDataWithSessionNumber:currentSession.idNumber andParams:params andsuccess:^(id responseObject) {
        NSLog(@"updateSessionSuccess:%@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"update session fail:%@", error);
    }];
}

#pragma mark - Location Delegate

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == 3 || status == 4 || status == 5) {
        if ([_currentUser.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
            [manager startUpdatingLocation];
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"locationRequest"];
    [locManager stopUpdatingLocation];
}

-(void)back {
    [locManager stopUpdatingLocation];
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)figureOutDayOfWeekAndSetArray {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateStyle:NSDateFormatterShortStyle];
    
    dateEnding = [format dateFromString:currentSession.dateEnding];
    [format setDateFormat:@"EEEE"];
    weekDay = [format stringFromDate:dateEnding];
}

#pragma mark - Save Timecard and Navigation

-(void)nextTimecard {
    [NetworkingHelper saveFileWithFileIDNumber:currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetCsvFile", @"fileName":@"timesheetCsvFile.csv", @"fileData":csvDataString, @"mimeType":@"text/csv", @"timesheet":currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
        [NetworkingHelper saveFileWithFileIDNumber:currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetPdfFile", @"fileName":@"timesheetPdfFile.pdf", @"fileData":[self generatePDF], @"mimeType":@"application/pdf", @"timesheet":currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
            [self backToRecipientScreen];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
            NSLog(@"failed To Save PDF to server:%@", error);
        }];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"Error Saving csv to server:%@", error.localizedDescription);
    }];
    
}

-(void)backToRecipientScreen {

    [locManager stopUpdatingLocation];
    if (_currentUser.userType == UserType_IsRecipient) {
        currentSession.currentTimecard = currentSession.recipientTimesheetsArray[[currentSession.currentTimesheetNumber longValue]];
        if ([currentSession.currentTimesheetNumber isEqualToNumber:@1]) {
            currentSession.currentTimesheetNumber = @2;
        } else if ([currentSession.currentTimesheetNumber isEqualToNumber:@2]) {
            currentSession.currentTimesheetNumber = @3;
        }
        [self generatePDF];
        [self buttonLogic];
    } else {
        // If user is a worker do this
        for (UIViewController *controllers in self.navigationController.viewControllers) {
            if ([controllers isKindOfClass:[ChooseUserViewController class]]) {
                ChooseUserViewController *vc = (ChooseUserViewController *)controllers;
                currentSession.continueTimesheetNumber = @6;
                
                if ([currentSession.currentTimesheetNumber isEqualToNumber:@1]) {
                    currentSession.currentTimesheetNumber = @2;
                } else if ([currentSession.currentTimesheetNumber isEqualToNumber:@2]) {
                    currentSession.currentTimesheetNumber = @3;
                }
                
                _currentUser.currentSession = currentSession;
                [_currentUser save];
                vc.shouldStartNewTimesheet = YES;
                [self.navigationController popToViewController:vc animated:YES];
                break;
            }
        }
    }
    
}

-(void)showSuccessAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Time entry was sent successfully" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self finishSessionAndShouldRemoveSession:YES];
    }]];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self presentViewController:alert animated:YES completion:NULL];
}

-(void)sendTimesheetAndFinish {
    LoadingView *lv = [[LoadingView alloc] initWithFrame:CGRectMake(0, 0, 150, 100) andText:@"Saving"];
    lv.center = self.view.center;
    [self.view addSubview:lv];
    NSLog(@"currentSession b4 sennding:%@", currentSession.currentTimecard);
    
        [NetworkingHelper saveFileWithFileIDNumber:currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetPdfFile", @"fileName":@"timesheetPdfFile.pdf", @"fileData":[self generatePDF], @"mimeType":@"application/pdf", @"timesheet":currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
            [NetworkingHelper saveFileWithFileIDNumber:currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetCsvFile", @"fileName":@"timesheetCsvFile.csv", @"fileData":csvDataString, @"mimeType":@"text/csv", @"timesheet":currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
            [NetworkingHelper finishSessionWithSessionID:currentSession.idNumber andSuccess:^(id responseObject) {
                
                [lv hideActivityIndicator];
                if (_currentUser.userType == UserType_IsPCA) {
                    [self showSuccessAlert];
                }
                else {
                    [self goToNextRecipientTimesheet];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
                [lv hideActivityIndicator];
                [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Time entry was not successfully saved. Check your network connection and try again."] animated:YES completion:NULL];
            }];
        
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Time entry was not successfully saved. Check your network connection and try again."] animated:YES completion:NULL];
            [lv hideActivityIndicator];
        }];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        [lv hideActivityIndicator];
        [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Time entry was not successfully saved. Check your network connection and try again."] animated:YES completion:NULL];
    }];
}

-(void)goToNextRecipientTimesheet {
    [NetworkingHelper getSessionDataWithSuccess:^(id responseObject) {
        NSLog(@"responseObject:%@", responseObject);
        // This tells if we have another timecard to sign off on or not.
        _currentUser = [[User alloc] initWithDictionary:responseObject];
        [_currentUser save];
        if (_currentUser.sessions.count > 0) {
            _currentUser.currentSession = _currentUser.sessions[0];
        }
        currentSession = _currentUser.currentSession;
        if (currentSession.currentTimecard.file.recipientSignature) {
            currentSession.currentTimecard.recipientSignature.wasSigned = @YES;
        }
        if (currentSession.currentTimecard.file.pcaSignature) {
            currentSession.currentTimecard.pcaSignature.wasSigned = @YES;
        }
//        NSLog(@"currentUser:%@", _currentUser);
        NSLog(@"self.currentUser.currentSession.currentTimecard:%@", self.currentUser.currentSession.currentTimecard);
        if ([currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES] && shouldAutoSignRecip == YES) {
            shouldAutoSignRecip = YES;
            [webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
            [self sendTimesheetAndFinish];
        } else if ([currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES]) {
            UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:@"New Time Entry" message:@"You have a new time entry to sign. Apply this signature to all outstanding time entries?" preferredStyle:UIAlertControllerStyleActionSheet];
            alert2.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            alert2.popoverPresentationController.sourceView = self.view;
            CGRect rect = CGRectMake(webView.center.x - 400, webView.center.y - 250, 200, 500);
            alert2.popoverPresentationController.sourceRect = rect;
            [alert2 addAction:[UIAlertAction actionWithTitle:@"Apply to All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                shouldAutoSignRecip = YES;
                [webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
                [self sendTimesheetAndFinish];
            }]];
            [alert2 addAction:[UIAlertAction actionWithTitle:@"View Next Time Entry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
                [self buttonLogic];
                NSLog(@"next time entry button hit");
            }]];
            
            [alert2 addAction:[UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                [self finishSessionAndShouldRemoveSession:NO];
            }]];
            
            alert2.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            
            [self presentViewController:alert2 animated:YES completion:NULL];
        } else {
//            [self showSuccessAlert];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Outstanding Time Entries" message:@"Time entry sent successfully. There are no more time entry that require a signature." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self finishSessionAndShouldRemoveSession:YES];
            }]];
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self presentViewController:alert animated:YES completion:NULL];

        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        
    }];
}

-(void)goToHomeController {
    
}

-(void)finishSessionAndShouldRemoveSession:(BOOL)removeSession {

    [locManager stopUpdatingLocation];
    if  (_currentUser.userType == UserType_IsPCA) {
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[HomeViewController class]]) {
                if (removeSession == YES) {
                    [_currentUser removeSessionWithID:currentSession.idNumber];
                    
                } else {
                    NSLog(@"_currentUser.sessions:%@", _currentUser.sessions);
                    [_currentUser save];
                }
                [self.navigationController popToViewController:vc animated:YES];
                break;
            }
        }
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
//        NSLog(@"return NO");
        return NO;
    }
    return YES;
}

#pragma mark - PDF

-(NSData *)generatePDF {
    
//    NSLog(@"currentSession:%@", currentSession);
    NSLog(@"pdf has been generated from next timehseet button");
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filename = [NSString stringWithFormat:@"Time entry for %@ %@.pdf", _currentUser.pca.firstName, _currentUser.pca.lastName];
    
    filename = [self _sanitizeFileNameString:filename];
    
    NSString *newDocumentPath = [documentsDirectory stringByAppendingPathComponent:filename];
    NSString *templatePath = [[NSBundle mainBundle] pathForResource:[self pdfNameForService:_currentUser.currentSession.service.serviceName] ofType:@"pdf"];
    
    
    if ([fileManager fileExistsAtPath:newDocumentPath]) {
        [[NSFileManager defaultManager] removeItemAtPath: newDocumentPath error: &error];
    }
    
    [fileManager copyItemAtPath:templatePath toPath:newDocumentPath error:&error];
    
    NSURL *documentURL = [NSURL fileURLWithPath:newDocumentPath];
    
    CGPDFDocumentRef ourPDF = CGPDFDocumentCreateWithURL((CFURLRef)documentURL);
    
    
    // const size_t numberOfPages = CGPDFDocumentGetNumberOfPages(ourPDF);
    NSMutableData* data = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(data, CGRectZero, nil);
    
    
    CGPDFPageRef pdfPage = CGPDFDocumentGetPage(ourPDF, 1);
    const CGRect pageFrame = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
    
    UIGraphicsBeginPDFPageWithInfo(pageFrame, nil);
    
    // Draw the page (flipped)
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -pageFrame.size.height);
    CGContextDrawPDFPage(ctx, pdfPage);
    CGContextRestoreGState(ctx);
    
    //Top of the PDF ================================================================================
    
    
    
    NSString *checkMarkString = @"\u2713";
//    [[UIColor blackColor] set];
//    if ([self string:currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
//        [checkMarkString drawAtPoint:CGPointMake(182, 92) withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18]}];
//    } else if ([self string:currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
//        [checkMarkString drawAtPoint:CGPointMake(328, 92) withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18]}];
//    }
    
    
    [_currentUser.agency.name drawAtPoint:CGPointMake(120, 122) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    [_currentUser.agency.phone drawAtPoint:CGPointMake(442, 122) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    
    //Change back to above if we make this more widely available.
    
    if (currentSession.dateEnding) {
//        NSLog(@"pdf dateEnding:%@", currentSession.dateEnding);
        [[self dateEndingString] drawAtPoint:CGPointMake(208, 160) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:16]}];
    }
    
    
    // Activities Part of the PDF
    
    
    long activitiesCount = _currentUser.possibleCareOptions.count;
    NSMutableArray *dayActivitiesArray = [NSMutableArray arrayWithCapacity:activitiesCount];
    for (int i = 0; i < activitiesCount; i ++) {
        
        CareOption *possibleOption = (CareOption *)_currentUser.possibleCareOptions[i];
        if (currentSession.currentTimecard.careOptionsArray.count > 0) {
            long lastIndex = currentSession.currentTimecard.careOptionsArray.count - 1;
            for (int j = 0; j < currentSession.currentTimecard.careOptionsArray.count; j++) {
                NSNumber *optionID = currentSession.currentTimecard.careOptionsArray[j];
                if ([possibleOption.idNumber isEqualToNumber:optionID]) {
                    [dayActivitiesArray insertObject:@"1" atIndex:i];
                    if (i < 10) {
                        long x = 264;
                        if ([self string:currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
                            x = 498;
                        }
                        int y = 206 + (28.1 * i);
                        [checkMarkString drawAtPoint:CGPointMake(x, y) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:32]}];
                    } else {
                        int y = 206 + (28.1 * (i - 10));
                        [checkMarkString drawAtPoint:CGPointMake(498, y) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:32]}];
                    }
                    break;
                } else {
                    if (j == lastIndex) {
                        [dayActivitiesArray insertObject:@"0" atIndex:i];
                    }
                }
            }
        } else {
            [dayActivitiesArray insertObject:@"0" atIndex:i];
        }
    }
    
    // Total Hours
    NSDateFormatter *shortFormatter = [[NSDateFormatter alloc] init];
    [shortFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc] init];
    [shortDateFormatter setDateStyle:NSDateFormatterShortStyle];
    [shortDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSDateFormatter *medFormatter = [[NSDateFormatter alloc] init];
    [medFormatter setTimeStyle:NSDateFormatterMediumStyle];
    NSString *timeInString;
    if (currentSession.timeIn.timeIn) {
        timeInString = [shortFormatter stringFromDate:currentSession.timeIn.timeIn];
//        NSLog(@"timeInString:%@", timeInString);
        bool isAm;
        NSArray *stringAr = [timeInString componentsSeparatedByString:@" "];
        NSString *one = [stringAr objectAtIndex:0];
        [one drawAtPoint:CGPointMake(454, 404.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:20]}];
        if (stringAr.count > 1) {
            NSString *two = [stringAr objectAtIndex:1];
            if ([two isEqualToString:@"AM"]) {
                isAm = YES;
            } else {
                isAm = NO;
            }
            
            CGRect circleRect;
            if (isAm) {
                circleRect = CGRectMake(511.2, 403, 22, 10);
            } else {
                circleRect = CGRectMake(512, 414.4, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        } else {
            NSArray *timeStringArray = [one componentsSeparatedByString:@":"];
//            NSLog(@"timeStringArray[0]:%@", timeStringArray[0]);
            if ([timeStringArray[0] longLongValue] < 12) {
                isAm = YES;
            } else {
                isAm = NO;
            }
            CGRect circleRect;
            if (isAm) {
                circleRect = CGRectMake(511.2, 403, 22, 11);
            } else {
                circleRect = CGRectMake(512, 414.4, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        }
    } else {
        timeInString = @"na";
    }
    
    NSString *timeOutString;
    if (currentSession.timeOut.timeOut) {
//        NSDate *convertDate = [medFormatter dateFromString:currentSession.timeOut.timeOut];
        timeOutString = [shortFormatter stringFromDate:currentSession.timeOut.timeOut];
        bool isAM;
        NSArray *stringAr = [timeOutString componentsSeparatedByString:@" "];
        NSString *one = [stringAr objectAtIndex:0];
        [one drawAtPoint:CGPointMake(454, 432) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:20]}];
        if (stringAr.count > 1) {
            NSString *two = [stringAr objectAtIndex:1];
            if ([two isEqualToString:@"AM"]) {
                isAM = YES;
            } else {
                isAM = NO;
            }
            
            CGRect circleRect;
            if (isAM) {
                circleRect = CGRectMake(511.2, 430, 22, 11);
            } else {
                circleRect = CGRectMake(512, 441.5, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        } else {
            NSArray *timeStringArray = [one componentsSeparatedByString:@":"];
            if ([timeStringArray[0] longLongValue] < 12) {
                isAM = YES;
            } else {
                isAM = NO;
            }
            CGRect circleRect;
            if (isAM) {
                circleRect = CGRectMake(511.2, 430, 22, 11);
            } else {
                circleRect = CGRectMake(512, 441.5, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        }
    } else {
        timeOutString = @"na";
    }
    
    CGRect dayCircle;
    if ([currentSession.ratio.ratio isEqualToString:@"1:1"]) {
        dayCircle = CGRectMake(439.5, 336, 28, 26);
    } else if ([currentSession.ratio.ratio isEqualToString:@"1:2"]) {
        dayCircle = CGRectMake(476, 336, 28, 26);
    } else if ([currentSession.ratio.ratio isEqualToString:@"1:3"]) {
        dayCircle = CGRectMake(507, 336, 28, 26);
    } else {
        dayCircle = CGRectZero;
    }
    
    UIBezierPath *monPath = [UIBezierPath bezierPathWithOvalInRect:dayCircle];
    [[UIColor blackColor] setStroke];
    [monPath stroke];
    
    if (![timeInString isEqualToString:@"na"]) {
//        currentTimecard.sharedCareLocation = @"Community";
        if ([self string:currentSession.careLocation.sharedCareLocation containsCaseInsensitiveString:@"Home"]) {
            [currentSession.careLocation.sharedCareLocation drawAtPoint:CGPointMake(454, 370) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:24]}];
        } else if ([self string:currentSession.careLocation.sharedCareLocation containsCaseInsensitiveString:@"Community"]) {
            [currentSession.careLocation.sharedCareLocation drawAtPoint:CGPointMake(442.25, 376) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:15]}];
        } else {
            [currentSession.careLocation.sharedCareLocation drawAtPoint:CGPointMake(467, 370) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:24]}];
        }
        
    }
    
    NSString *totalHoursString = [self convertSecondsToString:currentSession.timeOut.billableHours];
    [totalHoursString drawAtPoint:CGPointMake(450, 465.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:20]}];
    
    //GPS Locations
    
    if (currentSession.timeIn.address) {
        [currentSession.timeIn.address drawAtPoint:CGPointMake(135, 554.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    }
    
    if (currentSession.timeOut.address) {
        [currentSession.timeOut.address drawAtPoint:CGPointMake(149, 588.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    }
    
    //Bottom of the PDF
    
    if (currentSession.currentTimecard.recipient.firstName || currentSession.currentTimecard.recipient.lastName) {
        NSString *fullRecName = [NSString stringWithFormat:@"%@ %@",currentSession.currentTimecard.recipient.firstName, currentSession.currentTimecard.recipient.lastName];
        [fullRecName drawAtPoint:CGPointMake(70, 680) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:15]}];
    }
    if (currentSession.currentTimecard.recipient.maNumber) {
        [currentSession.currentTimecard.recipient.maNumber drawAtPoint:CGPointMake(238, 680) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    }
    if (_currentUser.pca.firstName || _currentUser.pca.lastName) {
        NSString *fullPCAName = [NSString stringWithFormat:@"%@ %@", _currentUser.pca.firstName, _currentUser.pca.lastName];
        [fullPCAName drawAtPoint:CGPointMake(70, 708) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:15]}];
    }
    if (_currentUser.pca.umpi) {
        [_currentUser.pca.umpi drawAtPoint:CGPointMake(233, 710) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:12]}];
    }
    if (currentSession.currentTimecard.file.recipientSignature && !currentSession.currentTimecard.recipientSignature.signatureImageData) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", currentSession.fileURL, currentSession.currentTimecard.file.recipientSignature]];
        NSData *imgDat = [NSData dataWithContentsOfURL:url];
        currentSession.currentTimecard.recipientSignature.signatureImageData = imgDat;
    } else if (!currentSession.currentTimecard.recipientSignature.signatureImageData && recipientSignature && shouldAutoSignRecip) {
        currentSession.currentTimecard.recipientSignature.signatureImageData = recipientSignature;
        currentSession.currentTimecard.recipientSignature.timeStamp = [NSDate date];
    }
    
    if (currentSession.currentTimecard.recipientSignature.signatureImageData) {
        UIImage *image = [UIImage imageWithData:currentSession.currentTimecard.recipientSignature.signatureImageData];
        if (!isIpad) {
            image = [self rotateImage:image];
            [image drawInRect:CGRectMake(339, 672, 131, 26.5)];
        } else {
            [image drawInRect:CGRectMake(339, 674, 131, 26.5)];
        }
        
        if (currentSession.currentTimecard.recipientSignature.timeStamp) {
            NSString *timeStampString = [shortDateFormatter stringFromDate:currentSession.currentTimecard.recipientSignature.timeStamp];
            [timeStampString drawAtPoint:CGPointMake(487, 680) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:13]}];
        }
    }
    
//    NSLog(@"currentSession.currentTimecard.file.pcaSignature:%@", currentSession.currentTimecard.file.pcaSignature);
    if (currentSession.currentTimecard.file.pcaSignature && !currentSession.currentTimecard.pcaSignature.signatureImageData) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", currentSession.fileURL, currentSession.currentTimecard.file.pcaSignature]];
        NSData *imgDat = [NSData dataWithContentsOfURL:url];
        currentSession.currentTimecard.pcaSignature.signatureImageData = imgDat;
    }
        if (currentSession.currentTimecard.pcaSignature.signatureImageData) {
            UIImage *image = [UIImage imageWithData:currentSession.currentTimecard.pcaSignature.signatureImageData];
            if (!isIpad) {
                image = [self rotateImage:image];
                [image drawInRect:CGRectMake(339, 705, 131, 26.5)];
            } else {
                [image drawInRect:CGRectMake(339, 702.5, 131, 26.5)];
            }
            if (currentSession.currentTimecard.pcaSignature.timeStamp) {
                NSString *timeStampString = [shortDateFormatter stringFromDate:currentSession.currentTimecard.pcaSignature.timeStamp];
                [timeStampString drawAtPoint:CGPointMake(487, 710) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:13]}];
            }
        }
    
    
    
    
    //Create CSV File from currentTimecard Vars - WeekendingDate, dates, activities, total time on date, total time week, names, dob, date signed.
    
    
    // @"inpatient_incarcerated", @"start_date_inpatient_incarcerated", @"end_date_inpatient_incarcerated",
    NSArray *fieldsArray = [NSArray arrayWithObjects:@"state", @"service", @"agency_name", @"phone_number", @"day_of_service", @"date_of_service", @"recipient_first_name", @"recipient_last_name", @"recipient_ma_number", @"recipient_company_assigned_id", @"recipient_date_signed", @"pca_first_name", @"pca_last_name", @"pca_npi_umpi", @"pca_company_assigned_id", @"pca_date_signed", @"dressing", @"grooming", @"bathing", @"eating", @"transfers", @"mobility", @"positioning", @"toileting", @"health_related", @"behavior", @"light_housekeeping", @"laundry", @"other", @"visit_one_ratio", @"visit_one_location", @"visit_one_time_in", @"visit_one_time_in_address", @"visit_one_time_out", @"visit_one_time_out_address", @"recipient_signature_image_timestamp", @"pca_signature_image_timestamp", @"daily_total", nil];
    
    NSMutableString *csvString = [[NSMutableString alloc] init];
    [csvString appendString:[fieldsArray componentsJoinedByString:@","]];
    [csvString appendString:@"\n"];
    
    _currentUser.agency.state = @"Minnesota";
    NSString *pcaDateString;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    if (currentSession.currentTimecard.pcaSignature.timeStamp) {
        pcaDateString = [dateFormat stringFromDate:currentSession.currentTimecard.pcaSignature.timeStamp];
    } else {
        pcaDateString = @"na";
    }
    NSString *recipDateString;
    if (currentSession.currentTimecard.recipientSignature.timeStamp) {
        recipDateString = [dateFormat stringFromDate:currentSession.currentTimecard.recipientSignature.timeStamp];
    } else {
        recipDateString = @"na";
    }
    
    if (!currentSession.currentTimecard.recipientSignature.address) {
        currentSession.currentTimecard.recipientSignature.address = @"n/a";
    }
    if (!currentSession.currentTimecard.pcaSignature.address) {
        currentSession.currentTimecard.pcaSignature.address = @"n/a";
    }
    
    if (!currentSession.timeIn.address) {
        currentSession.timeIn.address = @"na";
        
    }
        
    if (!currentSession.timeOut.address) {
        currentSession.timeOut.address = @"na";
    }
    if (dayActivitiesArray.count != 13) {
        dayActivitiesArray = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", nil];
    }
    [csvString appendString:[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@\n", _currentUser.agency.state, currentSession.service.serviceName, _currentUser.agency.name, _currentUser.agency.phone, weekDay, currentSession.dateEnding, currentSession.currentTimecard.recipient.firstName, currentSession.currentTimecard.recipient.lastName, currentSession.currentTimecard.recipient.maNumber, currentSession.currentTimecard.recipient.companyAssignedID, recipDateString, _currentUser.pca.firstName, _currentUser.pca.lastName, _currentUser.pca.umpi, _currentUser.pca.companyAssignedID, pcaDateString, dayActivitiesArray[0], dayActivitiesArray[1], dayActivitiesArray[2], dayActivitiesArray[3], dayActivitiesArray[4], dayActivitiesArray[5], dayActivitiesArray[6], dayActivitiesArray[7], dayActivitiesArray[8], dayActivitiesArray[9], dayActivitiesArray[10], dayActivitiesArray[11], dayActivitiesArray[12], currentSession.ratio.ratio, currentSession.careLocation.sharedCareLocation, timeInString, currentSession.timeIn.address, timeOutString, currentSession.timeOut.address, currentSession.currentTimecard.recipientSignature.timeStamp, currentSession.currentTimecard.pcaSignature.timeStamp, [NSString stringWithFormat:@"%@", totalHoursString]]];
    csvDataString = [csvString dataUsingEncoding:NSUTF8StringEncoding];
     
    
    UIGraphicsEndPDFContext();
    
    CGPDFDocumentRelease(ourPDF);
    ourPDF = nil;
    [data writeToURL:documentURL atomically:YES];
    return data;
}


- (NSString *)_sanitizeFileNameString:(NSString *)fileName {
    // Mad props to http://stackoverflow.com/questions/1281576/how-to-make-an-nsstring-path-file-name-safe
    
    NSCharacterSet* illegalFileNameCharacters = [NSCharacterSet characterSetWithCharactersInString:@"/\\?%*|\"<>"];
    return [[fileName componentsSeparatedByCharactersInSet:illegalFileNameCharacters] componentsJoinedByString:@""];
}

-(NSString *)pdfNameForService:(NSString *)service
{
    if ([self string:service containsCaseInsensitiveString:@"Personal Care Service"]) {
        return @"PCA";
    } else if ([self string:service containsCaseInsensitiveString:@"Homemaking"]) {
        return @"Homemaking";
    } else if ([self string:service containsCaseInsensitiveString:@"Respite"]) {
        return @"Respite";
    } else if ([self string:service containsCaseInsensitiveString:@"Personal Assistance"]) {
        return @"Personal Assistance";
    } else if ([self string:service containsCaseInsensitiveString:@"Personal Support"]) {
        return @"Personal Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Self-Direction Support"]) {
        return @"Self-Direction Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Treatment and Training"]) {
        return @"Treatment and Training";
    } else if ([self string:service containsCaseInsensitiveString:@"Caregiving Expense"]) {
        return @"Caregiving Expense";
    } else if ([self string:service containsCaseInsensitiveString:@"Consumer Support"]) {
        return @"Consumer Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Environmental Modifications"]) {
        return @"Environmental Modifications";
    }
    return @"Time and Activity 12.2.15";
}

-(NSString *)convertSecondsToString:(NSNumber *)kseconds {
    long hours, minutes, seconds;
    long accumulatedSeconds = [kseconds doubleValue];
    hours = accumulatedSeconds/3600;
    minutes = (accumulatedSeconds % 3600) / 60;
    seconds = (accumulatedSeconds % 3600) % 60;
    return [NSString stringWithFormat:@"%2ld:%02ld:%02ld", hours, minutes, seconds];
}

-(NSString *)dateEndingString {
    //        NSLog(@"pdf dateEnding:%@", currentSession.dateEnding);
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    NSDate *stringDate = [dateformat dateFromString:currentSession.dateEnding];
    //        NSLog(@"dateEnding:%@", currentSession.dateEnding);
    
    //        NSLog(@"stringDate:%@", stringDate);
    NSDateFormatter *shortFormat = [NSDateFormatter new];
    [shortFormat setDateStyle:NSDateFormatterShortStyle];
    [shortFormat setTimeStyle:NSDateFormatterNoStyle];
    NSString *dateString = [shortFormat stringFromDate:stringDate];
    //        NSLog(@"dateString:%@", dateString);
    if (!stringDate) {
        return currentSession.dateEnding;
    } else {
        return dateString;
    }
    
}

#pragma mark - Alert View

-(void)addSignature:(UIButton *)sender {
    UIAlertController *proceedAlert = [UIAlertController alertControllerWithTitle:@"Proceed" message:@"Be sure your timecard is completed and correct before proceeding. Once you sign, you will have to send the timecard." preferredStyle:UIAlertControllerStyleAlert];
    [proceedAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [proceedAlert addAction:[UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self chooseUserForSignature:sender];
    }]];
    [self presentViewController:proceedAlert animated:YES completion:NULL];
    
}

-(void)chooseUserForSignature:(UIButton *)sender {
    UIAlertController *sigAlertController = [UIAlertController alertControllerWithTitle:@"Add Signature" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    sigAlertController.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    sigAlertController.popoverPresentationController.sourceView = self.view;
    CGRect rect = CGRectMake(webView.center.x - 400, webView.center.y - 250, 200, 500);
    
    sigAlertController.popoverPresentationController.sourceRect = rect;
    if (![currentSession.currentTimecard.recipientSignature.wasSigned isEqualToNumber:@YES] && ![currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES] && _currentUser.userType == UserType_IsPCA) {
        
        [sigAlertController addAction:[UIAlertAction actionWithTitle:@"Recipient/Responsible Party Signature" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showFraudWarningFor:SignatureIsFor_Recipient];
        }]];
        
        [sigAlertController addAction:[UIAlertAction actionWithTitle:@"Worker Signature" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showFraudWarningFor:SignatureIsFor_PCA];
            
        }]];
        [sigAlertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:sigAlertController animated:YES completion:NULL];
    } else if ([currentSession.currentTimecard.recipientSignature.wasSigned isEqualToNumber:@YES] && ![currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES]) {
        
        [sigAlertController addAction:[UIAlertAction actionWithTitle:@"Worker Signature" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showFraudWarningFor:SignatureIsFor_PCA];
        }]];
        [sigAlertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:sigAlertController animated:YES completion:NULL];
        
    } else if ([currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES] && ![currentSession.currentTimecard.recipientSignature.wasSigned isEqualToNumber:@YES]) {
        
        [sigAlertController addAction:[UIAlertAction actionWithTitle:@"Recipient/Responsible Party Signature" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showFraudWarningFor:SignatureIsFor_Recipient];
        }]];
        [sigAlertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:sigAlertController animated:YES completion:NULL];
        
    } else {
        sigAlertController = [UIAlertController alertControllerWithTitle:@"Signatures Already Added!" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        sigAlertController.popoverPresentationController.sourceView = webView;
        sigAlertController.popoverPresentationController.sourceRect = webView.frame;
        [sigAlertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:sigAlertController animated:YES completion:NULL];
        
    }
}



-(void)showFraudWarningFor:(SignatureIsFor)signatureIsFor {
    sigButton.alpha = 0.0;
    saveAndExitButton.alpha = 0.0;
    long tag = 5;
    fraudView = [[UIView alloc] init];
    if (isIpad) {
        fraudView.frame = CGRectMake(width * 0.25, 100, width * 0.5, height - 200);
    } else {
        fraudView.frame = CGRectMake(0, 64, width, height - 75 - 64);
    }
    fraudView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:fraudView];
    
    UITextView *warningTV = [[UITextView alloc] init];
    if (signatureIsFor == SignatureIsFor_PCA) {
        tag = 6;
    } else {
        warningTV.contentOffset = CGPointMake(0, 8);
    }
    warningTV.frame = CGRectMake(10, 10, fraudView.frame.size.width - 20, fraudView.frame.size.height - 20);
    warningTV.layer.cornerRadius = 8;
    warningTV.layer.borderColor = [UIColor blackColor].CGColor;
    warningTV.layer.borderWidth = 1.5;
    warningTV.textAlignment = NSTextAlignmentCenter;
    warningTV.backgroundColor = [UIColor whiteColor];
    warningTV.textColor = [UIColor blackColor];
    int size = 0;
    if (isIpad) {
        size = 30;
    } else {
        if (four) {
            size = 17;
        } else {
            size = 20;
        }
    }
    warningTV.font = [UIFont systemFontOfSize:size];
    warningTV.editable = NO;
    [fraudView addSubview:warningTV];
    
    warningTV.text = [self stringForFraudWarningTextView:signatureIsFor];
    [warningTV sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        warningTV.center = CGPointMake(webView.center.x, webView.center.y - 64);
    } else {
        warningTV.center = CGPointMake(warningTV.center.x, self.view.center.y);
        fraudView.frame = CGRectMake(fraudView.frame.origin.x, warningTV.frame.origin.y - 10, fraudView.frame.size.width, warningTV.frame.size.height + 20);
        warningTV.frame = CGRectMake(10, 10, fraudView.frame.size.width - 20, fraudView.frame.size.height - 20);
    }
    [warningTV sizeToFit];
    if (!cancelFWButotn) {
    cancelFWButotn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelFWButotn.frame = CGRectMake(0, height - 75, width/2, 75);
    [cancelFWButotn addTarget:self action:@selector(cancelFW) forControlEvents:UIControlEventTouchUpInside];
    [cancelFWButotn setTitle:@"Decline" forState:UIControlStateNormal];
    [cancelFWButotn setTitleColor:[UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0] forState:UIControlStateNormal];
    cancelFWButotn.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    cancelFWButotn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view addSubview:cancelFWButotn];
    }];
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            cancelFWButotn.alpha = 1.0;
        }];
    }
    if (!acceptFWButton) {
        acceptFWButton = [UIButton buttonWithType:UIButtonTypeCustom];
        acceptFWButton.frame = CGRectMake(width/2, height - 75, width/2, 75);
        [acceptFWButton setTitle:@"I Accept" forState:UIControlStateNormal];
        [acceptFWButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:18]];
        [acceptFWButton setTitleColor:[UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0] forState:UIControlStateNormal];
        acceptFWButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [acceptFWButton addTarget:self action:@selector(acceptFWFor:) forControlEvents:UIControlEventTouchUpInside];
        
        acceptFWButton.alpha = 0.0;
        [self.view addSubview:acceptFWButton];
    }
    acceptFWButton.tag = tag;
    [UIView animateWithDuration:0.3 animations:^{
        acceptFWButton.alpha = 1.0;
    }];
    
    fraudLine = [CALayer layer];
    fraudLine.backgroundColor = [UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0].CGColor;
    fraudLine.frame = CGRectMake(width/2 - 0.5, acceptFWButton.frame.origin.y, 1, acceptFWButton.frame.size.height);
    [self.view.layer addSublayer:fraudLine];
}

-(NSString *)stringForFraudWarningTextView:(SignatureIsFor)signatureIsFor {
    NSString *warningTVText;
    if (signatureIsFor == SignatureIsFor_Recipient) {
        self.title = @"Recipient/Responsible Party Signature";
        if ([self string:currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
            warningTVText = @"It is a federal crime to provide false information on PCA billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that the services were performed as specified in the PCA care plan. The time and activity document created with this app is transmitted via email. By using this app my health care provider may assume that e-mail communications are acceptable to me and that I am aware of the risks of using unencrypted e-mail.";
        } else {
            warningTVText = @"It is a federal crime to provide false information on billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that the services were performed as specified. The time and activity document created with this app is transmitted via email. By using this app my health care provider may assume that e-mail communications are acceptable to me and that I am aware of the risks of using unencrypted e-mail.";
        }
    } else {
        //        NSLog(@"self.serviceName:%@", self.currentSession.service.serviceName);
        self.title = @"PCA Signature";
        warningTVText = @"It is a federal crime to provide false information on PCA billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that the services were performed as specified in the PCA care plan.";
        if (![self string:currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
            self.title = @"Worker Signature";
            warningTVText = @"It is a federal crime to provide false information on billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that the services were performed as specified.";
        }
    }
    return warningTVText;
}

-(void)cancelFW {
    [UIView animateWithDuration:0.3 animations:^{
        cancelFWButotn.alpha = 0;
        acceptFWButton.alpha = 0;
        self.title = @"Review Timecard";
        [fraudView removeFromSuperview];
        [fraudLine removeFromSuperlayer];
        sigButton.alpha = 1.0;
        saveAndExitButton.alpha = 1.0;
    }];
    
}

-(void)acceptFWFor:(id)sender {
    SignatureIsFor sigFor = SignatureIsFor_Recipient; UIButton *button = (UIButton *)sender;
//    NSLog(@"sender.tag:%ld", (long)button.tag);
    if (button.tag == 6) {
//        NSLog(@"button.tag==6");
        sigFor = SignatureIsFor_PCA;
    }
    [UIView animateWithDuration:0.3 animations:^{
        cancelFWButotn.alpha = 0;
        acceptFWButton.alpha = 0;
        [fraudView removeFromSuperview];
        [fraudLine removeFromSuperlayer];
        if (sigFor == SignatureIsFor_Recipient && UserType_IsRecipient) {
            
            if (_currentUser.userType == UserType_IsPCA ) {
                [self showRecipientPasswordView];
            } else {
                [self showSignatureView:SignatureIsFor_Recipient];
            }
        } else {
            UIButton *button = [UIButton new];
            button.tag = 6;
            [self showSignatureView:SignatureIsFor_PCA];
        }
    }];
}

-(void)showRecipientPasswordView {
    recipientPasswordView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width * 0.8, self.view.frame.size.width * 0.8)];
    if (isIpad) {
        recipientPasswordView.frame = CGRectMake(0, 64, self.view.frame.size.height * 0.6, self.view.frame.size.height * 0.6);
    }
    recipientPasswordView.center = CGPointMake(self.view.center.x, self.view.center.y - 40);
    recipientPasswordView.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    recipientPasswordView.layer.borderColor = [UIColor grayColor].CGColor;
    recipientPasswordView.layer.borderWidth = 1;
    recipientPasswordView.layer.cornerRadius = 8;
    recipientPasswordView.alpha = 0.0;
    [self.view addSubview:recipientPasswordView];
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:@"\u2715" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [cancelButton addTarget:self action:@selector(removeRecipientPasswordView) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.frame = CGRectMake(recipientPasswordView.frame.size.width - 40, 0, 40, 40);
    [recipientPasswordView addSubview:cancelButton];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, cancelButton.frame.size.height, recipientPasswordView.frame.size.width, 40)];
    label.text = @"Recipient/RP Password";
    label.font = [UIFont systemFontOfSize:24];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    [recipientPasswordView addSubview:label];
    passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(recipientPasswordView.frame.size.width * 0.1, label.frame.origin.y + label.frame.size.height, recipientPasswordView.frame.size.width * 0.8, 40)];
    passwordTextField.font = [UIFont systemFontOfSize:24];
    passwordTextField.delegate = self;
    passwordTextField.borderStyle = UITextBorderStyleBezel;
    passwordTextField.textAlignment = NSTextAlignmentCenter;
    passwordTextField.backgroundColor = [UIColor whiteColor];
    passwordTextField.secureTextEntry = YES;
    [recipientPasswordView addSubview:passwordTextField];
    UIButton *sendRequestButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sendRequestButton.frame = CGRectMake(recipientPasswordView.frame.size.width * 0.33, passwordTextField.frame.origin.y + passwordTextField.frame.size.height + 20, recipientPasswordView.frame.size.width * 0.33, 40);
    [sendRequestButton setTitle:@"Verify" forState:UIControlStateNormal];
    [sendRequestButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendRequestButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [sendRequestButton.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [sendRequestButton addTarget:self action:@selector(verifyRecipientPasswordWithServer) forControlEvents:UIControlEventTouchUpInside];
    [recipientPasswordView addSubview:sendRequestButton];
    [passwordTextField becomeFirstResponder];
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        recipientPasswordView.alpha = 1.0;
    } completion:^(BOOL finished) {
//        recipientPasswordView.alpha = 1.0;
    }];
}
     
-(void)removeRecipientPasswordView {
    [recipientPasswordView removeFromSuperview];
    sigButton.alpha = 1.0;
    saveAndExitButton.alpha = 1.0;
}

-(void)verifyRecipientPasswordWithServer {
    [self.view endEditing:YES];
    [NetworkingHelper verifyRecipientPassword:passwordTextField.text andPCAID:_currentUser.pca.idNumber andRecipientID:currentSession.currentTimecard.recipient.idNumber andSuccess:^(id responseObject) {
//        NSLog(@"verifyRecipientPasswordResponseObject:%@", responseObject);
        if (responseObject[@"error"]) {
            [self alertViewOrAlertControllerWithMessage:responseObject[@"errorMessage"]];
        } else {
            sigButton.alpha = 1.0;
            saveAndExitButton.alpha = 1.0;
            [self removeRecipientPasswordView];
            [self showSignatureView:SignatureIsFor_Recipient];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        [self alertViewOrAlertControllerWithMessage:error.localizedDescription];
    }];
}

-(void)alertViewOrAlertControllerWithMessage:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alert animated:YES completion:NULL];
}

#pragma mark - signature methods

-(void)createTimeCardFile {
    if (!currentSession.currentTimecard.file.idNumber) {
        [NetworkingHelper createFileAndSuccess:^(id responseObject) {
//            NSLog(@"responseObject:%@", responseObject);
            if (!currentSession.currentTimecard.file) {
                currentSession.currentTimecard.file = [File new];
            }
            currentSession.currentTimecard.file.idNumber = responseObject[@"createdFilesId"];
//            NSLog(@"currentSession.currentTimecard.file.idNumber:%@", currentSession.currentTimecard.file.idNumber);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
            NSLog(@"Error creating File:%@", error);
        }];
    }
}

-(void)showSignatureView:(SignatureIsFor)signatureIsFor {
    //x 98.5 y 20 4.925
    sigButton.alpha = 0.0;
    saveAndExitButton.alpha = 0.0;
    NSString *title = @"Worker Signature";
    if ([self.currentSession.service.serviceName isEqualToString:@"Personal Care Service"]) {
        title = @"PCA Signature";
    }
    self.title = title;
    long tag = 6;
    if (signatureIsFor == SignatureIsFor_Recipient) {
        self.title = @"Recipient/Responsible Party Signature";
        tag = 5;
    }
    
    blackView = [[UIView alloc] init];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.75;
    blackView.frame = CGRectMake(0, 0, width, height - 75);
    [self.view addSubview:blackView];
    sigView = [[SignatureView alloc] init];
    sigView.frame = CGRectMake(80, 64, 160, height - 75 - 64);
    if (isIpad) {
        //        sigView.frame = CGRectMake(297.5, 304, 429, 160);
        sigView.frame = CGRectMake(100, 150, width - 200, (((width - 200) * 26.5) / 131));
    }
    sigView.center = self.view.center;
    sigView.backgroundColor = [UIColor whiteColor];
    sigView.multipleTouchEnabled = NO;
    sigView.delegate = self;
    /*  sigView.layer.borderColor = [UIColor blackColor].CGColor;
     sigView.layer.borderWidth = 1;
     */
    [self.view addSubview:sigView];
    bottomLine = [CALayer layer];
    if (isIpad) {
        bottomLine.frame = CGRectMake(20, sigView.frame.size.height - 40, sigView.frame.size.width - 40, 1.5);
    } else {
        bottomLine.frame = CGRectMake(sigView.frame.size.width - 40, 20, 1.5, sigView.frame.size.height - 40);
    }
    bottomLine.backgroundColor = [UIColor blackColor].CGColor;
    [sigView.layer addSublayer:bottomLine];
    xLab = [[UILabel alloc] init];
    if (isIpad) {
        xLab.frame = CGRectMake(40, sigView.frame.size.height - 59, 20, 20);
    } else {
        xLab.frame = CGRectMake(sigView.frame.size.width - 60, sigView.frame.size.height - 20, 20, 20);
    }
    xLab.text = @"X";
    if (isIpad) {
        xLab.font = [UIFont systemFontOfSize:32];
    } else {
        xLab.font = [UIFont boldSystemFontOfSize:16];
    }
    if (!isIpad) {
        xLab.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    
    [sigView addSubview:xLab];
    
    
    acceptSignatureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    acceptSignatureButton.frame = CGRectMake(width * 0.66, height - 75, width * 0.33, 75);
    [acceptSignatureButton setTitle:@"Accept" forState:UIControlStateNormal];
    [acceptSignatureButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [acceptSignatureButton setTitleColor:[UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:0.5] forState:UIControlStateHighlighted];
    [acceptSignatureButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [acceptSignatureButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
    if (isIpad) {
        [acceptSignatureButton.titleLabel setFont:[UIFont systemFontOfSize:24]];
    }
    [acceptSignatureButton addTarget:self action:@selector(acceptSignature:) forControlEvents:UIControlEventTouchUpInside];
    acceptSignatureButton.tag = tag;
    [self.view addSubview:acceptSignatureButton];
    [acceptSignatureButton setEnabled:NO];
    
    clearSignature = [UIButton buttonWithType:UIButtonTypeCustom];
    clearSignature.frame = CGRectMake(width * 0.33, height - 75, width * 0.33, 75);
    [clearSignature setTitle:@"Clear" forState:UIControlStateNormal];
    [clearSignature setTitleColor:[UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0] forState:UIControlStateNormal];
    [clearSignature setTitleColor:[UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:0.5] forState:UIControlStateHighlighted];
    clearSignature.backgroundColor = [UIColor whiteColor];
    [clearSignature.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [clearSignature.titleLabel setFont:acceptSignatureButton.titleLabel.font];
    [clearSignature addTarget:self action:@selector(clearSignature:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:clearSignature];
    
    deleteSignature = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteSignature.frame = CGRectMake(0, height - 75, width * 0.33, 75);
    [deleteSignature setTitleColor:[UIColor  colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0] forState:UIControlStateNormal];
    [deleteSignature setTitleColor:[UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:0.5] forState:UIControlStateHighlighted];
    [deleteSignature setTitle:@"Cancel" forState:UIControlStateNormal];
    [deleteSignature.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [deleteSignature.titleLabel setFont:acceptSignatureButton.titleLabel.font];
    [deleteSignature addTarget:self action:@selector(deleteSignature) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:deleteSignature];
    line1 = [CALayer layer];
    line1.backgroundColor = [UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0].CGColor;
    line1.frame = CGRectMake(width * 0.33 - 0.5, clearSignature.frame.origin.y, 1, clearSignature.frame.size.height);
    [self.view.layer addSublayer:line1];
    line2 = [CALayer layer];
    line2.backgroundColor = [UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0].CGColor;
    line2.frame = CGRectMake(width * 0.66 - 0.5, clearSignature.frame.origin.y, 1, clearSignature.frame.size.height);
    [self.view.layer addSublayer:line2];
}

-(void)enableButton {
    [acceptSignatureButton setTitleColor:[UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0] forState:UIControlStateNormal];
    [acceptSignatureButton setEnabled:YES];
}

-(void)acceptSignature:(id)sender {
    self.navigationItem.leftBarButtonItem = nil;
    [xLab removeFromSuperview];
    [bottomLine removeFromSuperlayer];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDateFormat];
    UIImage *finalImage = [self getFinalImageFromSignatureView];
    UIButton *button = (UIButton*)sender;
    if (button.tag == 5) {
        if (!currentSession.currentTimecard.recipientSignature) {
            currentSession.currentTimecard.recipientSignature = [RecipientSignature new];
        }
        currentSession.currentTimecard.recipientSignature.signatureImageData = UIImagePNGRepresentation(finalImage);
        recipientSignature = UIImagePNGRepresentation(finalImage);
        [NetworkingHelper saveFileWithFileIDNumber:currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetRecSig", @"fileName":@"timesheetRecSig.png", @"fileData":currentSession.currentTimecard.recipientSignature.signatureImageData, @"mimeType":@"image/png", @"timesheet":currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
            NSLog(@"responseObject for saving recipient signature file:%@", responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
            NSLog(@"failed to save recipient signature image:%@", error);
        }];
        currentSession.currentTimecard.recipientSignature.timeStamp = [NSDate date];
        currentSession.currentTimecard.recipientSignature.wasSigned = @YES;
        
        if ([currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
            [geoCoder reverseGeocodeLocation:locManager.location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *placeMark = placemarks[0];
                currentSession.currentTimecard.recipientSignature.address = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", [placeMark subThoroughfare], [placeMark thoroughfare], [placeMark locality], [placeMark administrativeArea], [placeMark postalCode]];
                //            NSLog(@"params:%@", @{@"timesheet":currentSession.currentTimecard.idNumber, @"recipientSigTime":currentSession.currentTimecard.recipientSignature.timeStamp, @"recipientSigAddress": currentSession.currentTimecard.recipientSignature.address, @"latitude":[NSNumber numberWithDouble:locManager.location.coordinate.latitude], @"longitude":[NSNumber numberWithDouble:locManager.location.coordinate.longitude], @"recipientPhotoTime":[NSDate date]});
                [self createRecipientSignatureTable];
            }];
        } else {
            currentSession.currentTimecard.recipientSignature.address = @"";
            currentSession.currentTimecard.recipientSignature.lat = @0.0;
            currentSession.currentTimecard.recipientSignature.lng = @0.0;
            [self createRecipientSignatureTable];
        }
    } else {
        if (!currentSession.currentTimecard.pcaSignature) {
            currentSession.currentTimecard.pcaSignature = [PCASignature new];
        }
        currentSession.currentTimecard.pcaSignature.signatureImageData = UIImagePNGRepresentation(finalImage);
        [NetworkingHelper saveFileWithFileIDNumber:currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetPcaSig", @"fileName":@"timesheetPcaSig.png", @"fileData":currentSession.currentTimecard.pcaSignature.signatureImageData, @"mimeType":@"image/png", @"timesheet":currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
            NSLog(@"saveFilePCASig responseObject:%@", responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
            NSLog(@"failed to save pca signature image:%@", error);
        }];
        currentSession.currentTimecard.pcaSignature.timeStamp = [NSDate date];
        currentSession.currentTimecard.pcaSignature.wasSigned = @YES;
        if ([currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
            [geoCoder reverseGeocodeLocation:locManager.location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *placeMark = placemarks[0];
                currentSession.currentTimecard.pcaSignature.address = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", [placeMark subThoroughfare], [placeMark thoroughfare], [placeMark locality], [placeMark administrativeArea], [placeMark postalCode]];
                //   {"homecare_homecarebundle_recipientsignature":{"timesheet":"1741","recipientSigTime":"2015-03-03 05:22:28","recipientSigAddress":"1752 sequoia lane New Richmond WI 54017","latitude":"-20.82939392","longitude":"60.82939562","recipientPhotoTime":"2015-03-03 10:20:33"}
                [self createPCASignatureTable];
            }];
        } else {
            currentSession.currentTimecard.pcaSignature.address = @"";
            currentSession.currentTimecard.pcaSignature.lat = @0.0;
            currentSession.currentTimecard.pcaSignature.lng = @0.0;
            [self createPCASignatureTable];
        }
    }
    [webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
    [self buttonLogic];
    self.title = @"Review Time Entry";
    [self clearViews];
    
}

-(void)createRecipientSignatureTable {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDateFormat];
    NSString *rPhotoTime = [format stringFromDate:[NSDate date]];
    [NetworkingHelper createRecipientSignatureTableWithParams:@{@"timesheet":currentSession.currentTimecard.idNumber, @"recipientSigTime":rPhotoTime, @"recipientSigAddress": currentSession.currentTimecard.recipientSignature.address, @"latitude":[NSNumber numberWithDouble:locManager.location.coordinate.latitude], @"longitude":[NSNumber numberWithDouble:locManager.location.coordinate.longitude], @"recipientPhotoTime":rPhotoTime} andSuccess:^(id responseObject) {
        // NSLog(@"success createing recipient signature:%@", responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"failed to create recipient signature:%@", error);
    }];
}

-(void)createPCASignatureTable {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDateFormat];
    NSString *pDateString = [format stringFromDate:[NSDate date]];
    [NetworkingHelper createPCASignatureTableWithParams:@{@"timesheet":currentSession.currentTimecard.idNumber, @"pcaSigTime":pDateString, @"pcaSigAddress": currentSession.currentTimecard.pcaSignature.address, @"latitude":[NSNumber numberWithDouble:locManager.location.coordinate.latitude], @"longitude":[NSNumber numberWithDouble:locManager.location.coordinate.longitude], @"pcaPhotoTime":pDateString} andSuccess:^(id responseObject) {
        //                NSLog(@"responseObject:%@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"failedToCreatepcaSignature Table:%@", error);
    }];
}

-(void)clearSignature:(UIButton *)button {
    [self clearViews];
    [self showSignatureView:button.tag == 6 ? SignatureIsFor_PCA : SignatureIsFor_Recipient];
}

-(void)deleteSignature {
    sigButton.alpha = 1.0;
    saveAndExitButton.alpha = 1.0;
    self.title = @"Review Time Entry";
    [webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
    [self clearViews];
}

-(void)clearViews {
    [sigView removeFromSuperview];
    [acceptSignatureButton removeFromSuperview];
    [clearSignature removeFromSuperview];
    [deleteSignature removeFromSuperview];
    [line1 removeFromSuperlayer];
    [line2 removeFromSuperlayer];
    [blackView removeFromSuperview];
    NSLog(@"clearViewsCalled");
}

#pragma mark - UIImage Processing (For Signature Screenshot)

-(UIImage *)getFinalImageFromSignatureView {
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    [sigView.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGRect rect = sigView.frame;
    rect.origin.x = rect.origin.x + 1;
    rect.origin.y = rect.origin.y + 6;
    rect.size.height = rect.size.height - 12;
    rect.size.width = rect.size.width - 2;
    UIImage *secondImage = [self processImage:image];
    return [self cropImage:secondImage rect:rect];
}

-(UIImage *)cropImage:(UIImage *)image rect:(CGRect)cropRect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return img;
}

-(UIImage *)rotateImage:(UIImage *)image {
    return [[UIImage alloc] initWithCGImage:image.CGImage scale:1.0 orientation:UIImageOrientationRight];
}

- (UIImage*) processImage :(UIImage*) inputImage
{
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, 1.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRef rawImageRef = viewImage.CGImage;
    const CGFloat colorMasking[6] = {255.0f, 255.0f, 255.0f, 255.0f, 255.0f, 255.0f};
    UIGraphicsBeginImageContext(viewImage.size);
    CGImageRef maskedImageRef = CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, viewImage.size.height);
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, viewImage.size.width, viewImage.size.height), maskedImageRef);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end


