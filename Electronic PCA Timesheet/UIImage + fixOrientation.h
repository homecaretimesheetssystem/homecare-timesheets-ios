//
//  UIImage + fixOrientation.h
//  FTP Picture Uploader
//
//  Created by Scott Mahonis on 6/17/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end