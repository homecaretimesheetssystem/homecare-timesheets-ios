//
//  RecipientSignature.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/14/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "RecipientSignature.h"

@implementation RecipientSignature

-(id)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        NSDateFormatter *dateFormatter = [RecipientSignature dateWriter];
        dateFormatter.dateFormat = kDateFormat;
        
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            self.idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }

        
        if ([dictionary[@"recipientPhotoTime"] isKindOfClass:[NSDate class]]) {
            self.userImageTimestamp = dictionary[@"recipientPhotoTime"];
        } else {
            self.userImageTimestamp = [dateFormatter dateFromString:dictionary[@"recipientPhotoTime"]];
        }
        
        if ([dictionary objectForKey:@"recipientSigAddress"] != [NSNull null]) {
            self.address = [dictionary[@"recipientSigAddress"] uppercaseString];
        }else{
            self.address = @"";
        }
        
        if ([dictionary[@"recipientSigTime"] isKindOfClass:[NSDate class]]) {
            self.timeStamp = dictionary[@"recipientSigTime"];
        } else {
            self.timeStamp = [dateFormatter dateFromString:dictionary[@"recipientSigTime"]];
        }
        
        self.wasSigned = @NO;
        
        if ([dictionary objectForKey:@"latitude"] != [NSNull null]) {
            self.lat = dictionary[@"latitude"];
        }else{
            self.lat = [NSNumber numberWithLong:0];
        }
        
        
        if ([dictionary objectForKey:@"longitude"] != [NSNull null]) {
            self.lng = dictionary[@"longitude"];
        }else{
            self.lng = [NSNumber numberWithLong:0];
        }
        
    }
    return self;
}



-(id)copyWithZone:(NSZone *)zone {
    RecipientSignature *copy = [[RecipientSignature allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.signatureImageData = self.signatureImageData;
    copy.address = self.address;
    copy.timeStamp = self.timeStamp;
    copy.userImageData = self.userImageData;
    copy.userImageTimestamp = self.userImageTimestamp;
    copy.lat = _lat;
    copy.lng = _lng;
    copy.wasSigned = self.wasSigned;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        self.idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        self.signatureImageData = [aDecoder decodeObjectForKey:@"signatureImageData"];
        self.address = [aDecoder decodeObjectForKey:@"address"];
        self.timeStamp = [aDecoder decodeObjectForKey:@"timeStamp"];
        self.userImageData = [aDecoder decodeObjectForKey:@"userImageData"];
        self.userImageTimestamp = [aDecoder decodeObjectForKey:@"userImageTimestamp"];
        self.lat = [aDecoder decodeObjectForKey:@"lat"];
        self.lng = [aDecoder decodeObjectForKey:@"lng"];
        self.wasSigned = [aDecoder decodeObjectForKey:@"wasSigned"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.idNumber forKey:@"idNumber"];
    [aCoder encodeObject:self.signatureImageData forKey:@"signatureImageData"];
    [aCoder encodeObject:self.address forKey:@"address"];
    [aCoder encodeObject:self.timeStamp forKey:@"timeStamp"];
    [aCoder encodeObject:self.userImageData forKey:@"userImageData"];
    [aCoder encodeObject:self.userImageTimestamp forKey:@"userImageTimestamp"];
    [aCoder encodeObject:self.lat forKey:@"lat"];
    [aCoder encodeObject:self.lng forKey:@"lng"];
    [aCoder encodeObject:self.wasSigned forKey:@"wasSigned"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Signature = address:%@, timeStamp:%@, userImageTimestamp:%@, wasSigned:%@", self.address, self.timeStamp, self.userImageTimestamp, self.wasSigned];
}

+ (NSDateFormatter *)dateWriter {
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateWriter = [dictionary objectForKey:@"SCDateWriter"];
    if (!dateWriter) {
        dateWriter = [[NSDateFormatter alloc] init];
        dateWriter.locale = [NSLocale currentLocale];
        dateWriter.timeZone = [NSTimeZone defaultTimeZone];
        dateWriter.dateStyle = NSDateFormatterMediumStyle;
        [dictionary setObject:dateWriter forKey:@"SCDateWriter"];
    }
    return dateWriter;
}

@end
