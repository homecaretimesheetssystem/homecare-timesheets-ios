//
//  SessionFile.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/28/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "SessionFile.h"

@implementation SessionFile

-(id)copyWithZone:(NSZone *)zone {
    SessionFile *copy = [[SessionFile allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.timeInPicture = _timeInPicture;
    copy.timeOutPicture = _timeOutPicture;
    copy.verificationPicture = _verificationPicture;
    copy.verificationID = _verificationID;
    return copy;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"timeInPhoto"] != [NSNull null]) {
            _timeInPicture = dictionary[@"timeInPhoto"];
        }else{
            _timeInPicture = @"";
        }
        
        if ([dictionary objectForKey:@"timeOutPhoto"] != [NSNull null]) {
            _timeOutPicture = dictionary[@"timeOutPhoto"];
        }else{
            _timeOutPicture = @"";
        }

        if ([dictionary objectForKey:@"verificationPhoto"] != [NSNull null]) {
            _verificationPicture = dictionary[@"verificationPhoto"];
        }else{
            _verificationPicture = @"";
        }
        
        if ([dictionary objectForKey:@"verificationID"] != [NSNull null]) {
            _verificationID = dictionary[@"verificationID"];
        }else{
            _verificationID = [NSNumber numberWithLong:0];
        }

    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _timeInPicture = [aDecoder decodeObjectForKey:@"timeInPhoto"];
        _timeOutPicture = [aDecoder decodeObjectForKey:@"timeOutPhoto"];
        _verificationPicture =[aDecoder decodeObjectForKey:@"verificationPhoto"];
        _verificationID = [aDecoder decodeObjectForKey:@"verificationID"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_timeInPicture forKey:@"timeInPhoto"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:_timeOutPicture forKey:@"timeOutPhoto"];
    [aCoder encodeObject:_verificationPicture forKey:@"verificationPhoto"];
    [aCoder encodeObject:_verificationID forKey:@"verificationID"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"idNumber:%@, timeInPicture:%@, timeOutPicture:%@, verificationPicture:%@, theVerificationID:%@",  _idNumber, _timeInPicture, _timeOutPicture, _verificationPicture, _verificationID];
}

@end
