//
//  RatioSharedCareViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 9/11/14.
//  Copyright (c) 2014 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "RatioSharedCareViewController.h"

@interface RatioSharedCareViewController ()

@end

@implementation RatioSharedCareViewController
//@synthesize currentSession;


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _currentUser = [User currentUser];
    _currentSession = _currentUser.currentSession;
    [self destroyUI];
    isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
    self.navigationItem.backBarButtonItem.title = @"";
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self createUI];
}

-(void)destroyUI {
    for (UIView *subview in [self.view subviews]) {
        [subview removeFromSuperview];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    long number = 3;
    
    _currentSession.continueTimesheetNumber = [NSNumber numberWithLong:number - 1];
    [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:@{@"continueTimesheetNumber":_currentSession.continueTimesheetNumber} andsuccess:^(id responseObject) {
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"errorSavingSessionData:%@", error);
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)createUI {
    scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, width, height - 64)];
    
    [scroller setContentSize:CGSizeMake(width, scroller.frame.size.height + 250)];
    scroller.delegate = self;
    scroller.userInteractionEnabled = YES;
    scroller.delaysContentTouches = NO;
    scroller.showsHorizontalScrollIndicator = NO;
    scroller.showsVerticalScrollIndicator = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    scroller.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    scroller.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    
    [self.view addSubview:scroller];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"Ratio + Shared Care";
    if (isIpad) {
        titleLabel.frame = CGRectMake(0, -30, width, 50);
        titleLabel.font = [UIFont fontWithName:@"Helvetica" size:50];
    } else {
        if (four) {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
            titleLabel.frame = CGRectMake(0, 9, width, 28);
        } else {
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:28];
            titleLabel.frame = CGRectMake(0, 14, width, 32);
        }
    }
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    titleLabel.textAlignment = NSTextAlignmentCenter;
   // [scroller addSubview:titleLabel];

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [scroller addGestureRecognizer:singleTap];
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, width, 50)];
    pickerToolbar.opaque = NO;
    UIBarButtonItem *prevButton = [[UIBarButtonItem alloc] initWithTitle:@"Prev" style:UIBarButtonItemStylePlain target:self action:@selector(prevTextField:)];
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextTextField)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:Nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissKeyboard)];
    NSArray *itemsArray = [NSArray arrayWithObjects:prevButton, nextButton, flex, doneButton, nil];
    pickerToolbar.items = itemsArray;

    ratioArray = _currentUser.possibleRatios;
    sharedLocationsArray = _currentUser.possibleCareLocations;
    ratioPicker = [[UIPickerView alloc] init];
    ratioPicker.delegate = self;
    ratioPicker.dataSource = self;
    locationPicker = [[UIPickerView alloc] init];
    locationPicker.delegate = self;
    locationPicker.dataSource = self;
    
    ratioTextField = [[UITextField alloc] init];
    if (isIpad) {
        ratioTextField.frame = CGRectMake(width * 0.55, 75, width * 0.4, 75);
        ratioTextField.font = [UIFont fontWithName:@"Helvetica" size:40];
    } else {
        if (four) {
            ratioTextField.frame = CGRectMake(width * 0.05, 20, width * 0.9, 50);
        } else {
            ratioTextField.frame = CGRectMake(width * 0.05, 60, width * 0.9, 50);
        }
        ratioTextField.font = [UIFont fontWithName:@"Helvetica" size:24];
    }
    ratioTextField.delegate = self;
    ratioTextField.inputView = ratioPicker;
    ratioTextField.inputAccessoryView = pickerToolbar;
    ratioTextField.backgroundColor = [UIColor whiteColor];
    ratioTextField.borderStyle = UITextBorderStyleBezel;
    ratioTextField.textAlignment = NSTextAlignmentCenter;
    [scroller addSubview:ratioTextField];
    if (_currentSession.ratio.ratio) {
        ratioTextField.text = _currentSession.ratio.ratio;
        long index = 0;
        for (Ratio *ratio in _currentUser.possibleRatios) {
            if ([_currentSession.ratio.ratio isEqualToString:ratio.ratio]) {
                arrayRowRatio = index;
                break;
            } else {
                index++;
            }
        }
        UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
        self.navigationItem.rightBarButtonItem = nextButton;
    } else {
        ratioTextField.text = ((Ratio*)[ratioArray objectAtIndex:0]).ratio;
        _currentSession.ratio = (Ratio *)ratioArray[0];
        arrayRowRatio = 0;
    }
    
    
    UILabel *ratioLabel = [[UILabel alloc] init];
    ratioLabel.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    ratioLabel.text = @"Ratio:";
    if (isIpad) {
        ratioLabel.font = [UIFont fontWithName:@"Helvetica" size:40];
        ratioLabel.frame = CGRectMake(0, ratioTextField.frame.origin.y, ratioTextField.frame.origin.x, ratioTextField.frame.size.height);
    } else {
        if (four) {
            ratioLabel.font = [UIFont fontWithName:@"Helvetica" size:21];
        }else {
            ratioLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
        }
        ratioLabel.frame = CGRectMake(0, ratioTextField.frame.origin.y - 40, width, 30);
    }
    ratioLabel.textColor = [UIColor whiteColor];
    ratioLabel.textAlignment = NSTextAlignmentCenter;
    [scroller addSubview:ratioLabel];
    
    
    locationTextField = [[UITextField alloc] init];
    locationTextField.delegate = self;
    if (isIpad) {
        locationTextField.frame = CGRectMake(width * 0.55, 200, width * 0.4, 75);
        locationTextField.font = [UIFont fontWithName:@"Helvetica" size:40];
    } else {
        if (four) {
            locationTextField.frame = CGRectMake(width * 0.05, 100, width * 0.9, 50);
        } else {
            locationTextField.frame = CGRectMake(width * 0.05, 150, width * 0.9, 50);
        }
        locationTextField.font = [UIFont fontWithName:@"Helvetica" size:24];
    }
    locationTextField.inputView = locationPicker;
    locationTextField.inputAccessoryView = pickerToolbar;
    locationTextField.backgroundColor = [UIColor whiteColor];
    locationTextField.borderStyle = UITextBorderStyleBezel;
    locationTextField.textAlignment = NSTextAlignmentCenter;
    [scroller addSubview:locationTextField];
    if (_currentSession.careLocation.sharedCareLocation) {
        locationTextField.text = _currentSession.careLocation.sharedCareLocation;
        long index = 0;
        for (CareLocation *loc in _currentUser.possibleCareLocations) {
            if ([_currentSession.careLocation.sharedCareLocation isEqualToString:loc.sharedCareLocation]) {
                arrayRow = index;
                break;
            } else {
                index++;
            }
        }
    } else {
        locationTextField.text = ((CareLocation*)[sharedLocationsArray objectAtIndex:0]).sharedCareLocation;
        arrayRow = 0;
        _currentSession.careLocation = sharedLocationsArray[0];
    }
    UILabel *sharedLabel = [[UILabel alloc] init];
    sharedLabel.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    sharedLabel.text = @"Shared Care Locations:";
    if (isIpad) {
        sharedLabel.font = [UIFont fontWithName:@"Helvetica" size:40];
        sharedLabel.frame = CGRectMake(0, locationTextField.frame.origin.y, locationTextField.frame.origin.x, locationTextField.frame.size.height);
    } else {
        if (four) {
            sharedLabel.font = [UIFont fontWithName:@"Helvetica" size:21];
        } else {
            sharedLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
        }
        sharedLabel.frame = CGRectMake(0, locationTextField.frame.origin.y - 30, width, 30);
    }
    sharedLabel.textAlignment = NSTextAlignmentCenter;
    sharedLabel.textColor = [UIColor whiteColor];
    [scroller addSubview:sharedLabel];
    
    ratioTextField.placeholder = @"Required";
    locationTextField.placeholder = @"Required";
    if (ratioTextField.text.length == 0 || [ratioTextField.text isEqualToString:@"1:1"]) {
        locationTextField.userInteractionEnabled = NO;
    } else {
        locationTextField.userInteractionEnabled = YES;
    }
    nextScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (isIpad) {
        nextScreenButton.frame = CGRectMake(width * 0.4, 440, width * 0.2, 50);
        [nextScreenButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:50]];
    } else {
        if (four) {
            nextScreenButton.frame = CGRectMake(width * 0.1, locationTextField.frame.origin.y + locationTextField.frame.size.height + 35, width * 0.8, 60);
        } else {
            nextScreenButton.frame = CGRectMake(width * 0.1, locationTextField.frame.origin.y + locationTextField.frame.size.height + 35, width * 0.8, 60);
        }
        [nextScreenButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:28]];
    }
    [nextScreenButton addTarget:self action:@selector(nextScreen) forControlEvents:UIControlEventTouchUpInside];
    [nextScreenButton setTitle:@"Next" forState:UIControlStateNormal];
    [nextScreenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextScreenButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    nextScreenButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    nextScreenButton.layer.borderWidth = 1;
    nextScreenButton.layer.borderColor = [UIColor whiteColor].CGColor;
    nextScreenButton.layer.cornerRadius = nextScreenButton.frame.size.height/2;
    
    [scroller addSubview:nextScreenButton];
    if (ratioTextField.text.length == 0) {
        nextScreenButton.alpha = 0.0;
    } else {
        nextScreenButton.alpha = 1.0;
    }
    [scroller setContentOffset:CGPointMake(scroller.contentOffset.x, scroller.contentOffset.y - titleLabel.frame.size.height)];
}

-(void)nextScreen {
    if (ratioTextField.text.length == 0 || locationTextField.text.length == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Required" message:@"Both Fields are Required." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
        [self presentViewController:alert animated:YES completion:NULL];
    } else {
        [self.view endEditing:YES];
        [self saveDataToSession];
    }
}


-(void)saveDataToSession {
    LoadingView *lv = [LoadingView addLoadingViewWithText:@"Saving"];
    [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:@{@"ratio":_currentSession.ratio.idNumber,@"sharedCareLocation":_currentSession.careLocation.idNumber} andsuccess:^(id responseObject) {
        [lv hideActivityIndicator];
        _currentUser.currentSession = _currentSession;
        [_currentUser save];
        if (_currentUser.possibleRecipients.count > 1) {
            ChooseUserViewController *vc2 = [[ChooseUserViewController alloc] init];
            [self.navigationController pushViewController:vc2 animated:YES];
        } else {
            if (_currentUser.possibleRecipients.count > 0) {
                return [self saveRecipientToCurrentSession];
            } else {
                [self.navigationController popToRootViewControllerAnimated:YES];
                [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"You have no assigned recipients. Please contact system administrator."] animated:YES completion:NULL];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"error:%@", error);
        
        [lv hideActivityIndicator];
    }];
}


-(void)saveRecipientToCurrentSession {
    _currentSession.currentTimecard.recipient = _currentUser.possibleRecipients[0];
    _currentUser.currentSession = _currentSession;
    [_currentUser save];
    [NetworkingHelper saveTimesheetDataWithTimesheetID:_currentSession.currentTimecard.idNumber andParams:@{@"recipient": _currentSession.currentTimecard.recipient.idNumber} andsuccess:^(id responseObject) {
        // The new call to check if verification comes in
        [NetworkingHelper checkVerificationForCamera:@{@"pcaid":_currentUser.pca.idNumber, @"recipientid":_currentSession.currentTimecard.recipient.idNumber} success:^(id responseObject) {
            NSLog(@"thisResponse object:%@", responseObject);
            if ([self string:self.currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
                
                if ([responseObject[@"verification_needed"] boolValue] == YES) {
                    _currentSession.verificationID = responseObject[@"id"];
//                    _currentSession.verificationID = @1461;
                    _currentSession.verificationIsRequiredForWorkerAndRecipient = @YES;
                    NSLog(@"ThisVerificationID is :%@", _currentSession.verificationID);
                    _currentUser.currentSession = _currentSession;
                    [_currentUser save];
                    [self showTheCamera];
                }
                else
                {
                    NSLog(@"currentTimesheetNumber:%@", _currentSession.currentTimesheetNumber);
                    [self showNextScreenThatIsntTheCamera];
                }
            }
            else
            {
                [self showNextScreenThatIsntTheCamera];
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"Failed to check verification:%@", error);
            [CrashlyticsKit recordError:error];
            [self showNextScreenThatIsntTheCamera];
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Failed to save recipient and pca:%@", error);
    }];
}

-(void)showNextScreenThatIsntTheCamera {
    id vc;
    if ([_currentSession.currentTimesheetNumber isEqualToNumber:@1]) {
        vc = [TimeInAndOutViewController new];
    }
    else
    {
        if ([self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"] || [self string:_currentUser.currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
            vc = [ActivityInfoViewController new];
        } else {
            vc = [PDFPreviewViewController new];
        }
    }
    [self.navigationController showViewController:vc sender:self];;
}

-(void)showTheCamera {
    [self getCameraPermissions:nil];
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        return NO;
    }
    return YES;
}

-(void)prevTextField:(UITextField *)textField {
    if (ratioTextField.isFirstResponder) {
        [self.view endEditing:YES];
    } else if (locationTextField.isFirstResponder) {
        [ratioTextField becomeFirstResponder];
    }
}

-(void)nextTextField {
    if (ratioTextField.isFirstResponder && ![ratioTextField.text isEqualToString:@"1:1"]) {
        [locationTextField becomeFirstResponder];
    } else {
        [self.view endEditing:YES];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (!arrayRow) {
        arrayRow = 0;
    }
    if (!arrayRowRatio) {
        arrayRowRatio = 0;
    }
    if (textField == ratioTextField) {
        textField.text = ((Ratio*)[ratioArray objectAtIndex:arrayRowRatio]).ratio;
        [ratioPicker selectRow:arrayRowRatio inComponent:0 animated:NO];
        if ([textField.text isEqualToString:@"1:1"]) {
            locationTextField.text = @"N/A";
            
        }
        [UIView animateWithDuration:0.3 animations:^{
            UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
            self.navigationItem.rightBarButtonItem = nextButton;
            nextScreenButton.alpha = 1.0;
        }];
    } else if (textField == locationTextField) {
        textField.text = ((CareLocation *)[sharedLocationsArray objectAtIndex:arrayRow]).sharedCareLocation;
        [locationPicker selectRow:arrayRow inComponent:0 animated:NO];
        if (!isIpad) {
            [UIView animateWithDuration:0.3 animations:^{
                if (four) {
                    scroller.contentOffset = CGPointMake(0, 5);
                } else {
                    scroller.contentOffset = CGPointMake(0, -20);
                }
            }];
            
        }
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    _currentSession.ratio = _currentUser.possibleRatios[arrayRowRatio];
    _currentSession.careLocation = _currentUser.possibleCareLocations[arrayRow];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITextView *textview in [self.view subviews]) {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [textview resignFirstResponder];
        }
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == ratioTextField && ![ratioTextField.text isEqualToString:@"1:1"]) {
        [locationTextField becomeFirstResponder];
    } else if (textField == locationTextField) {
        [self.view endEditing:YES];
    } else {
        [self.view endEditing:YES];
    }
    return YES;
}

#pragma mark - PickerView
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == ratioPicker) {
        return ratioArray.count;
    } else {
        return sharedLocationsArray.count;
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    if (pickerView == ratioPicker) {
        title = ((Ratio*)[ratioArray objectAtIndex:row]).ratio;
    } else {
        title = ((CareLocation*)[sharedLocationsArray objectAtIndex:row]).sharedCareLocation;
    }
    return title;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (pickerView == ratioPicker) {
        ratioTextField.text = ((Ratio*)[ratioArray objectAtIndex:row]).ratio;
        arrayRowRatio = row;
        if ([ratioTextField.text isEqualToString:@"1:1"]) {
            locationTextField.userInteractionEnabled = NO;
            locationTextField.text = @"N/A";
            [locationPicker selectRow:0 inComponent:0 animated:NO];
        } else {
            locationTextField.userInteractionEnabled = YES;
        }
    } else {
        locationTextField.text = ((CareLocation *)[sharedLocationsArray objectAtIndex:row]).sharedCareLocation;
        arrayRow = row;
    }
}

#pragma mark - Camera Methods

-(void)openProfilePictureCamera {
    
    JMGCameraViewController *camera = [JMGCameraViewController new];
    camera.parentNavBarStyle = self.navigationController.navigationBar.barStyle;
    camera.parentStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
    camera.cameraMode = CameraMode_square;
    camera.faceFrontOnLaunch = YES;
    // If no value for File type is specified, default will be png. Other available fileType is jpg
    camera.fileType = FileType_jpg;
    camera.showWhiteCircleInSquareMode = NO;
    camera.showPictureFromCameraRollForChooseFromPhotoLibraryButton = NO;
    
    // Add some text if you want to add a title above the square crop camera. If nil, no label will be added.
    camera.titleAboveCamera = @"Verification Picture";
    
    camera.cameraBackgroundColor = [UIColor colorWithWhite:0.05 alpha:1.0];
    
    // cameraMode Property must be set to custom for this to work.
    //    camera.fileSize = CGSizeMake(512., 1024.);
    if (!self.navigationController) {
        [self presentViewController:camera animated:YES completion:NULL];
    } else {
        [self.navigationController showViewController:camera sender:self];
    }
}

-(void)getCameraPermissions:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openProfilePictureCamera];
        });
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getCameraPermissions:sender];
                });
                
            }
            
        }];
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined) {
        if  (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [self getPhotoLibraryPermissions];
        }
    } else if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) {
        [self showCameraDeniedAlert];
    } else {
        [self showCameraDeniedAlert];
    }
    
}

-(void)showCameraDeniedAlert {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Camera Access Denied" message:[NSString stringWithFormat:@"We can't access your camera. To give us access, go to Settings -> %@ and allow camera permission to use this feature.", [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"]] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goToPhoneSettings];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)getPhotoLibraryPermissions {
    
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openProfilePictureCamera];
        });
    } else if (status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            } else if ([self cantAccessCameraOrPhotoLibrary]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showAccessAlert];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openProfilePictureCamera];
                });
            }
        }];
    } else if ([self cantAccessCameraOrPhotoLibrary]) {
        [self showAccessAlert];
    }
}

-(BOOL)cantAccessCameraOrPhotoLibrary {
    NSUInteger status = [PHPhotoLibrary authorizationStatus];
    if (([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied || [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusRestricted) && (status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)) {
        return YES;
    }
    return false;
}

-(void)showAccessAlert {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Photo Library Access Denied" message:[NSString stringWithFormat:@"We can't access your camera or photo library. To give us access, go to Settings -> %@ and allow access to camera or photos to use this feature.", [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey]] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goToPhoneSettings];
    }]];
    [self presentViewController:controller animated:YES completion:NULL];
}

-(void)goToPhoneSettings {
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:appSettings];
}


@end
