//
//  PDFCollectionViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/28/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "PDFCollectionViewController.h"
#import "PDFCollectionViewCell.h"

@implementation PDFCollectionViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Time Entry";
    _currentUser = [User currentUser];
    if ([_currentUser.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
        AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _locationManager = del.appLocationManager;
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [_locationManager startUpdatingLocation];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _currentUser = [User currentUser];
    [self.collectionView reloadData];
    if (_startingIndex) {
        [self.collectionView setContentOffset:CGPointMake(_startingIndex * self.view.frame.size.width, self.collectionView.contentOffset.y)];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _currentUser.signableSessions.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    PDFCollectionViewCell *cell = (PDFCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"pdfCell" forIndexPath:indexPath];
    Session *session = _currentUser.signableSessions[indexPath.row];
    cell.tag = indexPath.row;
    cell.navigationItem = self.navigationItem;
    cell.navigationController = self.navigationController;
    cell.locationManager = _locationManager;
    [cell sessionIsSet:session];
    cell.delegate = self;
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height);
}

#pragma mark - Delegate Methods

-(void)back {
    [_locationManager stopUpdatingLocation];
    [self.navigationController popViewControllerAnimated:YES];
}

-(CLLocation *)location {
    return self.locationManager.location;
}

-(void)setViewTitle:(NSString *)title {
    self.title = title;
}

-(void)presentAlertViewController:(UIAlertController *)alert {
    [self presentViewController:alert animated:YES completion:NULL];
}

-(void)finishSessionAndShouldRemoveSession:(BOOL)removeSession session:(Session *)session {
    [_locationManager stopUpdatingLocation];
    if (removeSession == YES) {
        [_currentUser removeSessionWithID:session.idNumber];
    } else {
        [_currentUser overWriteSessionWithID:session.idNumber withSession:session];
    }
    _currentUser = [User currentUser];
    if (_currentUser.signableSessions.count > 0) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

-(void)setScrollEnabled:(BOOL)enabled {
    self.collectionView.scrollEnabled = enabled;
}

#pragma mark - Location Manager Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [manager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == 3 || status == 4 || status == 5) {
        if ([_currentUser.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
            [manager startUpdatingLocation];
        }
    }
}

@end
