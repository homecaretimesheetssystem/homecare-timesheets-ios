//
//  Session.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 2/17/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "Session.h"

@implementation Session

-(instancetype)initWithDictionary:(NSDictionary *)dict andIndex:(long)index andUserType:(UserType)userType {
    if (self == [super init]) {
        if (dict[@"amazonUrl"] && ![dict[@"amazonUrl"] isEqual:[NSNull null]]) {
            _fileURL = dict[@"amazonUrl"];
        }
                
        _verificationIsRequiredForWorkerAndRecipient = [NSNumber numberWithBool:NO];
        /*   SESSION DATA    */
        if (dict[@"sessionData"] && [dict[@"sessionData"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *sessionDict = dict[@"sessionData"];
            [self getSessionDataFromDictionary:sessionDict andUserType:userType];
        } else if (dict[@"sessionData"] && [dict[@"sessionData"] isKindOfClass:[NSArray class]]) {
            [self getSessionDataFromDictionary:dict[@"sessionData"][index] andUserType:userType];
            
        }
    }
    return self;
}

-(void)getSessionDataFromDictionary:(NSDictionary *)sessionDict andUserType:(UserType)userType {
    NSLog(@"session dict is %@", sessionDict);
    if (sessionDict[@"id"]) {
        if (![sessionDict[@"id"] isEqual:[NSNull null]]) {
            _idNumber = sessionDict[@"id"];
        }
    }
    
    if (sessionDict[@"ratio"]) {
        _ratio = [[Ratio alloc] initWithDictionary:sessionDict[@"ratio"]];
    } else {
        _ratio = [Ratio new];
    }
    
    if (sessionDict[@"sharedCareLocation"]) {
        _careLocation = [[CareLocation alloc] initWithDictionary:sessionDict[@"sharedCareLocation"]];
    } else {
        _careLocation = [CareLocation new];
    }
    
    NSDictionary *timeInDict = sessionDict[@"timeIn"];
    if (timeInDict) {
        _timeIn = [[TimeIn alloc] initWithDictionary:timeInDict];
    } else {
        _timeIn = [TimeIn new];
    }
    
    NSDictionary *timeOutDict = sessionDict[@"timeOut"];
    if (timeOutDict) {
        _timeOut = [[TimeOut alloc] initWithDictionary:timeOutDict];
    } else {
        _timeOut = [TimeOut new];
    }
    
    if (sessionDict[@"currentTimesheetNumber"]) {
        _currentTimesheetNumber = sessionDict[@"currentTimesheetNumber"];
    } else {
        _currentTimesheetNumber = @1;
    }
    /*
     if (sessionDict[@"flagForFurtherInvestigation"]) {
     _flagForFurtherInvestigation = [NSNumber numberWithBool:[dict[@"flagForFurtherInvestigation"] boolValue]];
     } else {
     _flagForFurtherInvestigation = [NSNumber numberWithBool:@NO];
     }
     */
//    NSLog(@"sessionDict[@\"continueTimesheetNumber\"]:%@", sessionDict[@"continueTimesheetNumber"]);
    if (sessionDict[@"continueTimesheetNumber"]) {
        _continueTimesheetNumber = sessionDict[@"continueTimesheetNumber"];
    } else {
        _continueTimesheetNumber = @0;
    }
    
    if (sessionDict[@"currentTimesheetId"]) {
        _currentTimesheetID = sessionDict[@"currentTimesheetId"];
    } else {
        _currentTimesheetID = @0;
    }
    
    if (sessionDict[@"service"]) {
        _service = [[Service alloc] initWithDictionary:sessionDict[@"service"]];
    } else {
        _service = [Service new];
    }
    
    if (sessionDict[@"dateEnding"]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = kDateFormat;
        NSDate *endingDate = [dateFormatter dateFromString:sessionDict[@"dateEnding"]];
        dateFormatter.dateStyle = NSDateFormatterShortStyle;
        _dateEnding = [dateFormatter stringFromDate:endingDate];
    } else {
        NSDateFormatter *formatter = [NSDateFormatter new];
        _dateEnding = [formatter stringFromDate:[NSDate date]];
    }
    
    if (sessionDict[@"sessionFiles"]) {
        _sessionFile = [[SessionFile alloc] initWithDictionary:sessionDict[@"sessionFiles"]];
    } else {
        _sessionFile = [SessionFile new];
    }
    if (sessionDict[@"pca"] != [NSNull null]) {
        
        _pca = [[PCA alloc] initWithDictionary:sessionDict[@"pca"]];
    }
    if (sessionDict[@"timesheets"]) {
        NSMutableArray *mutArr = [NSMutableArray new];
        NSMutableArray *allTimesheetsMutArray = [NSMutableArray new];
        NSArray *timesheetsArray = sessionDict[@"timesheets"];
        if (timesheetsArray.count == 1) {
            _currentTimecard = [[Timecard alloc] initWithDictionary:timesheetsArray[0]];
        } else {
            for (NSDictionary *timecardDict in timesheetsArray) {
                if ([_currentTimesheetID isEqualToNumber:[timecardDict objectForKey:@"id"]]) {
                    _currentTimecard = [[Timecard alloc] initWithDictionary:timecardDict];
                    break;
                } else if ([_currentTimesheetID isEqualToNumber:@0] && userType == UserType_IsRecipient) {
                    _currentTimecard = [[Timecard alloc] initWithDictionary:timecardDict];
                    break;
                } else {
                    _currentTimecard = [Timecard new];
                }
                Timecard *tc = [[Timecard alloc] initWithDictionary:timecardDict];
                [allTimesheetsMutArray addObject:tc];
            }
        }
        for (NSDictionary *timecardDict in timesheetsArray) {
            [mutArr addObject:timecardDict[@"id"]];
        }
        _timesheetIDsArray = mutArr;
        _recipientTimesheetsArray = allTimesheetsMutArray;
        
    } else {
        _currentTimecard = [Timecard new];
        _recipientTimesheetsArray = [NSMutableArray new];
    }
    
    
}

-(void)resetLocalValues {
    self.currentTimecard = [Timecard new];
    self.service = [Service new];
    self.continueTimesheetNumber = @1;
    self.dateEnding = nil;
    self.ratio = [Ratio new];
    self.timeIn = [TimeIn new];
    self.timeOut = [TimeOut new];
    self.sessionFile = [SessionFile new];
    self.pdfState = PDFState_Default;
    self.pca = [PCA new];
//    self.pca = ((Session *)[User currentUser].sessions[0]).pca;
}

-(id)copyWithZone:(NSZone *)zone {
    Session *copy = [[Session allocWithZone:zone] init];
    copy.idNumber = self.idNumber;
    copy.currentTimecard = self.currentTimecard;
    copy.currentTimesheetNumber = self.currentTimesheetNumber;
    copy.service = self.service;
    copy.continueTimesheetNumber = self.continueTimesheetNumber;
    copy.dateEnding = self.dateEnding;
    copy.datesInHospital = self.datesInHospital;
    copy.ratio = self.ratio;
    copy.timeIn = self.timeIn;
    copy.timeOut = self.timeOut;
    copy.currentTimesheetID = _currentTimesheetID;
    copy.verificationID = _verificationID;
    copy.timesheetIDsArray = _timesheetIDsArray;
    copy.verificationIsRequiredForWorkerAndRecipient = _verificationIsRequiredForWorkerAndRecipient;
    copy.fileURL = _fileURL;
    copy.sessionFile = self.sessionFile;
    copy.selectedRecipients = _selectedRecipients;
    copy.pdfState = _pdfState;
    copy.pca = _pca;
    copy.onDemandCareOptionsArray = _onDemandCareOptionsArray;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        self.idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        self.currentTimecard = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"currentTimecard"]];
        self.currentTimesheetNumber = [aDecoder decodeObjectForKey:@"currentTimesheetNumber"];
        self.service = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"service"]];
        self.continueTimesheetNumber = [aDecoder decodeObjectForKey:@"continueTimesheetNumber"];
        self.dateEnding = [aDecoder decodeObjectForKey:@"dateEnding"];
        self.datesInHospital = [aDecoder decodeObjectForKey:@"datesInHospital"];
        self.ratio = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"ratio"]];
        self.timeIn = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"timeIn"]];
        self.timeOut = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"timeOut"]];
        self.currentTimesheetID = [aDecoder decodeObjectForKey:@"currentTimesheetID"];
        self.verificationID = [aDecoder decodeObjectForKey:@"verificationID"];
        self.timesheetIDsArray = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"timesheetIDsArray"]];
        self.verificationIsRequiredForWorkerAndRecipient = [aDecoder decodeObjectForKey:@"verificationIsRequiredForWorkerAndRecipient"];
        self.fileURL = [aDecoder decodeObjectForKey:@"fileURL"];
        self.sessionFile = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"sessionFile"]];
        self.selectedRecipients = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"selectedRecipients"]];
        self.pdfState = [aDecoder decodeIntegerForKey:@"pdfState"];
        self.pca = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"sessionpca"]];
        self.onDemandCareOptionsArray = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"onDemandCareOptionsArray"]];

    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.idNumber forKey:@"idNumber"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.currentTimecard] forKey:@"currentTimecard"];
    [aCoder encodeObject:self.currentTimesheetNumber forKey:@"currentTimesheetNumber"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.service] forKey:@"service"];
    [aCoder encodeObject:self.continueTimesheetNumber forKey:@"continueTimesheetNumber"];
    [aCoder encodeObject:self.dateEnding forKey:@"dateEnding"];
    [aCoder encodeObject:self.datesInHospital forKey:@"datesInHospital"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.ratio] forKey:@"ratio"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.timeIn] forKey:@"timeIn"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.timeOut] forKey:@"timeOut"];
    [aCoder encodeObject:self.currentTimesheetID forKey:@"currentTimesheetID"];
    [aCoder encodeObject:self.verificationID forKey:@"verificationID"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.timesheetIDsArray] forKey:@"timesheetIDsArray"];
    [aCoder encodeObject:self.verificationIsRequiredForWorkerAndRecipient forKey:@"verificationIsRequiredForWorkerAndRecipient"];
    [aCoder encodeObject:self.fileURL forKey:@"fileURL"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.sessionFile] forKey:@"sessionFile"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.selectedRecipients] forKey:@"selectedRecipients"];
    [aCoder encodeInteger:_pdfState forKey:@"pdfState"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.pca] forKey:@"sessionpca"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.onDemandCareOptionsArray] forKey:@"onDemandCareOptionsArray"];

}

-(NSString *)description {
    return [NSString stringWithFormat:@"Session ID: %@, Service: %@, ContinueTimesheetNumber: %@, Date Ending: %@, Ratio: %@, Time In:%@, Time Out:%@, CurrentTimecard:%@, VerificationID:%@, VerificationIsRequired:%@, fileURL:%@, selectedRecipients:%@, timesheetIDsArray:%@, pdfState:%@ pca:%@\r OnDemandCareOptionsArray:%@", self.idNumber, self.service, self.continueTimesheetNumber, self.dateEnding, self.ratio, self.timeIn, self.timeOut, self.currentTimecard, _verificationID,  _verificationIsRequiredForWorkerAndRecipient, _fileURL, self.selectedRecipients, self.timesheetIDsArray, [self stringForPDFState], _pca, _onDemandCareOptionsArray];
}

-(NSString *)stringForPDFState {
    switch (_pdfState) {
        case PDFState_Default:
            return @"PDFState_Default";
            break;
            
        case PDFState_ProceedWarningAccepted:
            return @"PDFState_ProceedWarningAccepted";
            break;
            
            
        case PDFState_FraudWarningAccepted:
            return @"PDFState_FraudWarningAccepted";
            break;
            
        case PDFState_SignatureApplied:
            return @"PDFState_SignatureApplied";
            break;
        
        default:
            return @"PDFState_Default";
            break;
    }
}

@end
