//
//  PDFCollectionViewCell.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/28/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "PDFCollectionViewCell.h"

@implementation PDFCollectionViewCell {
    CLGeocoder *geoCoder; NSString *weekDay; NSData *csvDataString;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor whiteColor];
}

-(void)sessionIsSet:(Session*)session {
    _currentUser = [User currentUser];
    _currentUser.currentSession = session;
    _currentSession = session;
    [_currentUser save];
    [self createTimeCardFile];
    isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    width = self.frame.size.width;
    height = self.frame.size.height;
    four = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960));
    if (isIpad) {
        width = 1024;
        height = 768;
    }
    geoCoder = [[CLGeocoder alloc] init];
    [self figureOutDayOfWeekAndSetArray];
    
    _webView.scrollView.contentOffset = CGPointMake(0, 64);
    [_webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
    
    
    
    [self buttonLogic];
}

-(void)buttonLogic {
    
    if (_currentSession.currentTimecard.file.recipientSignature) {
        _currentSession.currentTimecard.recipientSignature.wasSigned = @YES;
    }
    if (_currentSession.currentTimecard.file.pcaSignature) {
        _currentSession.currentTimecard.pcaSignature.wasSigned = @YES;
    }
    
    
    if (isIpad) {
        [_signButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:40]];
    }
    [_sendPDFButton.titleLabel setFont:_signButton.titleLabel.font];
    _saveAndExitButton.titleLabel.font = _signButton.titleLabel.font;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(sendTimesheetAndFinish:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(back)];
    
    if ([_currentSession.currentTimecard.recipientSignature.wasSigned isEqualToNumber:@YES] && [_currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES]) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
        _sendPDFButton.alpha = 1.0;
        _signButton.alpha = 0.0;
        _saveAndExitButton.alpha = 1.0;
    } else {
        _sendPDFButton.alpha = 0.0;
        _signButton.alpha = 1.0;
        _saveAndExitButton.alpha = 1.0;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

-(IBAction)saveAndExitPressed:(id)sender {
    [self finishSessionAndShouldRemoveSession:NO];
}

-(void)userIsRecipient {
    
    [self showFraudWarningFor:SignatureIsFor_Recipient];
    
}

-(void)updateCurrentSessionWithParams:(NSDictionary *)params {
    [NetworkingHelper saveSessionDataWithSessionNumber:_currentSession.idNumber andParams:params andsuccess:^(id responseObject) {
        NSLog(@"updateSessionSuccess:%@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"update session fail:%@", error);
    }];
}

#pragma mark - Location Delegate

-(void)back {
    [self.delegate back];
}

-(void)figureOutDayOfWeekAndSetArray {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateStyle:NSDateFormatterShortStyle];
    
    NSDate *dateEnding = [format dateFromString:_currentSession.dateEnding];
    [format setDateFormat:@"EEEE"];
    weekDay = [format stringFromDate:dateEnding];
}

#pragma mark - Save Timecard and Navigation

-(void)showSuccessAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Time entry was sent successfully" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self finishSessionAndShouldRemoveSession:YES];
    }]];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    [self.delegate presentAlertViewController:alert];
}

-(IBAction)sendTimesheetAndFinish:(id)sender {
    LoadingView *lv = [LoadingView addLoadingViewWithText:@"Saving"];
//    NSLog(@"currentSession b4 sennding:%@", _currentSession.currentTimecard);
    
    [NetworkingHelper saveFileWithFileIDNumber:_currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetPdfFile", @"fileName":@"timesheetPdfFile.pdf", @"fileData":[self generatePDF], @"mimeType":@"application/pdf", @"timesheet":_currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
        [NetworkingHelper saveFileWithFileIDNumber:_currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetCsvFile", @"fileName":@"timesheetCsvFile.csv", @"fileData":csvDataString, @"mimeType":@"text/csv", @"timesheet":_currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
            [NetworkingHelper finishSessionWithSessionID:_currentSession.idNumber andSuccess:^(id responseObject) {
                
                [lv removeFromSuperview];
                [self finishSessionAndShouldRemoveSession:YES];
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
                [lv hideActivityIndicator];
                [self.delegate presentAlertViewController:[ErrorAlertController alertControllerWithMessage:@"Time entry was not successfully saved. Check your internet connection and try again."]];
            }];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
            [self.delegate presentAlertViewController:[ErrorAlertController alertControllerWithMessage:@"Time entry was not successfully saved. Check your internet connection and try again."]];
            [lv removeFromSuperview];
        }];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        [lv removeFromSuperview];
        [self.delegate presentAlertViewController:[ErrorAlertController alertControllerWithMessage:@"Time entry was not successfully saved. Check your internet connection and try again."]];
    }];
}

-(void)goToNextRecipientTimesheet {
    [NetworkingHelper getSessionDataWithSuccess:^(id responseObject) {
        NSLog(@"responseObject:%@", responseObject);
        // This tells if we have another timecard to sign off on or not.
        _currentUser = [[User alloc] initWithDictionary:responseObject];
        [_currentUser save];
        if (_currentUser.sessions.count > 0) {
            _currentUser.currentSession = _currentUser.sessions[0];
        }
        _currentSession = _currentUser.currentSession;
        if (_currentSession.currentTimecard.file.recipientSignature) {
            _currentSession.currentTimecard.recipientSignature.wasSigned = @YES;
        }
        if (_currentSession.currentTimecard.file.pcaSignature) {
            _currentSession.currentTimecard.pcaSignature.wasSigned = @YES;
        }
//        NSLog(@"currentUser:%@", _currentUser);
        if ([_currentSession.currentTimecard.pcaSignature.wasSigned isEqualToNumber:@YES]) {
            UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:@"New Time Entry" message:@"You have a new time entry to sign. Apply this signature to all outstanding time entries?" preferredStyle:UIAlertControllerStyleActionSheet];
            alert2.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            alert2.popoverPresentationController.sourceView = self;
            CGRect rect = CGRectMake(_webView.center.x - 400, _webView.center.y - 250, 200, 500);
            alert2.popoverPresentationController.sourceRect = rect;
            [alert2 addAction:[UIAlertAction actionWithTitle:@"View Next Time Entry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [_webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
                [self buttonLogic];
                NSLog(@"next time entry button hit");
            }]];
            
            [alert2 addAction:[UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self finishSessionAndShouldRemoveSession:NO];
            }]];
            
            alert2.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self.delegate presentAlertViewController:alert2];
        } else {
            //            [self showSuccessAlert];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Outstanding Time Entries" message:@"Time entry sent successfully. There are no more time entries that require a signature." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self finishSessionAndShouldRemoveSession:YES];
            }]];
            alert.view.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
            [self.delegate presentAlertViewController:alert];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        
    }];
}

-(void)finishSessionAndShouldRemoveSession:(BOOL)removeSession {
    [self.delegate finishSessionAndShouldRemoveSession:removeSession session:_currentSession];
}

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        //        NSLog(@"return NO");
        return NO;
    }
    return YES;
}

#pragma mark - PDF

-(NSData *)generatePDF {
    
    //    NSLog(@"currentSession:%@", currentSession);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filename = [NSString stringWithFormat:@"Time entry for %@ %@.pdf", _currentUser.currentSession.pca.firstName, _currentUser.currentSession.pca.lastName];
    
    filename = [self _sanitizeFileNameString:filename];
    
    NSString *newDocumentPath = [documentsDirectory stringByAppendingPathComponent:filename];
    NSString *templatePath = [[NSBundle mainBundle] pathForResource:[self pdfNameForService:_currentUser.currentSession.service.serviceName] ofType:@"pdf"];    
    
    if ([fileManager fileExistsAtPath:newDocumentPath]) {
        [[NSFileManager defaultManager] removeItemAtPath: newDocumentPath error: &error];
    }
    
    [fileManager copyItemAtPath:templatePath toPath:newDocumentPath error:&error];
    
    NSURL *documentURL = [NSURL fileURLWithPath:newDocumentPath];
    
    CGPDFDocumentRef ourPDF = CGPDFDocumentCreateWithURL((CFURLRef)documentURL);
    
    
    // const size_t numberOfPages = CGPDFDocumentGetNumberOfPages(ourPDF);
    NSMutableData* data = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(data, CGRectZero, nil);
    
    
    CGPDFPageRef pdfPage = CGPDFDocumentGetPage(ourPDF, 1);
    const CGRect pageFrame = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
    
    UIGraphicsBeginPDFPageWithInfo(pageFrame, nil);
    
    // Draw the page (flipped)
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -pageFrame.size.height);
    CGContextDrawPDFPage(ctx, pdfPage);
    CGContextRestoreGState(ctx);
    
    //Top of the PDF ================================================================================
    
    
    
    NSString *checkMarkString = @"\u2713";
//    [[UIColor blackColor] set];
//    if ([self string:_currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
//        [checkMarkString drawAtPoint:CGPointMake(182, 92) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:18]}];
//    } else if ([self string:_currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
//        [checkMarkString drawAtPoint:CGPointMake(328, 92) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:18]}];
//    }
    
    
    [_currentUser.agency.name drawAtPoint:CGPointMake(120, 122) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    [_currentUser.agency.phone drawAtPoint:CGPointMake(442, 122) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    
    //Change back to above if we make this more widely available.
    
    if (_currentSession.dateEnding) {
        //        NSLog(@"pdf dateEnding:%@", currentSession.dateEnding);
        [[self dateEndingString] drawAtPoint:CGPointMake(208, 160) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:16]}];
    }
    
    
    // Activities Part of the PDF
    
    
    long activitiesCount = _currentUser.possibleCareOptions.count;
    NSMutableArray *dayActivitiesArray = [NSMutableArray arrayWithCapacity:activitiesCount];
    for (int i = 0; i < activitiesCount; i ++) {
        
        CareOption *possibleOption = (CareOption *)_currentUser.possibleCareOptions[i];
        if (_currentSession.currentTimecard.careOptionsArray.count > 0) {
            long lastIndex = _currentSession.currentTimecard.careOptionsArray.count - 1;
            for (int j = 0; j < _currentSession.currentTimecard.careOptionsArray.count; j++) {
                NSNumber *optionID = _currentSession.currentTimecard.careOptionsArray[j];
                if ([possibleOption.idNumber isEqualToNumber:optionID]) {
                    [dayActivitiesArray insertObject:@"1" atIndex:i];
                    if (i < 10) {
                        long x = 264;
                        if ([self string:_currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
                            x = 498;
                        }
                        int y = 206 + (28.1 * i);
                        [checkMarkString drawAtPoint:CGPointMake(x, y) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:32]}];
                    } else {
                        int y = 206 + (28.1 * (i - 10));
                        [checkMarkString drawAtPoint:CGPointMake(498, y) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:32]}];
                    }
                    break;
                } else {
                    if (j == lastIndex) {
                        [dayActivitiesArray insertObject:@"0" atIndex:i];
                    }
                }
            }
        } else {
            [dayActivitiesArray insertObject:@"0" atIndex:i];
        }
    }
    
    // Total Hours
    NSDateFormatter *shortFormatter = [[NSDateFormatter alloc] init];
    [shortFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc] init];
    [shortDateFormatter setDateStyle:NSDateFormatterShortStyle];
    [shortDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSDateFormatter *medFormatter = [[NSDateFormatter alloc] init];
    [medFormatter setTimeStyle:NSDateFormatterMediumStyle];
    NSString *timeInString;
    if (_currentSession.timeIn.timeIn) {
        timeInString = [shortFormatter stringFromDate:_currentSession.timeIn.timeIn];
        //        NSLog(@"timeInString:%@", timeInString);
        bool isAm;
        NSArray *stringAr = [timeInString componentsSeparatedByString:@" "];
        NSString *one = [stringAr objectAtIndex:0];
        [one drawAtPoint:CGPointMake(454, 404.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:20]}];
        if (stringAr.count > 1) {
            NSString *two = [stringAr objectAtIndex:1];
            if ([two isEqualToString:@"AM"]) {
                isAm = YES;
            } else {
                isAm = NO;
            }
            
            CGRect circleRect;
            if (isAm) {
                circleRect = CGRectMake(511.2, 403, 22, 10);
            } else {
                circleRect = CGRectMake(512, 414.4, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        } else {
            NSArray *timeStringArray = [one componentsSeparatedByString:@":"];
            //            NSLog(@"timeStringArray[0]:%@", timeStringArray[0]);
            if ([timeStringArray[0] longLongValue] < 12) {
                isAm = YES;
            } else {
                isAm = NO;
            }
            CGRect circleRect;
            if (isAm) {
                circleRect = CGRectMake(511.2, 403, 22, 11);
            } else {
                circleRect = CGRectMake(512, 414.4, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        }
    } else {
        timeInString = @"na";
    }
    
    NSString *timeOutString;
    if (_currentSession.timeOut.timeOut) {
        //        NSDate *convertDate = [medFormatter dateFromString:currentSession.timeOut.timeOut];
        timeOutString = [shortFormatter stringFromDate:_currentSession.timeOut.timeOut];
        bool isAM;
        NSArray *stringAr = [timeOutString componentsSeparatedByString:@" "];
        NSString *one = [stringAr objectAtIndex:0];
        [one drawAtPoint:CGPointMake(454, 432) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:20]}];
        if (stringAr.count > 1) {
            NSString *two = [stringAr objectAtIndex:1];
            if ([two isEqualToString:@"AM"]) {
                isAM = YES;
            } else {
                isAM = NO;
            }
            
            CGRect circleRect;
            if (isAM) {
                circleRect = CGRectMake(511.2, 430, 22, 11);
            } else {
                circleRect = CGRectMake(512, 441.5, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        } else {
            NSArray *timeStringArray = [one componentsSeparatedByString:@":"];
            if ([timeStringArray[0] longLongValue] < 12) {
                isAM = YES;
            } else {
                isAM = NO;
            }
            CGRect circleRect;
            if (isAM) {
                circleRect = CGRectMake(511.2, 430, 22, 11);
            } else {
                circleRect = CGRectMake(512, 441.5, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        }
    } else {
        timeOutString = @"na";
    }
    
    CGRect dayCircle;
    if ([_currentSession.ratio.ratio isEqualToString:@"1:1"]) {
        dayCircle = CGRectMake(439.5, 336, 28, 26);
    } else if ([_currentSession.ratio.ratio isEqualToString:@"1:2"]) {
        dayCircle = CGRectMake(476, 336, 28, 26);
    } else if ([_currentSession.ratio.ratio isEqualToString:@"1:3"]) {
        dayCircle = CGRectMake(507, 336, 28, 26);
    } else {
        dayCircle = CGRectZero;
    }
    
    UIBezierPath *monPath = [UIBezierPath bezierPathWithOvalInRect:dayCircle];
    [[UIColor blackColor] setStroke];
    [monPath stroke];
    
    if (![timeInString isEqualToString:@"na"]) {
        //        currentTimecard.sharedCareLocation = @"Community";
        if ([self string:_currentSession.careLocation.sharedCareLocation containsCaseInsensitiveString:@"Home"]) {
            [_currentSession.careLocation.sharedCareLocation drawAtPoint:CGPointMake(454, 370) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:24]}];
        } else if ([self string:_currentSession.careLocation.sharedCareLocation containsCaseInsensitiveString:@"Community"]) {
            [_currentSession.careLocation.sharedCareLocation drawAtPoint:CGPointMake(442.25, 376) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:15]}];
        } else {
            [_currentSession.careLocation.sharedCareLocation drawAtPoint:CGPointMake(467, 370) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:24]}];
        }
        
    }
    
    NSString *totalHoursString = [self convertSecondsToString:_currentSession.timeOut.billableHours];
    [totalHoursString drawAtPoint:CGPointMake(450, 465.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:20]}];
    
    //GPS Locations
    NSLog(@"current session time in address %@", _currentSession.timeIn);
    
    if (_currentSession.timeIn.address) {
        [_currentSession.timeIn.address drawAtPoint:CGPointMake(135, 554.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    }
    
    if (_currentSession.timeOut.address) {
        [_currentSession.timeOut.address drawAtPoint:CGPointMake(149, 588.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    }
    
    //Bottom of the PDF
    
    if (_currentSession.currentTimecard.recipient.firstName || _currentSession.currentTimecard.recipient.lastName) {
        NSString *fullRecName = [NSString stringWithFormat:@"%@ %@",_currentSession.currentTimecard.recipient.firstName, _currentSession.currentTimecard.recipient.lastName];
        [fullRecName drawAtPoint:CGPointMake(70, 680) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:15]}];
    }
    if (_currentSession.currentTimecard.recipient.maNumber) {
        [_currentSession.currentTimecard.recipient.maNumber drawAtPoint:CGPointMake(238, 680) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    }
    if (_currentUser.currentSession.pca.firstName || _currentUser.currentSession.pca.lastName) {
        NSString *fullPCAName = [NSString stringWithFormat:@"%@ %@", _currentUser.currentSession.pca.firstName, _currentUser.currentSession.pca.lastName];
        [fullPCAName drawAtPoint:CGPointMake(70, 708) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:15]}];
    }
    if (_currentUser.currentSession.pca.umpi) {
        [_currentUser.currentSession.pca.umpi drawAtPoint:CGPointMake(233, 710) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:12]}];
    }
    if (_currentSession.currentTimecard.file.recipientSignature && !_currentSession.currentTimecard.recipientSignature.signatureImageData) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", _currentSession.fileURL, _currentSession.currentTimecard.file.recipientSignature]];
        _currentSession.currentTimecard.recipientSignature.signatureImageData = [NSData dataWithContentsOfURL:url];
        [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
    }
    
    if (_currentSession.currentTimecard.recipientSignature.signatureImageData) {
        UIImage *image = [UIImage imageWithData:_currentSession.currentTimecard.recipientSignature.signatureImageData];
        if (!isIpad) {
            image = [self rotateImage:image];
            [image drawInRect:CGRectMake(339, 672, 131, 26.5)];
        } else {
            [image drawInRect:CGRectMake(339, 674, 131, 26.5)];
        }
        
        if (_currentSession.currentTimecard.recipientSignature.timeStamp) {
            NSString *timeStampString = [shortDateFormatter stringFromDate:_currentSession.currentTimecard.recipientSignature.timeStamp];
            [timeStampString drawAtPoint:CGPointMake(487, 680) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:13]}];
        }
    }
    
    //    NSLog(@"currentSession.currentTimecard.file.pcaSignature:%@", currentSession.currentTimecard.file.pcaSignature);
    if (_currentSession.currentTimecard.file.pcaSignature && !_currentSession.currentTimecard.pcaSignature.signatureImageData) {
        NSLog(@"string:%@", [NSString stringWithFormat:@"%@%@", _currentSession.fileURL, _currentSession.currentTimecard.file.pcaSignature]);
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", _currentSession.fileURL, _currentSession.currentTimecard.file.pcaSignature]];
        _currentSession.currentTimecard.pcaSignature.signatureImageData = [NSData dataWithContentsOfURL:url];
    }
    if (_currentSession.currentTimecard.pcaSignature.signatureImageData) {
        UIImage *image = [UIImage imageWithData:_currentSession.currentTimecard.pcaSignature.signatureImageData];
        if (!isIpad) {
            image = [self rotateImage:image];
            [image drawInRect:CGRectMake(339, 705, 131, 26.5)];
        } else {
            [image drawInRect:CGRectMake(339, 702.5, 131, 26.5)];
        }
        if (_currentSession.currentTimecard.pcaSignature.timeStamp) {
            NSString *timeStampString = [shortDateFormatter stringFromDate:_currentSession.currentTimecard.pcaSignature.timeStamp];
            [timeStampString drawAtPoint:CGPointMake(487, 710) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:13]}];
        }
    }
    
    
    
    
    //Create CSV File from currentTimecard Vars - WeekendingDate, dates, activities, total time on date, total time week, names, dob, date signed.
    
    
    // @"inpatient_incarcerated", @"start_date_inpatient_incarcerated", @"end_date_inpatient_incarcerated",
    NSArray *fieldsArray = [NSArray arrayWithObjects:@"state", @"service", @"agency_name", @"phone_number", @"day_of_service", @"date_of_service", @"recipient_first_name", @"recipient_last_name", @"recipient_ma_number", @"recipient_company_assigned_id", @"recipient_date_signed", @"pca_first_name", @"pca_last_name", @"pca_npi_umpi", @"pca_company_assigned_id", @"pca_date_signed", @"dressing", @"grooming", @"bathing", @"eating", @"transfers", @"mobility", @"positioning", @"toileting", @"health_related", @"behavior", @"light_housekeeping", @"laundry", @"other", @"visit_one_ratio", @"visit_one_location", @"visit_one_time_in", @"visit_one_time_in_address", @"visit_one_time_out", @"visit_one_time_out_address", @"recipient_signature_image_timestamp", @"pca_signature_image_timestamp", @"daily_total", nil];
    
    NSMutableString *csvString = [[NSMutableString alloc] init];
    [csvString appendString:[fieldsArray componentsJoinedByString:@","]];
    [csvString appendString:@"\n"];
    
    _currentUser.agency.state = @"Minnesota";
    NSString *pcaDateString;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    if (_currentSession.currentTimecard.pcaSignature.timeStamp) {
        pcaDateString = [dateFormat stringFromDate:_currentSession.currentTimecard.pcaSignature.timeStamp];
    } else {
        pcaDateString = @"na";
    }
    NSString *recipDateString;
    if (_currentSession.currentTimecard.recipientSignature.timeStamp) {
        recipDateString = [dateFormat stringFromDate:_currentSession.currentTimecard.recipientSignature.timeStamp];
    } else {
        recipDateString = @"na";
    }
    
    if (!_currentSession.currentTimecard.recipientSignature.address) {
        _currentSession.currentTimecard.recipientSignature.address = @"n/a";
    }
    if (!_currentSession.currentTimecard.pcaSignature.address) {
        _currentSession.currentTimecard.pcaSignature.address = @"n/a";
    }
    
    if (!_currentSession.timeIn.address) {
        _currentSession.timeIn.address = @"na";
        
    }
    
    if (!_currentSession.timeOut.address) {
        _currentSession.timeOut.address = @"na";
    }
    if (dayActivitiesArray.count != 13) {
        dayActivitiesArray = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", nil];
    }
    [csvString appendString:[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@\n", _currentUser.agency.state, _currentSession.service.serviceName, _currentUser.agency.name, _currentUser.agency.phone, weekDay, _currentSession.dateEnding, _currentSession.currentTimecard.recipient.firstName, _currentSession.currentTimecard.recipient.lastName, _currentSession.currentTimecard.recipient.maNumber, _currentSession.currentTimecard.recipient.companyAssignedID, recipDateString, _currentUser.currentSession.pca.firstName, _currentUser.currentSession.pca.lastName, _currentUser.currentSession.pca.umpi, _currentUser.currentSession.pca.companyAssignedID, pcaDateString, dayActivitiesArray[0], dayActivitiesArray[1], dayActivitiesArray[2], dayActivitiesArray[3], dayActivitiesArray[4], dayActivitiesArray[5], dayActivitiesArray[6], dayActivitiesArray[7], dayActivitiesArray[8], dayActivitiesArray[9], dayActivitiesArray[10], dayActivitiesArray[11], dayActivitiesArray[12], _currentSession.ratio.ratio, _currentSession.careLocation.sharedCareLocation, timeInString, _currentSession.timeIn.address, timeOutString, _currentSession.timeOut.address, _currentSession.currentTimecard.recipientSignature.timeStamp, _currentSession.currentTimecard.pcaSignature.timeStamp, [NSString stringWithFormat:@"%@", totalHoursString]]];
    csvDataString = [csvString dataUsingEncoding:NSUTF8StringEncoding];
    
    UIGraphicsEndPDFContext();
    
    CGPDFDocumentRelease(ourPDF);
    ourPDF = nil;
    [data writeToURL:documentURL atomically:YES];
    return data;
}


- (NSString *)_sanitizeFileNameString:(NSString *)fileName {
    // Mad props to http://stackoverflow.com/questions/1281576/how-to-make-an-nsstring-path-file-name-safe
    
    NSCharacterSet* illegalFileNameCharacters = [NSCharacterSet characterSetWithCharactersInString:@"/\\?%*|\"<>"];
    return [[fileName componentsSeparatedByCharactersInSet:illegalFileNameCharacters] componentsJoinedByString:@""];
}

-(NSString *)pdfNameForService:(NSString *)service
{
    if ([self string:service containsCaseInsensitiveString:@"Personal Care Service"]) {
        return @"PCA";
    } else if ([self string:service containsCaseInsensitiveString:@"Homemaking"]) {
        return @"Homemaking";
    } else if ([self string:service containsCaseInsensitiveString:@"Respite"]) {
        return @"Respite";
    } else if ([self string:service containsCaseInsensitiveString:@"Personal Assistance"]) {
        return @"Personal Assistance";
    } else if ([self string:service containsCaseInsensitiveString:@"Personal Support"]) {
        return @"Personal Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Self-Direction Support"]) {
        return @"Self-Direction Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Treatment and Training"]) {
        return @"Treatment and Training";
    } else if ([self string:service containsCaseInsensitiveString:@"Caregiving Expense"]) {
        return @"Caregiving Expense";
    } else if ([self string:service containsCaseInsensitiveString:@"Consumer Support"]) {
        return @"Consumer Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Environmental Modifications"]) {
        return @"Environmental Modifications";
    }
    return @"Time and Activity 12.2.15";
}

-(NSString *)convertSecondsToString:(NSNumber *)kseconds {
    long hours, minutes, seconds;
    long accumulatedSeconds = [kseconds doubleValue];
    hours = accumulatedSeconds/3600;
    minutes = (accumulatedSeconds % 3600) / 60;
    seconds = (accumulatedSeconds % 3600) % 60;
    return [NSString stringWithFormat:@"%2ld:%02ld:%02ld", hours, minutes, seconds];
}

-(NSString *)dateEndingString {
    //        NSLog(@"pdf dateEnding:%@", currentSession.dateEnding);
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    NSDate *stringDate = [dateformat dateFromString:_currentSession.dateEnding];
    //        NSLog(@"dateEnding:%@", currentSession.dateEnding);
    
    //        NSLog(@"stringDate:%@", stringDate);
    NSDateFormatter *shortFormat = [NSDateFormatter new];
    [shortFormat setDateStyle:NSDateFormatterShortStyle];
    [shortFormat setTimeStyle:NSDateFormatterNoStyle];
    NSString *dateString = [shortFormat stringFromDate:stringDate];
    //        NSLog(@"dateString:%@", dateString);
    if (!stringDate) {
        return _currentSession.dateEnding;
    } else {
        return dateString;
    }
    
}

#pragma mark - Alert View

-(IBAction)addSignature:(UIButton *)sender {
    UIAlertController *proceedAlert = [UIAlertController alertControllerWithTitle:@"Proceed" message:@"Be sure your timecard is completed and correct before proceeding. Once you sign, you will have to send the timecard." preferredStyle:UIAlertControllerStyleAlert];
    [proceedAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [proceedAlert addAction:[UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _currentSession.pdfState = PDFState_ProceedWarningAccepted;
        [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
        [self showFraudWarningFor:sender];
    }]];
    [self.delegate presentAlertViewController:proceedAlert];
    
}

-(IBAction)showFraudWarningFor:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        _fraudView.alpha = 1.0;
        _fraudView.warningTV.text = [self stringForFraudWarningTextView:SignatureIsFor_Recipient];
        if (four) {
            _fraudView.warningTV.font = [UIFont systemFontOfSize:17.0];
        }
        [_fraudView.warningTV sizeToFit];
        _fraudView.warningTV.center = CGPointMake(_fraudView.center.x, _fraudView.center.y - _actionsView.frame.size.height/2);
        [self.delegate setViewTitle:@"Recipient/Responsible Party Signature"];
    }];
}

-(NSString *)stringForFraudWarningTextView:(SignatureIsFor)signatureIsFor {
    NSString *warningTVText;
    if (signatureIsFor == SignatureIsFor_Recipient) {
        if ([self string:_currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
            warningTVText = @"It is a federal crime to provide false information on PCA billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that the services were performed as specified in the PCA care plan. The time and activity document created with this app is transmitted via email. By using this app my health care provider may assume that e-mail communications are acceptable to me and that I am aware of the risks of using unencrypted e-mail.";
        } else {
            warningTVText = @"It is a federal crime to provide false information on billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that the services were performed as specified. The time and activity document created with this app is transmitted via email. By using this app my health care provider may assume that e-mail communications are acceptable to me and that I am aware of the risks of using unencrypted e-mail.";
        }
    }
    return warningTVText;
}

-(IBAction)cancelFW:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        _fraudView.alpha = 0.0;
        [self.delegate setViewTitle:@"Review Timecard"];
    }];
    
}

-(IBAction)acceptFWFor:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        _fraudView.alpha = 0.0;
    }];
    _currentSession.pdfState = PDFState_FraudWarningAccepted;
    [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
    [self showSignatureView];
   
}

#pragma mark - signature methods

-(void)createTimeCardFile {
    if (!_currentSession.currentTimecard.file.idNumber) {
        [NetworkingHelper createFileAndSuccess:^(id responseObject) {
            //            NSLog(@"responseObject:%@", responseObject);
            if (!_currentSession.currentTimecard.file) {
                _currentSession.currentTimecard.file = [File new];
            }
            _currentSession.currentTimecard.file.idNumber = responseObject[@"createdFilesId"];
            [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
            //            NSLog(@"currentSession.currentTimecard.file.idNumber:%@", currentSession.currentTimecard.file.idNumber);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
            NSLog(@"Error creating File:%@", error);
        }];
    }
}

-(void)showSignatureView {
    //x 98.5 y 20 4.925
    [self.delegate setViewTitle:@"Recipient/Responsible Party Signature"];
    
    
    
    if (isIpad) {
        //        sigView.frame = CGRectMake(297.5, 304, 429, 160);
        _signatureView.alpha = 0.0;
        _iPadSignatureView.frame = CGRectMake(100, 150, width - 200, (((width - 200) * 26.5) / 131));
        _iPadSignatureView.alpha = 1.0;
        _iPadSignatureView.delegate = self;
        [_signatureContainerView addSubview:_iPadSignatureView];
        for (UIButton *button in _signatureContainerView.subviews) {
            if ([button isKindOfClass:[UIButton class]]) {
                button.titleLabel.font = [UIFont systemFontOfSize:24];
            }
        }
    } else {
        _signatureView.alpha = 1.0;
        _signatureView.delegate = self;
    }
    
    _signatureContainerView.alpha = 1.0;
    [self.delegate setScrollEnabled:NO];
}

-(void)enableButton {
    [_acceptSignatureButton setTitleColor:[UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_acceptSignatureButton setEnabled:YES];
}

-(IBAction)acceptSignature:(id)sender {
    self.navigationItem.leftBarButtonItem = nil;
    _sigViewSubViewsContainerView.alpha = 0.0;
    _iPadSigViewSubViewsContainerView.alpha = 0.0;
    _currentSession.pdfState = PDFState_SignatureApplied;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDateFormat];
    UIImage *finalImage = [self getFinalImageFromSignatureView];
    
    if (!_currentSession.currentTimecard.recipientSignature) {
        _currentSession.currentTimecard.recipientSignature = [RecipientSignature new];
    }
    _currentSession.currentTimecard.recipientSignature.signatureImageData = UIImagePNGRepresentation(finalImage);

    [NetworkingHelper saveFileWithFileIDNumber:_currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetRecSig", @"fileName":@"timesheetRecSig.png", @"fileData":_currentSession.currentTimecard.recipientSignature.signatureImageData, @"mimeType":@"image/png", @"timesheet":_currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
//            NSLog(@"responseObject for saving recipient signature file:%@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"failed to save recipient signature image:%@", error);
    }];
    _currentSession.currentTimecard.recipientSignature.timeStamp = [NSDate date];
    _currentSession.currentTimecard.recipientSignature.wasSigned = @YES;
    [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
    
    if ([_currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
        [geoCoder reverseGeocodeLocation:[self.delegate location] completionHandler:^(NSArray *placemarks, NSError *error) {
            CLPlacemark *placeMark = placemarks[0];
            _currentSession.currentTimecard.recipientSignature.address = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", [placeMark subThoroughfare], [placeMark thoroughfare], [placeMark locality], [placeMark administrativeArea], [placeMark postalCode]];
            [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
            [self createRecipientSignatureTable];
            //            NSLog(@"params:%@", @{@"timesheet":currentSession.currentTimecard.idNumber, @"recipientSigTime":currentSession.currentTimecard.recipientSignature.timeStamp, @"recipientSigAddress": currentSession.currentTimecard.recipientSignature.address, @"latitude":[NSNumber numberWithDouble:[self.delegate locationManager].location.coordinate.latitude], @"longitude":[NSNumber numberWithDouble:[self.delegate locationManager].location.coordinate.longitude], @"recipientPhotoTime":[NSDate date]});
            
        }];
    } else {
        _currentSession.currentTimecard.recipientSignature.address = @"";
        _currentSession.currentTimecard.recipientSignature.lat = @0.0;
        _currentSession.currentTimecard.recipientSignature.lng = @0.0;
        [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
        [self createRecipientSignatureTable];
    }
    
    [_webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
    [self buttonLogic];
    [self.delegate setViewTitle:@"Review Time Entry"];
    [self clearViews];
    
}

-(void)createRecipientSignatureTable {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDateFormat];
    NSString *rPhotoTime = [format stringFromDate:[NSDate date]];
    [NetworkingHelper createRecipientSignatureTableWithParams:@{@"timesheet":_currentSession.currentTimecard.idNumber, @"recipientSigTime":rPhotoTime, @"recipientSigAddress": _currentSession.currentTimecard.recipientSignature.address, @"latitude":[NSNumber numberWithDouble:[self.delegate location].coordinate.latitude], @"longitude":[NSNumber numberWithDouble:[self.delegate location].coordinate.longitude], @"recipientPhotoTime":rPhotoTime} andSuccess:^(id responseObject) {
        // NSLog(@"success createing recipient signature:%@", responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        NSLog(@"failed to create recipient signature:%@", error);
    }];
}

-(IBAction)clearSignature {
    [self clearViews];
    [self acceptSignature:nil];
}

-(IBAction)deleteSignature {
    [self.delegate setViewTitle:@"Review Time Entry"];
    [_webView loadData:[self generatePDF] MIMEType:@"application/pdf" textEncodingName:@"" baseURL:[NSURL new]];
    [self clearViews];
}

-(void)clearViews {
    _signatureContainerView.alpha = 0.0;
    [self.delegate setScrollEnabled:YES];
}

#pragma mark - UIImage Processing (For Signature Screenshot)

-(UIImage *)getFinalImageFromSignatureView {
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIView *viewToUse = _signatureView;
    if (isIpad) {
        viewToUse = _iPadSignatureView;
    }
    [viewToUse.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGRect rect = viewToUse.frame;
    rect.origin.x = rect.origin.x + 1;
    rect.origin.y = rect.origin.y + 6;
    rect.size.height = rect.size.height - 12;
    rect.size.width = rect.size.width - 2;
    UIImage *secondImage = [self processImage:image];
    return [self cropImage:secondImage rect:rect];
}

-(UIImage *)cropImage:(UIImage *)image rect:(CGRect)cropRect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return img;
}

-(UIImage *)rotateImage:(UIImage *)image {
    return [[UIImage alloc] initWithCGImage:image.CGImage scale:1.0 orientation:UIImageOrientationRight];
}

- (UIImage*) processImage :(UIImage*) inputImage
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 1.0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRef rawImageRef = viewImage.CGImage;
    const CGFloat colorMasking[6] = {255.0f, 255.0f, 255.0f, 255.0f, 255.0f, 255.0f};
    UIGraphicsBeginImageContext(viewImage.size);
    CGImageRef maskedImageRef = CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, viewImage.size.height);
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, viewImage.size.width, viewImage.size.height), maskedImageRef);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
