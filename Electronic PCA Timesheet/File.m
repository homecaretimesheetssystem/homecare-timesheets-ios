//
//  File.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/30/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "File.h"

@implementation File

-(id)copyWithZone:(NSZone *)zone {
    File *copy = [[File allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.csv = _csv;
    copy.pcaSignature = _pcaSignature;
    copy.recipientSignature = _recipientSignature;
    copy.pdf = _pdf;
    return copy;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"timesheetCsvFile"] != [NSNull null]) {
            _csv = dictionary[@"timesheetCsvFile"];
        }else{
            _csv = @"";
        }
        
        
        if ([dictionary objectForKey:@"timesheetPdfFile"] != [NSNull null]) {
            _pdf = dictionary[@"timesheetPdfFile"];
        }else{
            _pdf = @"";
        }

        if ([dictionary objectForKey:@"timesheetRecSig"] != [NSNull null]) {
            _recipientSignature = dictionary[@"timesheetRecSig"];
        }else{
            _recipientSignature = @"";
        }

        if ([dictionary objectForKey:@"timesheetPcaSig"] != [NSNull null]) {
            _pcaSignature = dictionary[@"timesheetPcaSig"];
        }else{
            _pcaSignature = @"";
        }
        
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _csv = [aDecoder decodeObjectForKey:@"csv"];
        _pdf = [aDecoder decodeObjectForKey:@"pdf"];
        _recipientSignature =[aDecoder decodeObjectForKey:@"recipientSignature"];
        _pcaSignature = [aDecoder decodeObjectForKey:@"pcaSignature"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_csv forKey:@"csv"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:_pdf forKey:@"pdf"];
    [aCoder encodeObject:_recipientSignature forKey:@"recipientSignature"];
    [aCoder encodeObject:_pcaSignature forKey:@"pcaSignature"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"idNumber:%@,  csv:%@, pcaSignature:%@, recipientSignature:%@, pdf:%@", _idNumber, _csv, _pcaSignature, _recipientSignature, _pdf];
}

@end
