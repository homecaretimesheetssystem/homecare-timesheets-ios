//
//  PCASignature.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/14/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "PCASignature.h"

@implementation PCASignature

-(id)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        NSDateFormatter *dateFormatter = [PCASignature dateWriter];
        dateFormatter.dateFormat = kDateFormat;
        
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            self.idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary[@"pcaPhotoTime"] isKindOfClass:[NSDate class]]) {
            self.userImageTimestamp = dictionary[@"pcaPhotoTime"];
        } else {
            self.userImageTimestamp = [dateFormatter dateFromString:dictionary[@"pcaPhotoTime"]];
        }
        
        
        
        if ([dictionary objectForKey:@"pcaPhotoTime"] != [NSNull null]) {
            self.userImageTimestamp = dictionary[@"pcaPhotoTime"];
        }else{
            self.userImageTimestamp = [NSDate date];
        }
        
        if ([dictionary objectForKey:@"pcaSigAddress"] != [NSNull null]) {
            self.address = [dictionary[@"pcaSigAddress"] uppercaseString];
        }else{
            self.address = @"";
        }

        
        
        if ([dictionary[@"pcaSigTime"] isKindOfClass:[NSDate class]]) {
            self.timeStamp = dictionary[@"pcaSigTime"];
        } else {
            self.timeStamp = [dateFormatter dateFromString:dictionary[@"pcaSigTime"]];
        }
        
        if (![self.idNumber isEqualToNumber:@0] && self.idNumber) {
            self.wasSigned = @YES;
        } else {
            self.wasSigned = @NO;
        }
        
        
        if ([dictionary objectForKey:@"latitude"] != [NSNull null]) {
            self.lat = dictionary[@"latitude"];
        }else{
            self.lat = [NSNumber numberWithLong:0];
        }
        
        
        if ([dictionary objectForKey:@"longitude"] != [NSNull null]) {
            self.lng = dictionary[@"longitude"];
        }else{
            self.lng = [NSNumber numberWithLong:0];
        }
        
    }
    return self;
}



-(id)copyWithZone:(NSZone *)zone {
    PCASignature *copy = [[PCASignature allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.signatureImageData = self.signatureImageData;
    copy.address = self.address;
    copy.timeStamp = self.timeStamp;
    copy.userImageData = self.userImageData;
    copy.userImageTimestamp = self.userImageTimestamp;
    copy.wasSigned = self.wasSigned;
    copy.lat = _lat;
    copy.lng = _lng;
    copy.urlExtension = self.urlExtension;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        self.idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        self.signatureImageData = [aDecoder decodeObjectForKey:@"signatureImageData"];
        self.address = [aDecoder decodeObjectForKey:@"address"];
        self.timeStamp = [aDecoder decodeObjectForKey:@"timeStamp"];
        self.userImageData = [aDecoder decodeObjectForKey:@"userImageData"];
        self.userImageTimestamp = [aDecoder decodeObjectForKey:@"userImageTimestamp"];
        self.lat = [aDecoder decodeObjectForKey:@"lat"];
        self.lng = [aDecoder decodeObjectForKey:@"lng"];
        self.wasSigned = [aDecoder decodeObjectForKey:@"wasSigned"];
        self.urlExtension = [aDecoder decodeObjectForKey:@"urlExtension"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.idNumber forKey:@"idNumber"];
    [aCoder encodeObject:self.signatureImageData forKey:@"signatureImageData"];
    [aCoder encodeObject:self.address forKey:@"address"];
    [aCoder encodeObject:self.timeStamp forKey:@"timeStamp"];
    [aCoder encodeObject:self.userImageData forKey:@"userImageData"];
    [aCoder encodeObject:self.userImageTimestamp forKey:@"userImageTimestamp"];
    [aCoder encodeObject:self.lat forKey:@"lat"];
    [aCoder encodeObject:self.lng forKey:@"lng"];
    [aCoder encodeObject:self.wasSigned forKey:@"wasSigned"];
    [aCoder encodeObject:self.urlExtension forKey:@"urlExtension"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Signature = address:%@, timeStamp:%@, userImageTimestamp:%@, wasSigned:%@", self.address, self.timeStamp, self.userImageTimestamp, self.wasSigned];
}

+ (NSDateFormatter *)dateWriter {
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateWriter = [dictionary objectForKey:@"SCDateWriter"];
    if (!dateWriter) {
        dateWriter = [[NSDateFormatter alloc] init];
        dateWriter.locale = [NSLocale currentLocale];
        dateWriter.timeZone = [NSTimeZone defaultTimeZone];
        dateWriter.dateStyle = NSDateFormatterMediumStyle;
        [dictionary setObject:dateWriter forKey:@"SCDateWriter"];
    }
    return dateWriter;
}

@end
