//
//  TimeOut.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/27/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "TimeOut.h"

@implementation TimeOut

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        NSDateFormatter *dateFormatter = [TimeOut dateWriter];
        dateFormatter.dateFormat = kDateFormat;

        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }

        _timeOut = dictionary[@"timeOut"];
        if ([dictionary[@"timeOut"] isKindOfClass:[NSDate class]]) {
            _timeOut = dictionary[@"timeOut"];
        } else {
            _timeOut = [dateFormatter dateFromString:dictionary[@"timeOut"]];
        }
        
        if ([dictionary objectForKey:@"timeOutAddress"] != [NSNull null]) {
            _address = [dictionary[@"timeOutAddress"]uppercaseString];
        }else{
            _address = @"";
        }
        
        
        if ([dictionary objectForKey:@"latitude"] != [NSNull null]) {
            self.latitude = dictionary[@"latitude"];
        }else{
            self.latitude = [NSNumber numberWithLong:0];
        }
        
        
        if ([dictionary objectForKey:@"longitude"] != [NSNull null]) {
            self.longitude = dictionary[@"longitude"];
        }else{
            self.longitude = [NSNumber numberWithLong:0];
        }
        
        
        
        _pictureTimestamp = dictionary[@"timeOutPictureTime"];
        if ([dictionary[@"timeOutPictureTime"] isKindOfClass:[NSDate class]]) {
            _pictureTimestamp = dictionary[@"timeOutPictureTime"];
        } else {
            _pictureTimestamp = [dateFormatter dateFromString:dictionary[@"timeOutPictureTime"]];
        }
        
        
        if ([dictionary objectForKey:@"totalHours"] != [NSNull null]) {
            _totalHours = dictionary[@"totalHours"];
        }else{
            _totalHours = [NSNumber numberWithLong:0];
        }
        
        
        if ([dictionary objectForKey:@"billableHours"] != [NSNull null]) {
            _billableHours = dictionary[@"billableHours"];
        }else{
            _billableHours = [NSNumber numberWithLong:0];
        }
        
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    TimeOut *copy = [[TimeOut allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.timeOut = _timeOut;
    copy.address = _address;
    copy.totalHours = _totalHours;
    copy.latitude = _latitude;
    copy.longitude = _longitude;
    copy.pictureTimestamp = _pictureTimestamp;
    copy.billableHours = _billableHours;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _timeOut = [aDecoder decodeObjectForKey:@"timeOut"];
        _address = [aDecoder decodeObjectForKey:@"address"];
        _totalHours =[aDecoder decodeObjectForKey:@"totalHours"];
        _latitude = [aDecoder decodeObjectForKey:@"latitude"];
        _longitude = [aDecoder decodeObjectForKey:@"longitude"];
        _pictureTimestamp = [aDecoder decodeObjectForKey:@"pictureTimestamp"];
        _billableHours = [aDecoder decodeObjectForKey:@"billableHours"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_timeOut forKey:@"timeOut"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:_address forKey:@"address"];
    [aCoder encodeObject:_totalHours forKey:@"totalHours"];
    [aCoder encodeObject:_latitude forKey:@"latitude"];
    [aCoder encodeObject:_longitude forKey:@"longitude"];
    [aCoder encodeObject:_pictureTimestamp forKey:@"pictureTimestamp"];
    [aCoder encodeObject:_billableHours forKey:@"billableHours"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"ID: %@, timeOut:%@, address:%@, totalHours:%@, latitude:%@, longitude:%@, pictureTimestamp:%@, billableHours:%@", _idNumber, _timeOut, _address, _totalHours, _latitude, _longitude, _pictureTimestamp, _billableHours];
}

+ (NSDateFormatter *)dateWriter {
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateWriter = [dictionary objectForKey:@"SCDateWriter"];
    if (!dateWriter) {
        dateWriter = [[NSDateFormatter alloc] init];
        dateWriter.locale = [NSLocale currentLocale];
        dateWriter.timeZone = [NSTimeZone defaultTimeZone];
        dateWriter.dateStyle = NSDateFormatterMediumStyle;
        [dictionary setObject:dateWriter forKey:@"SCDateWriter"];
    }
    return dateWriter;
}

@end
