//
//  PCA.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/22/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface PCA : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSNumber *idNumber;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *umpi;
@property (nonatomic, strong) NSDictionary *randomSessionData;
@property (nonatomic, strong) NSNumber *companyAssignedID;
@property (nonatomic, strong) NSArray *servicesArray;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
