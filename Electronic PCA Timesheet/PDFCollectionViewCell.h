//
//  PDFCollectionViewCell.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/28/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PDFCollectionViewCellDelegate <NSObject>

-(void)back;
-(void)finishSessionAndShouldRemoveSession:(BOOL)removeSession session:(Session *)session;
-(void)presentAlertViewController:(UIAlertController *)alert;
-(CLLocation *)location;
-(void)setViewTitle:(NSString *)title;
-(void)setScrollEnabled:(BOOL)enabled;

@end


#import "SignatureView.h"
#import "FraudView.h"
#import <CoreLocation/CoreLocation.h>

@interface PDFCollectionViewCell : UICollectionViewCell <sigViewDelegate, CLLocationManagerDelegate, UITextFieldDelegate> {
    bool isIpad; bool four; int width; int height;
}

@property (nonatomic, strong) UINavigationItem *navigationItem;
@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic) id<PDFCollectionViewCellDelegate>delegate;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIButton *signButton;
@property (nonatomic, weak) IBOutlet UIButton *saveAndExitButton;
@property (nonatomic, weak) IBOutlet UIButton *sendPDFButton;
@property (nonatomic, weak) IBOutlet UIView *actionsView;
@property (nonatomic, weak) IBOutlet FraudView *fraudView;
@property (nonatomic, weak) IBOutlet UIView *signatureContainerView;
@property (nonatomic, weak) IBOutlet SignatureView *signatureView;
@property (nonatomic, weak) IBOutlet SignatureView *iPadSignatureView;
@property (nonatomic, weak) IBOutlet UIButton *acceptSignatureButton;
@property (nonatomic, weak) IBOutlet UIView *sigViewSubViewsContainerView;
@property (nonatomic, weak) IBOutlet UIView *iPadSigViewSubViewsContainerView;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) Session *currentSession;

-(void)sessionIsSet:(Session*)session;

@end
