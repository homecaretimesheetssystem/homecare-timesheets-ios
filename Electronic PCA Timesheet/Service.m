//
//  Service.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/25/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "Service.h"

@implementation Service

-(instancetype)initWithDictionary:(NSDictionary *)dict {
    if (self == [super init]) {
        
        if ([dict objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dict[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        
        if ([dict objectForKey:@"service_name"] != [NSNull null]) {
            _serviceName = [dict[@"service_name"] uppercaseString];

        }else{
            _serviceName = @"";
        }
        
        if ([dict objectForKey:@"service_number"] != [NSNull null]) {
            _serviceNumberdescr = [dict[@"service_number"] unsignedIntegerValue];
        }else{
            _serviceNumberdescr = 0;
        }
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    Service *copy = [[Service allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.serviceName = _serviceName;
    copy.serviceNumberdescr = _serviceNumberdescr;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _serviceName = [aDecoder decodeObjectForKey:@"serviceName"];
        _serviceNumberdescr = [aDecoder decodeIntegerForKey:@"service_number"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_serviceName forKey:@"serviceName"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeInteger:_serviceNumberdescr forKey:@"service_number"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Service: idNumber:%@, serviceName:%@, serviceNumberDescr:%lu", _idNumber, _serviceName, (unsigned long)_serviceNumberdescr];
}

@end
