//
//  CareLocation.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/27/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "CareLocation.h"

@implementation CareLocation

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"location"] != [NSNull null]) {
            _sharedCareLocation = [dictionary[@"location"] uppercaseString];
        }else{
            _sharedCareLocation = @"";
        }
        
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    CareLocation *copy = [[CareLocation allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.sharedCareLocation = _sharedCareLocation;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _sharedCareLocation = [aDecoder decodeObjectForKey:@"sharedCareLocation"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_sharedCareLocation forKey:@"sharedCareLocation"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"ID:%@, SharedCareLocation:%@", _idNumber, _sharedCareLocation];
}

@end
