//
//  Recipient.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/22/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "Recipient.h"

@implementation Recipient

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"first_name"] != [NSNull null]) {
            _firstName = [dictionary[@"first_name"] uppercaseString];

        }else{
            _firstName = @"";
        }
        
        
        if ([dictionary objectForKey:@"last_name"] != [NSNull null]) {
            _lastName = [dictionary[@"last_name"] uppercaseString];

        }else{
            _lastName = @"";
        }
        
        if ([dictionary objectForKey:@"ma_number"] != [NSNull null]) {
            _maNumber = [dictionary[@"ma_number"] uppercaseString];
        }else{
            _maNumber = @"";
        }
        if (dictionary[@"company_assigned_id"] && ![dictionary[@"company_assigned_id"] isEqual:[NSNull null]]) {
            _companyAssignedID = dictionary[@"company_assigned_id"];
        }
        
        if ([dictionary objectForKey:@"skip_gps"] != [NSNull null]) {
            BOOL skipGPS = [dictionary[@"skip_gps"] boolValue];
            _shouldRecordLocation = [NSNumber numberWithBool:!skipGPS];

        }else{
            _shouldRecordLocation = [NSNumber numberWithLong:1];
        }
        
        if (dictionary[@"careOptions"] != [NSNull null]) {
            NSMutableArray *allCareOptionsArray = [NSMutableArray new];
            for (NSDictionary *careOptionsDict in dictionary[@"careOptions"]) {
                CareOption *co = [[CareOption alloc] initWithDictionary:careOptionsDict];
                [allCareOptionsArray addObject:co];
            }
            _CareOptionsArray = allCareOptionsArray.copy;
        }
        
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    Recipient *copy = [[Recipient allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.firstName = _firstName;
    copy.lastName = _lastName;
    copy.maNumber = _maNumber;
    copy.companyAssignedID = _companyAssignedID;
    copy.shouldRecordLocation = _shouldRecordLocation;
    copy.CareOptionsArray = _CareOptionsArray;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _firstName = [aDecoder decodeObjectForKey:@"firstName"];
        _lastName = [aDecoder decodeObjectForKey:@"lastName"];
        _maNumber =[aDecoder decodeObjectForKey:@"maNumber"];
        _companyAssignedID = [aDecoder decodeObjectForKey:@"companyAssignedID"];
        _shouldRecordLocation = [aDecoder decodeObjectForKey:@"shouldRecordLocation"];
        _CareOptionsArray = [aDecoder decodeObjectForKey:@"careOptions"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_firstName forKey:@"firstName"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:_lastName forKey:@"lastName"];
    [aCoder encodeObject:_maNumber forKey:@"maNumber"];
    [aCoder encodeObject:_companyAssignedID forKey:@"companyAssignedID"];
    [aCoder encodeObject:_shouldRecordLocation forKey:@"shouldRecordLocation"];
    [aCoder encodeObject:_CareOptionsArray forKey:@"careOptions"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Recipient - idNumber:%@, firstName:%@, lastName:%@, maNumber:%@, companyAssignedID:%@, shouldRecordLocation:%@, OnDemandCareOptions:%@", _idNumber, _firstName, _lastName, _maNumber, _companyAssignedID, _shouldRecordLocation, _CareOptionsArray];
}

@end
