//
//  TimeIn.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/27/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "TimeIn.h"

@implementation TimeIn

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        NSDateFormatter *formatter = [TimeIn dateWriter];
        formatter.dateFormat = kDateFormat;
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary[@"timeIn"] isKindOfClass:[NSDate class]]) {
            _timeIn = dictionary[@"timeIn"];
        } else {
            _timeIn = [formatter dateFromString:dictionary[@"timeIn"]];
        }
        
        if ([dictionary objectForKey:@"timeInAddress"] != [NSNull null]) {
            _address = [dictionary[@"timeInAddress"]uppercaseString];
        }else{
            _address = @"";
        }

        
        if ([dictionary objectForKey:@"estimatedShiftLength"] != [NSNull null]) {
            _estimatedShiftLenght = dictionary[@"estimatedShiftLength"];
        }else{
            _estimatedShiftLenght = [NSNumber numberWithLong:0];
        }

        if ([dictionary objectForKey:@"latitude"] != [NSNull null]) {
            self.latitude = dictionary[@"latitude"];
        }else{
            self.latitude = [NSNumber numberWithLong:0];
        }
        
        
        if ([dictionary objectForKey:@"longitude"] != [NSNull null]) {
            self.longitude = dictionary[@"longitude"];
        }else{
            self.longitude = [NSNumber numberWithLong:0];
        }
        
        
        if ([dictionary[@"timeInPictureTime"] isKindOfClass:[NSDate class]]) {
            _pictureTimestamp = dictionary[@"timeInPictureTime"];
        } else {
            _pictureTimestamp = [formatter dateFromString:dictionary[@"timeInPictureTime"]];
        }
        
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    TimeIn *copy = [[TimeIn allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.timeIn = _timeIn;
    copy.address = _address;
    copy.estimatedShiftLenght = _estimatedShiftLenght;
    copy.latitude = _latitude;
    copy.longitude = _longitude;
    copy.pictureTimestamp = _pictureTimestamp;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _timeIn = [aDecoder decodeObjectForKey:@"timeIn"];
        _address = [aDecoder decodeObjectForKey:@"address"];
        _estimatedShiftLenght =[aDecoder decodeObjectForKey:@"estimatedShiftLenght"];
        _latitude = [aDecoder decodeObjectForKey:@"latitude"];
        _longitude = [aDecoder decodeObjectForKey:@"longitude"];
        _pictureTimestamp = [aDecoder decodeObjectForKey:@"pictureTimestamp"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_timeIn forKey:@"timeIn"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:_address forKey:@"address"];
    [aCoder encodeObject:_estimatedShiftLenght forKey:@"estimatedShiftLenght"];
    [aCoder encodeObject:_latitude forKey:@"latitude"];
    [aCoder encodeObject:_longitude forKey:@"longitude"];
    [aCoder encodeObject:_pictureTimestamp forKey:@"pictureTimestamp"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"ID: %@, timeIn:%@, address:%@, estimatedShiftLenght:%@, latitude:%@, longitude:%@, pictureTimestamp:%@", _idNumber, _timeIn, _address, _estimatedShiftLenght, _latitude, _longitude, _pictureTimestamp];
}

+ (NSDateFormatter *)dateWriter {
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateWriter = [dictionary objectForKey:@"SCDateWriter"];
    if (!dateWriter) {
        dateWriter = [[NSDateFormatter alloc] init];
        dateWriter.locale = [NSLocale currentLocale];
        dateWriter.timeZone = [NSTimeZone defaultTimeZone];
        dateWriter.dateStyle = NSDateFormatterMediumStyle;
        [dictionary setObject:dateWriter forKey:@"SCDateWriter"];
    }
    return dateWriter;
}

@end
