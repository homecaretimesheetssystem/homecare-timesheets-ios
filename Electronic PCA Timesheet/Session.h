//
//  Session.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 2/17/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Timecard.h"
#import "Service.h"
#import "PCA.h"
#import "Recipient.h"
#import "TimeIn.h"
#import "TimeOut.h"
#import "CareOption.h"
#import "CareLocation.h"
#import "Ratio.h"
#import "SessionFile.h"
#import "User.h"

@class Timecard, Agency, Service, User, TimeIn, TimeOut, Recipient, CareOption, Ratio, CareLocation, SessionFile;

typedef NS_ENUM(NSUInteger, PDFState) {
    PDFState_Default,
    PDFState_ProceedWarningAccepted,
    PDFState_FraudWarningAccepted,
    PDFState_SignatureApplied,
};




@interface Session : NSObject <NSCopying, NSCoding>


//Variables
@property (strong, nonatomic) Timecard *currentTimecard;
@property (strong, nonatomic) Service *service;
@property (strong, nonatomic) Ratio *ratio;
@property (strong, nonatomic) CareLocation *careLocation;
@property (strong, nonatomic) TimeIn *timeIn;
@property (strong, nonatomic) TimeOut *timeOut;
@property (strong, nonatomic) SessionFile *sessionFile;
@property (strong, nonatomic) PCA *pca;
@property (strong, nonatomic) Recipient *recipient;
@property (strong, nonatomic) NSMutableArray *recipientTimesheetsArray;
@property (strong, nonatomic) NSMutableArray *timesheetIDsArray;



//Other Session Data
@property (strong, nonatomic) NSNumber *idNumber;
@property (strong, nonatomic) NSNumber *continueTimesheetNumber;
@property (strong, nonatomic) NSNumber *currentTimesheetNumber;
@property (strong, nonatomic) NSNumber *currentTimesheetID;
@property (strong, nonatomic) NSNumber *verificationID;
@property (strong, nonatomic) NSNumber *flagForFurtherInvestigation;
@property (strong, nonatomic) NSNumber *verificationIsRequiredForWorkerAndRecipient;
@property (strong, nonatomic) NSString *fileURL;
@property (strong, nonatomic) NSString *dateEnding;
@property (strong, nonatomic) NSString *datesInHospital;
@property (strong, nonatomic) NSMutableArray *onDemandCareOptionsArray;

@property (strong, nonatomic) NSMutableArray *arrayOfSessions;
@property (nonatomic) PDFState pdfState;

@property (nonatomic, strong) NSMutableArray *selectedRecipients;

-(instancetype)initWithDictionary:(NSDictionary *)dict andIndex:(long)index andUserType:(UserType)userType;

-(void)resetLocalValues;


@end
