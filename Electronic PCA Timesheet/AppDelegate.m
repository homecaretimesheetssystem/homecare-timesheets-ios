//
//  AppDelegate.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 10/29/13.
//  Copyright (c) 2013 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "NetworkingHelper.h"
#import "DeepLinkViewController.h"

@implementation AppDelegate
@synthesize navCont;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [Fabric with:@[[Crashlytics class]]];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.appLocationManager = [CLLocationManager new];

    
    
    LoginViewController *vc = [LoginViewController new];
    navCont = [[UINavigationController alloc] initWithRootViewController:vc];
    navCont.navigationBar.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    self.window.rootViewController = navCont;
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  //  [self saveTimecard];
//    NSLog(@"locRequest:%d", [[NSUserDefaults standardUserDefaults] boolForKey:@"locRequest"]);
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"locRequest"] != YES) {
        [self.navCont popToRootViewControllerAnimated:NO];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"locRequest"] == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"askLocationPermission" object:nil];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return _shouldRotate ? UIInterfaceOrientationMaskAll : UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? UIInterfaceOrientationMaskLandscape : UIInterfaceOrientationMaskPortrait;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    DeepLinkViewController *vc = [DeepLinkViewController new];
    navCont = [[UINavigationController alloc] initWithRootViewController:vc];
    navCont.navigationBar.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    self.window.rootViewController = navCont;
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    NSString *tokenString;
    NSString *firstSessionIDString;
    
    NSString *str1 = [url query];
    NSRange range = [str1 rangeOfString:@"&sessionID="];
    if (range.location != NSNotFound) {
        tokenString = [str1 substringToIndex:range.location];
    } else {
        
    }
    
    NSString *str2= [url query];
    NSRange range2 = [str2 rangeOfString:@"&sessionID=" options: NSBackwardsSearch];
    firstSessionIDString = [str2 substringFromIndex:(range2.location+1)];
    
    NSString *sessionIDString = [firstSessionIDString stringByReplacingOccurrencesOfString:@"sessionID=" withString:@""];
    NSNumber *sessionIDNumber = @([sessionIDString intValue]);
    
    [NetworkingHelper setNetworkingHelperOAuthToken:tokenString];
    [NetworkingHelper getSessionDataWithSuccess:^(id responseObject) {
        User *user = [[User alloc] initWithDictionary:responseObject];
                for (Session *session in user.sessions) {
                    
                    if ([sessionIDNumber isEqualToNumber:session.idNumber]) {
                        user.currentSession = session;
                    }

                }
        
        for (int i = 0; i < user.recipientsArray.count; i++) {
            if ([user.currentSession.currentTimecard.recipient.idNumber isEqualToNumber:((Recipient *)user.recipientsArray[i]).idNumber]) {
                    user.currentSession.currentTimecard.recipient.CareOptionsArray = [((Recipient *)user.recipientsArray[i]).CareOptionsArray mutableCopy];
                
            }
        }
        
        [user save];
        [NSNotificationCenter.defaultCenter postNotificationName:@"userLoadedAfterDeepLink" object:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failed to login because :%@", error);
        LoginViewController *vc = [LoginViewController new];
        navCont = [[UINavigationController alloc] initWithRootViewController:vc];
        navCont.navigationBar.tintColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
        self.window.rootViewController = navCont;
        self.window.backgroundColor = [UIColor blackColor];
        [self.window makeKeyAndVisible];
        
    }];
    return YES;
}

@end
