//
//  SignatureView.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/8/14.
//  Copyright (c) 2014 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "SignatureView.h"

@implementation SignatureView {
    UIBezierPath *bezPath;
    UIImage *incrementalImage;
    CGPoint pts[5];
    uint ctr;
}
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setMultipleTouchEnabled:NO];
        [self setBackgroundColor:[UIColor whiteColor]];
        bezPath = [UIBezierPath bezierPath];
        [bezPath setLineWidth:2.0];
    }
    return self;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    [self setMultipleTouchEnabled:NO];
    [self setBackgroundColor:[UIColor whiteColor]];
    bezPath = [UIBezierPath bezierPath];
    [bezPath setLineWidth:2.0];
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [incrementalImage drawInRect:rect];
    [bezPath stroke];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    ctr = 0;
    UITouch *touch = [touches anyObject];
    pts[0] = [touch locationInView:self];
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    ctr++;
    pts[ctr] = p;
    if (ctr == 4) {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0);
        [bezPath moveToPoint:pts[0]];
        [bezPath addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]];
        [self setNeedsDisplay];
        pts[0] = pts[3];
        pts[1] = pts[4];
        ctr = 1;
    }
    [self.delegate enableButton];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self drawBitmap];
    [self setNeedsDisplay];
    [bezPath removeAllPoints];
    ctr = 0;
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesEnded:touches withEvent:event];
}

-(void)drawBitmap {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0.0);
    if (!incrementalImage) {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        [[UIColor whiteColor] setFill];
        [rectpath fill];
    }
    [incrementalImage drawAtPoint:CGPointZero];
    [[UIColor blackColor] setStroke];
    [bezPath stroke];
    incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

@end
