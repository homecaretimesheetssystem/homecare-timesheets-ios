//
//  LoginViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/12/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "Session.h"
#import "User.h"

@class Session;

@interface LoginViewController : UIViewController <UITextFieldDelegate, CLLocationManagerDelegate> {
    bool isIpad; bool four; float width; float height;
    UITextField *usernameTextField; UITextField *passwordTextField;
    UIButton *nextScreenButton;
}

@property (strong, nonatomic) Session *currentSession;

@end
