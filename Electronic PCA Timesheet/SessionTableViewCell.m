//
//  SessionTableViewCell.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 5/12/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "SessionTableViewCell.h"

@implementation SessionTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
        self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
        if (!self.leftImageView) {
            [self formatImageView];
        }
        if (!self.mainLabel) {
            [self formatMainLabel];
        }
        if (!self.subLabel) {
            [self formatSubLabel];
        }
    }
    
    
    return self;
    // Initialization code
}

-(void)formatMainLabel {
    float fontSize = 16.0;
    if (CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960))) {
        fontSize = 14.0;
    }
    self.mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.leftImageView.frame.origin.x + self.leftImageView.frame.size.width + 8, 0, self.contentView.frame.size.width - (self.leftImageView.frame.origin.x + self.leftImageView.frame.size.width + 16), self.leftImageView.frame.size.height - 16)];
    self.mainLabel.center = CGPointMake(self.mainLabel.center.x, self.imageView.center.y + 4);
    self.mainLabel.font = [UIFont systemFontOfSize:fontSize weight:UIFontWeightMedium];
    self.mainLabel.textColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    self.mainLabel.adjustsFontSizeToFitWidth = YES;
    self.mainLabel.minimumScaleFactor = 0.6;
    self.mainLabel.text = @"Test";
    self.mainLabel.center = CGPointMake(self.mainLabel.center.x, self.center.y - 5);
    [self.contentView addSubview:self.mainLabel];
}

-(void)formatSubLabel {
    float fontSize = 11.0;
    if (CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960))) {
        fontSize = 10.0;
    }
    self.subLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.mainLabel.frame.origin.x, 0, self.mainLabel.frame.size.width, self.leftImageView.frame.size.height/2)];
    self.subLabel.center = CGPointMake(self.mainLabel.center.x, self.imageView.center.y + 4);
    self.subLabel.font = [UIFont systemFontOfSize:fontSize weight:UIFontWeightMedium];
    self.subLabel.textColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    self.subLabel.adjustsFontSizeToFitWidth = YES;
    self.subLabel.minimumScaleFactor = 0.8;
    self.subLabel.text = @"Test";
    self.subLabel.center = CGPointMake(self.subLabel.center.x, self.center.y + 12);
    [self.contentView addSubview:self.subLabel];
}


-(void)formatImageView {
    self.leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(4, 4, 42, 42)];
    self.leftImageView.clipsToBounds = YES;
    self.leftImageView.backgroundColor = [UIColor whiteColor];
    self.leftImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.leftImageView.image = [UIImage imageNamed:@"PDFPreview"];
    
    [self.contentView addSubview:self.leftImageView];
    NSLog(@"imageViewAdded:%@", [UIImage imageNamed:@"PDFPreview"]);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
