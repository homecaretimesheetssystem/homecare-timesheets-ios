//
//  CareOption.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 4/2/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "CareOption.h"

@implementation CareOption

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        
        if ([dictionary objectForKey:@"care_option"] != [NSNull null]) {
            _name = [dictionary[@"care_option"] uppercaseString];
        }else{
            _name = @"";
        }
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"is_iadl"] != [NSNull null]) {
            _isIADL = [dictionary[@"is_iadl"] boolValue];
        }else{
            _isIADL = NO;
        }
        
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    CareOption *copy = [[CareOption allocWithZone:zone] init];
    copy.name = _name;
    copy.idNumber = _idNumber;
    copy.isIADL = _isIADL;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _name = [aDecoder decodeObjectForKey:@"name"];
        _isIADL = [aDecoder decodeBoolForKey:@"isIADL"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeBool:_isIADL forKey:@"isIADL"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"\r    Name:%@\r    id:%@\r    isIADL:%d\r", _name, _idNumber, _isIADL];
}

@end
