//
//  LoadingView.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/26/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

-(instancetype)initWithFrame:(CGRect)frame andText:(NSString *)text {
    if ((self = [super initWithFrame:CGRectMake(0, 0, 150, 100)])) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
        self.layer.cornerRadius = 10;
        self.layer.masksToBounds = YES;
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.activityIndicator.frame = CGRectMake(56.5, 20, 37, 37);
        [self.activityIndicator startAnimating];
        
        [self addSubview:self.activityIndicator];
        
        CGFloat labelY = self.activityIndicator.frame.size.height + self.activityIndicator.frame.origin.y + 10;
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, labelY, self.frame.size.width, 24)];
        self.label.font = [UIFont systemFontOfSize:12];
        self.label.textColor = [UIColor whiteColor];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.text = text;
        [self addSubview:self.label];
        
        
    }
    
    return self;
}

+(LoadingView *)addLoadingViewWithText:(NSString *)text {
    LoadingView *lv = [[LoadingView alloc] initWithFrame:CGRectZero andText:text];
    lv.center = CGPointMake([[UIApplication sharedApplication] keyWindow].center.x, [[UIApplication sharedApplication] keyWindow].center.y);
    for (LoadingView *lv in [[UIApplication sharedApplication] keyWindow].subviews) {
        if ([lv isKindOfClass:[LoadingView class]]) {
            [lv removeFromSuperview];
        }
    }
    [[[UIApplication sharedApplication] keyWindow] addSubview:lv];
    return lv;
}

-(void)showActivityIndicator {
    [self setAlpha:1];
}

-(void)hideActivityIndicator {
    [self setAlpha:0];
}

@end
