//
//  PCA.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 1/22/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "PCA.h"

@implementation PCA

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self == [super init]) {
        
        if ([dictionary objectForKey:@"id"] != [NSNull null]) {
            _idNumber = dictionary[@"id"];
        }else{
            _idNumber = [NSNumber numberWithLong:0];
        }
        
        if ([dictionary objectForKey:@"first_name"] != [NSNull null]) {
            _firstName = [dictionary[@"first_name"] uppercaseString];
            
        }else{
            _firstName = @"";
        }
        
        
        if ([dictionary objectForKey:@"last_name"] != [NSNull null]) {
            _lastName = [dictionary[@"last_name"] uppercaseString];
            
        }else{
            _lastName = @"";
        }
        
        if ([dictionary objectForKey:@"umpi"] != [NSNull null]) {
            _umpi = [dictionary[@"umpi"] uppercaseString];
        }else{
            _umpi = @"";
        }
        
        if ([dictionary objectForKey:@"randomSessionData"] != [NSNull null]) {
            _randomSessionData = dictionary[@"randomSessionData"];

        }else{
            _randomSessionData = [NSDictionary new];
        }
        
        if (dictionary[@"company_assigned_id"] && ![dictionary[@"company_assigned_id"] isEqual:[NSNull null]]) {
            _companyAssignedID = dictionary[@"company_assigned_id"];
        }
        
        
        if ([dictionary objectForKey:@"services"] != [NSNull null]) {
            NSArray *servArr = [dictionary valueForKey:@"services"];
            NSMutableArray *mutArr = [NSMutableArray new];
            for (NSDictionary *dict in servArr) {
                Service *service = [[Service alloc] initWithDictionary:dict];
                [mutArr addObject:service];
            }
            _servicesArray = [mutArr mutableCopy];
        }else{
            _servicesArray = [NSMutableArray new];
        }

    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    PCA *copy = [[PCA allocWithZone:zone] init];
    copy.idNumber = _idNumber;
    copy.firstName = _firstName;
    copy.lastName = _lastName;
    copy.umpi = _umpi;
    copy.randomSessionData = _randomSessionData;
    copy.companyAssignedID = _companyAssignedID;
    copy.servicesArray = _servicesArray;
    return copy;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        _idNumber = [aDecoder decodeObjectForKey:@"idNumber"];
        _firstName = [aDecoder decodeObjectForKey:@"firstName"];
        _lastName = [aDecoder decodeObjectForKey:@"lastName"];
        _umpi =[aDecoder decodeObjectForKey:@"umpi"];
        _randomSessionData = [aDecoder decodeObjectForKey:@"randomSessionData"];
        _companyAssignedID = [aDecoder decodeObjectForKey:@"companyAssignedID"];
        _servicesArray = [NSKeyedUnarchiver unarchiveObjectWithData:[aDecoder decodeObjectForKey:@"services"]];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_firstName forKey:@"firstName"];
    [aCoder encodeObject:_idNumber forKey:@"idNumber"];
    [aCoder encodeObject:_lastName forKey:@"lastName"];
    [aCoder encodeObject:_umpi forKey:@"umpi"];
    [aCoder encodeObject:_randomSessionData forKey:@"randomSessionData"];
    [aCoder encodeObject:_companyAssignedID forKey:@"companyAssignedID"];
    [aCoder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:_servicesArray] forKey:@"services"];
}

-(NSString *)description {
    return [NSString stringWithFormat:@"PCA {\r    idNumber:%@,\r    firstName:%@,\r    lastName:%@,\r    umpi:%@,\r    companyAssignedID:%@ randomSessionData:%@\r    servicesArray:%@\r }", _idNumber, _firstName, _lastName, _umpi, _companyAssignedID, _randomSessionData, _servicesArray];
}
@end
