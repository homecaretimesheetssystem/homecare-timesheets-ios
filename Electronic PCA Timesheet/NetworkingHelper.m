//
//  NetworkingHelper.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/26/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "NetworkingHelper.h"
#import "Session.h"
#import <AFOAuth2Manager/AFOAuth2Manager.h>
#import <AFHTTPRequestSerializer+OAuth2.h>


@implementation NetworkingHelper

+(AFHTTPSessionManager *)sharedManager {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *reqSerializer = [AFJSONRequestSerializer serializer];
    [reqSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [reqSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"Authorization"];
    AFOAuthCredential *credential = [NetworkingHelper credential];
    [reqSerializer setAuthorizationHeaderFieldWithCredential:credential];
    manager.requestSerializer = reqSerializer;
    return manager;
}

+(AFOAuthCredential *)credential {
    AFOAuthCredential *cred = [[AFOAuthCredential alloc] initWithOAuthToken:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] tokenType:@"Bearer"];
    long dateValue = -[[[NSUserDefaults standardUserDefaults] objectForKey:@"credExpiration"] longValue];
    [cred setExpiration:[NSDate dateWithTimeIntervalSinceNow:dateValue]];
    return cred;
}

//+(NSString *)urlForGettingToken {
//   return [NSString stringWithFormat:@"http://homecare-timesheet.herokuapp.com/oauth/v1/token?client_id=%@&client_secret=%@&grant_type=client_credentials", [NetworkingHelper clientID], [NetworkingHelper clientSecret]];
  
//}

//+(void)loginWithParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
//    NSString *url = [NSString stringWithFormat:@"%@/oauth/v1/token?client_id=%@&client_secret=%@&grant_type=password&username=%@&password=%@", kLoginURL, [NetworkingHelper clientID], [NetworkingHelper clientSecret], params[@"username"], params[@"password"]];
//    NSLog(@"url:%@", url);
//    [[self sharedManager] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        success(responseObject);
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        failure(task, error);
//    }];
//    
//}

+(void)loginWithParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/login", kURL];
    NSLog(@"params %@", params);
    [[self sharedManager] POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"response object %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error error %@", error);
        failure(task, error);
    }];

}
+(void)resetPasswordWithParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/forgot", kURL];
    [[self sharedManager] POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        failure(task, error);
    }];
    
}

+(void)checkVerificationForCamera:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/pca/%@/verify/%@", kURL, params[@"pcaid"], params[@"recipientid"]];
    [[self sharedManager] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)setVerificationForCameraWithID:(NSNumber *)verifID andParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/verify/%@", kURL, verifID];
    [[self sharedManager] PATCH:url parameters:@{@"homecare_homecarebundle_verification":params} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)getSessionDataWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/data", kURL] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}


+(void)createNewTimesheetTableWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/create/timesheet", kURL];
    NSDictionary *parameters = @{@"homecare_homecarebundle_timesheet":@""};
    [[self sharedManager] POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)saveTimesheetDataWithTimesheetID:(NSNumber *)timesheetID andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/timesheet/%@", kURL, timesheetID];
    NSDictionary *parameters = @{@"homecare_homecarebundle_timesheet":params};
    [[self sharedManager] PATCH:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)createSessionDataTableWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/create/sessiondata", kURL];
    [[self sharedManager] POST:url parameters:@{@"homecare_homecarebundle_sessiondata":@""} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)saveSessionDataWithSessionNumber:(NSNumber *)sessionNumber andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/sessiondata/%@", kURL, sessionNumber];
    NSDictionary *parameters = @{@"homecare_homecarebundle_sessiondata":params};
    [[self sharedManager] PATCH:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)saveTimesheetNumberWithTimesheetNumber:(NSNumber *)timesheetNumber andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/timesheet/%@", kURL, timesheetNumber];
    NSDictionary *parameters = @{@"homecare_homecarebundle_timesheet":params};
    [[self sharedManager] PATCH:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)createSessionFileAndSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/create/session/files", kURL];
    [[self sharedManager] POST:url parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        nil;
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)saveSessionFileWithFileIDNumber:(NSNumber *)fileID andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/session/%@/files", kURL, fileID];
    [[self sharedManager] POST:url parameters:@{@"sessionDataId":params[@"sessionDataId"]} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:params[@"fileData"] name:params[@"file"] fileName:params[@"fileName"] mimeType:params[@"mimeType"]];
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)createFileAndSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/create/files", kURL];
    [[self sharedManager] POST:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)saveFileWithFileIDNumber:(NSNumber *)fileID andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/files/%@", kURL, fileID];
    [[self sharedManager] POST:url parameters:@{@"timesheet":params[@"timesheet"]} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {[formData appendPartWithFileData:params[@"fileData"] name:params[@"file"] fileName:params[@"fileName"] mimeType:params[@"mimeType"]];
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)saveVerificationPhotoWithVerificationID:(NSNumber *)verificationID andParams:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/verifcation/%@/photo", kURL, verificationID];
    [[self sharedManager] POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:params[@"fileData"] name:params[@"file"] fileName:params[@"fileName"] mimeType:params[@"mimeType"]];
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)createTimeInTableWithSuccess:(void(^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/create/time/in", kURL];
    [[self sharedManager] POST:url parameters:@{@"homecare_homecarebundle_timein":@""} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)createTimeOutTableWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/create/time/out", kURL];
    [[self sharedManager] POST:url parameters:@{@"homecare_homecarebundle_timeout":@""} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)saveTimeInWithTimeInIdNumber:(NSNumber *)timeInIdNumber andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *))failure {
    NSString *url = [NSString stringWithFormat:@"%@/timein/%@", kURL, timeInIdNumber];
    [[self sharedManager] PATCH:url parameters:@{@"homecare_homecarebundle_timein":params} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)createAndSaveVerificationWithParams:(NSDictionary *)params andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/create/verification", kURL];
    [[self sharedManager] POST:url parameters:@{@"homecare_homecarebundle_verification":params} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)saveTimeOutWithTimeOutIdNumber:(NSNumber *)timeOutIdNumber andParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *))failure {
    NSString *url = [NSString stringWithFormat:@"%@/timeout/%@", kURL, timeOutIdNumber];
    [[self sharedManager] PATCH:url parameters:@{@"homecare_homecarebundle_timeout":params} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)addCareOptionsToTimesheetWithParams:(NSDictionary *)params andsuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/careoptions", kURL];
    [[self sharedManager] POST:url parameters:@{@"homecare_homecarebundle_careoptiontimesheet":params} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)deleteCareOptionsFromTimesheet:(NSNumber *)timesheetID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/careoptions/%@/delete", kURL, timesheetID];
    [[self sharedManager] DELETE:url parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)verifyRecipientPassword:(NSString *)password andPCAID:(NSNumber *)pcaID andRecipientID:(NSNumber *)recipientID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/recipientpassword", kURL];
    [[self sharedManager] POST:url parameters:@{@"password":password, @"pcaId":pcaID, @"recipientId":recipientID} progress:nil success:^(NSURLSessionDataTask * _Nullable task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError *error) {
        failure(task, error);
    }];
}

+(void)createPCASignatureTableWithParams:(NSDictionary *)params andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/create/pca/signature", kURL];
    [[self sharedManager] POST:url parameters:@{@"homecare_homecarebundle_pcasignature":params} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)createRecipientSignatureTableWithParams:(NSDictionary *)params andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/create/recipient/signature", kURL];
    NSDictionary *parameters = @{@"homecare_homecarebundle_recipientsignature":params};
    [[self sharedManager] POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)finishSessionWithSessionID:(NSNumber *)sessionID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/finish/timesheets", kURL];
    [[self sharedManager] POST:url parameters:@{@"homecare_homecarebundle_timesheet":@{@"sessionId":sessionID}} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)deleteTimecardsFromSessionWithID:(NSNumber*)sessionID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@/delete/timesheets", kURL];
    NSDictionary *params = @{@"homecare_homecarebundle_timesheet":@{@"sessionId":sessionID}};
    [[self sharedManager] POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)addVerificatioinIDToTimesheetWithID:(NSNumber*)timesheetID andVerificationID:(NSNumber *)verificationID andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] PATCH:[NSString stringWithFormat:@"%@/timesheet/%@", kURL, timesheetID] parameters:@{@"homecare_homecarebundle_timesheet":@{@"verification":verificationID}} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(void)checkWorkerHoursForTheWeekWithWorkerID:(NSNumber *)workerID andEstimatedShiftLengthHours:(NSNumber *)hours andSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [[self sharedManager] GET:[NSString stringWithFormat:@"%@/pca/%@/hours", kURL, workerID] parameters:@{@"estimatedShiftLength":hours} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
    }];
}

+(NSString *)clientID {
    return kClientID;
}

+(void)setNetworkingHelperOAuthToken:(NSString *)token {
    NSLog(@"token get set? %@", token);
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
}

@end
