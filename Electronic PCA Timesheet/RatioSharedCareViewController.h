//
//  RatioSharedCareViewController.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 9/11/14.
//  Copyright (c) 2014 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Session.h"
#import "TimeInAndOutViewController.h"
#import "User.h"

@class Session;

@interface RatioSharedCareViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIScrollViewDelegate> {
    bool isIpad; bool four; int width; int height;
    UIPickerView *ratioPicker;
    UIPickerView *locationPicker;
    NSArray *ratioArray;
    NSArray *sharedLocationsArray;
    UITextField *ratioTextField;
    UITextField *locationTextField;
    long arrayRow;
    long arrayRowRatio;
    UIScrollView *scroller;
    UIButton *nextScreenButton;
}

@property (nonatomic, strong) Session *currentSession;
@property (nonatomic, strong) User *currentUser;

@end
