//
//  ActiveTimesheetsCollectionViewController.m
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 6/25/16.
//  Copyright © 2016 The Jed Mahonis Group, LLC. All rights reserved.
//

#import "ActiveTimesheetsCollectionViewController.h"
#import "ActiveTimesheetCollectionViewCell.h"
#import "PDFCollectionViewController.h"

@implementation ActiveTimesheetsCollectionViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Select Time Entry";
    _currentUser = [User currentUser];
    iPad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    width = self.view.frame.size.width; height = self.view.frame.size.height;
    self.collectionView.backgroundColor = [UIColor colorWithRed:0/255.0f green:105/255.0f blue:140/255.0f alpha:1.0];
    if ([_currentUser.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
        AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _locationManager = del.appLocationManager;
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [_locationManager startUpdatingLocation];
    }
    UIView *blueView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 90, self.view.frame.size.width, 90)];
    blueView.backgroundColor = self.collectionView.backgroundColor;
    [self.view addSubview:blueView];
    UIButton *sendAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sendAllButton setTitle:@"Sign All Time Entries" forState:UIControlStateNormal];
    [sendAllButton setTitleColor:self.collectionView.backgroundColor forState:UIControlStateNormal];
    [sendAllButton setBackgroundColor:[UIColor whiteColor]];
    [sendAllButton addTarget:self action:@selector(signAllButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    sendAllButton.frame = CGRectMake(20, 20, self.view.frame.size.width - 40, 50);
    sendAllButton.layer.cornerRadius = sendAllButton.frame.size.height/2;
    sendAllButton.clipsToBounds = YES;
    [blueView addSubview:sendAllButton];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _currentUser = [User currentUser];
    [self.collectionView reloadData];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return _currentUser.signableSessions.count;
    }
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ActiveTimesheetCollectionViewCell *cell = (ActiveTimesheetCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"timesheetCell" forIndexPath:indexPath];
        Session *session = _currentUser.signableSessions[indexPath.row];
        cell.currentSession = session;
        cell.tag = indexPath.row;
        [cell setValuesForUI];
        return cell;
    } else {
        return [collectionView dequeueReusableCellWithReuseIdentifier:@"signAllCell" forIndexPath:indexPath];
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return CGSizeMake(collectionView.frame.size.width - 16, 124);
    }
    return CGSizeMake(self.view.frame.size.width - 40, 50);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        PDFCollectionViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PDFCollectionViewController"];
        vc.startingIndex = indexPath.row;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self signAllButtonPressed:nil];
    }
}

#pragma mark - Location Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [manager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == 3 || status == 4 || status == 5) {
        if ([_currentUser.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
            [manager startUpdatingLocation];
        }
    }
}

#pragma mark - Button Actions

-(IBAction)signAllButtonPressed:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sign All Time Entries?" message:@"Are you sure you wish to sign all time entries?" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Sign All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self signAllTimesheets];
    }]];
    [self presentViewController:alert animated:YES completion:NULL];
}

-(void)signAllTimesheets {
    UIAlertController *proceedAlert = [UIAlertController alertControllerWithTitle:@"Proceed" message:@"Be sure your timecard is completed and correct before proceeding. Once you sign, you will have to send the timecard." preferredStyle:UIAlertControllerStyleAlert];
    [proceedAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [proceedAlert addAction:[UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self showFraudWarningFor:nil];
    }]];
    [self presentViewController:proceedAlert animated:YES completion:NULL];
}

-(void)showFraudWarningFor:(id)sender {
    _fraudView.frame = CGRectMake(0, [self topOfView], width, height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height);
    _fraudView.warningTV.text = [self stringForFraudWarningTextView:SignatureIsFor_Recipient];
    [self.view addSubview:_fraudView];
    NSLog(@"topOfView:%f, self.collectionView.contentOffset.y:%f", [self topOfView], self.collectionView.contentOffset.y);
    [UIView animateWithDuration:0.3 animations:^{
        _fraudView.alpha = 1.0;
        
        if (CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size, CGSizeMake(640, 960))) {
            _fraudView.warningTV.font = [UIFont systemFontOfSize:17.0];
        } else if (iPad) { _fraudView.warningTV.font = [UIFont systemFontOfSize:30.0]; }
        [_fraudView.warningTV sizeToFit];
        _fraudView.warningTV.center = CGPointMake(_fraudView.center.x, self.view.center.y - (_acceptSignatureButton.frame.size.height));
        self.title = @"Recipient/Responsible Party Signature";
    }];
}

-(NSString *)stringForFraudWarningTextView:(SignatureIsFor)signatureIsFor {
    NSString *warningTVText;
    if (signatureIsFor == SignatureIsFor_Recipient) {
        self.title = @"Recipient/Responsible Party Signature";
        if ([self string:_currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
            warningTVText = @"It is a federal crime to provide false information on PCA billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that the services were performed as specified in the PCA care plan. The time and activity document created with this app is transmitted via email. By using this app my health care provider may assume that e-mail communications are acceptable to me and that I am aware of the risks of using unencrypted e-mail.";
        } else {
            warningTVText = @"It is a federal crime to provide false information on billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that the services were performed as specified. The time and activity document created with this app is transmitted via email. By using this app my health care provider may assume that e-mail communications are acceptable to me and that I am aware of the risks of using unencrypted e-mail.";
        }
    } else {
        //        NSLog(@"self.serviceName:%@", self.currentSession.service.serviceName);
        self.title = @"PCA Signature";
        warningTVText = @"It is a federal crime to provide false information on PCA billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that the services were performed as specified in the PCA care plan.";
        if (![self string:_currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
            self.title = @"Worker Signature";
            warningTVText = @"It is a federal crime to provide false information on Homemaking billing for Medical Assistance payment. Your signature verifies the time and services entered are accurate and that services were performed as represented.";
        }
    }
    return warningTVText;
}

-(IBAction)cancelFW:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        _fraudView.alpha = 0.0;
        self.title = @"Review Timecard";
    }];
    
}

-(IBAction)acceptFWFor:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        _fraudView.alpha = 0.0;
    }];
    
    [self showSignatureView];
    
}

#pragma mark - signature methods

-(void)createTimeCardFile {
    if (!_currentSession.currentTimecard.file.idNumber) {
        [NetworkingHelper createFileAndSuccess:^(id responseObject) {
            //            NSLog(@"responseObject:%@", responseObject);
            if (!_currentSession.currentTimecard.file) {
                _currentSession.currentTimecard.file = [File new];
            }
            _currentSession.currentTimecard.file.idNumber = responseObject[@"createdFilesId"];
            [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
            //            NSLog(@"currentSession.currentTimecard.file.idNumber:%@", currentSession.currentTimecard.file.idNumber);
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"Error creating File:%@", error);
        }];
    }
}

-(float)topOfView {
//    return [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationController.navigationBar.frame.size.height;
//    if (self.collectionView.contentOffset.y >= 64) {
//        return self.collectionView.contentOffset.y - [UIApplication sharedApplication].statusBarFrame.size.height - 10;
//    }
    return 64.0;
}

-(void)showSignatureView {
    //x 98.5 y 20 4.925
    self.title = @"Recipient/Responsible Party Signature";
    
    _signatureContainerView.frame = CGRectMake(0, [self topOfView], width, height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height);
    [self.view addSubview:_signatureContainerView];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        //        sigView.frame = CGRectMake(297.5, 304, 429, 160);
        _iPadSignatureView.frame = CGRectMake(100, 150, width - 200, (((width - 200) * 26.5) / 131));
        _iPadSignatureView.alpha = 1.0;
        _iPadSignatureView.delegate = self;
        [_signatureContainerView addSubview:_iPadSignatureView];
        for (UIButton *button in _signatureContainerView.subviews) {
            if ([button isKindOfClass:[UIButton class]]) {
                button.titleLabel.font = [UIFont systemFontOfSize:24];
            }
        }
    } else {
        
        _signatureView.alpha = 1.0;
        _signatureView.delegate = self;
    }
    
    _signatureContainerView.alpha = 1.0;
    [self.collectionView setScrollEnabled:NO];
}

-(void)enableButton {
    [_acceptSignatureButton setTitleColor:[UIColor colorWithRed:80/255.0f green:116/255.0f blue:187/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_acceptSignatureButton setEnabled:YES];
}

-(IBAction)acceptSignature:(id)sender {
    self.navigationItem.leftBarButtonItem = nil;
    _sigViewSubViewsContainerView.alpha = 0.0;
    _iPadSigViewSubViewsContainerView.alpha = 0.0;
    _currentSession.pdfState = PDFState_SignatureApplied;
    UIImage *finalImage = [self getFinalImageFromSignatureView];
    _recipientSignatureForAllTimesheets = UIImagePNGRepresentation(finalImage);
    [self clearViews];
    [self sendNextTimesheet];
}

-(void)sendNextTimesheet {
    self.collectionView.userInteractionEnabled = NO;
    if (_currentUser.signableSessions.count > 0) {
//        [self.collectionView reloadData];
        _currentSession = _currentUser.signableSessions[0];
        _currentUser.currentSession = _currentSession;
        [self sendRecipientSignatureToServer];
    } else {
        self.collectionView.userInteractionEnabled = YES;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(IBAction)clearSignature {
    [self clearViews];
    [self acceptSignature:nil];
}

-(IBAction)deleteSignature {
    self.title = @"Review Time Entry";
    [self clearViews];
}

-(void)clearViews {
    _signatureContainerView.alpha = 0.0;
    [self.collectionView setScrollEnabled:YES];
}

-(void)sendRecipientSignatureToServer {
    if (!_currentSession.currentTimecard.recipientSignature) {
        _currentSession.currentTimecard.recipientSignature = [RecipientSignature new];
    }
    _currentSession.currentTimecard.recipientSignature.signatureImageData = _recipientSignatureForAllTimesheets;
    
    [NetworkingHelper saveFileWithFileIDNumber:_currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetRecSig", @"fileName":@"timesheetRecSig.png", @"fileData":_currentSession.currentTimecard.recipientSignature.signatureImageData, @"mimeType":@"image/png", @"timesheet":_currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
        //            NSLog(@"responseObject for saving recipient signature file:%@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"failed to save recipient signature image:%@", error);
    }];
    _currentSession.currentTimecard.recipientSignature.timeStamp = [NSDate date];
    _currentSession.currentTimecard.recipientSignature.wasSigned = @YES;
    [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
    if ([_currentUser.currentSession.currentTimecard.recipient.shouldRecordLocation isEqualToNumber:@YES]) {
        CLGeocoder *geoCoder = [CLGeocoder new];
        [geoCoder reverseGeocodeLocation:_locationManager.location completionHandler:^(NSArray *placemarks, NSError *error) {
            CLPlacemark *placeMark = placemarks[0];
            _currentSession.currentTimecard.recipientSignature.address = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", [placeMark subThoroughfare], [placeMark thoroughfare], [placeMark locality], [placeMark administrativeArea], [placeMark postalCode]];
            [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
            [self createRecipientSignatureTable];
    
//        NSLog(@"params:%@", @{@"timesheet":_currentSession.currentTimecard.idNumber, @"recipientSigTime":_currentSession.currentTimecard.recipientSignature.timeStamp, @"recipientSigAddress": _currentSession.currentTimecard.recipientSignature.address, @"latitude":[NSNumber numberWithDouble:_locationManager.location.coordinate.latitude], @"longitude":[NSNumber numberWithDouble:_locationManager.location.coordinate.longitude], @"recipientPhotoTime":[NSDate date]});
        
        }];
    } else {
        _currentSession.currentTimecard.recipientSignature.address = @"";
        _currentSession.currentTimecard.recipientSignature.lat = @0.0;
        _currentSession.currentTimecard.recipientSignature.lng = @0.0;
        [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
        [self createRecipientSignatureTable];
    }
}

-(void)createRecipientSignatureTable {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDateFormat];
    NSString *rPhotoTime = [format stringFromDate:[NSDate date]];
    
    [NetworkingHelper createRecipientSignatureTableWithParams:@{@"timesheet":_currentSession.currentTimecard.idNumber, @"recipientSigTime":rPhotoTime, @"recipientSigAddress": _currentSession.currentTimecard.recipientSignature.address, @"latitude":[NSNumber numberWithDouble:_locationManager.location.coordinate.latitude], @"longitude":[NSNumber numberWithDouble:_locationManager.location.coordinate.longitude], @"recipientPhotoTime":rPhotoTime} andSuccess:^(id responseObject) {
        // NSLog(@"success createing recipient signature:%@", responseObject);
        [self sendTimesheetAndFinish:nil];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSInteger statusCode = 0;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        
        if ([httpResponse isKindOfClass:[NSHTTPURLResponse class]]) {
            statusCode = httpResponse.statusCode;
            NSLog(@"failed to create recipient signature:%@ code%lu", error, (long)statusCode);
            if (statusCode == 409) {
                NSLog(@"should continue");
                [self sendTimesheetAndFinish:nil];
            } else {
                self.collectionView.userInteractionEnabled = YES;
            }
        }
       
    }];
}

-(IBAction)sendTimesheetAndFinish:(id)sender {
    LoadingView *lv = [LoadingView addLoadingViewWithText:@"Saving"];
    //    NSLog(@"currentSession b4 sennding:%@", _currentSession.currentTimecard);
    
    [NetworkingHelper saveFileWithFileIDNumber:_currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetPdfFile", @"fileName":@"timesheetPdfFile.pdf", @"fileData":[self generatePDF], @"mimeType":@"application/pdf", @"timesheet":_currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
        [NetworkingHelper saveFileWithFileIDNumber:_currentSession.currentTimecard.file.idNumber andParams:@{@"file":@"timesheetCsvFile", @"fileName":@"timesheetCsvFile.csv", @"fileData":csvDataString, @"mimeType":@"text/csv", @"timesheet":_currentSession.currentTimecard.idNumber} andsuccess:^(id responseObject) {
            [NetworkingHelper finishSessionWithSessionID:_currentSession.idNumber andSuccess:^(id responseObject) {
                
                [lv removeFromSuperview];
                [self finishSessionAndShouldRemoveSession:YES session:_currentSession];
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
                self.collectionView.userInteractionEnabled = YES;
                [lv removeFromSuperview];
                id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
                [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"%@", responseObject[@"error"]]] animated:YES completion:NULL];

//                [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Time entry was not successfully saved. Check your network connection and try again."] animated:YES completion:NULL];
            }];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            self.collectionView.userInteractionEnabled = YES;
            id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
            [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"%@", responseObject[@"error"]]] animated:YES completion:NULL];

//            [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Time entry was not successfully saved. Check your network connection and try again."] animated:YES completion:NULL];
            [lv removeFromSuperview];
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        self.collectionView.userInteractionEnabled = YES;
        [lv removeFromSuperview];
        id responseObject = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
        [self presentViewController:[ErrorAlertController alertControllerWithMessage:[NSString stringWithFormat:@"%@", responseObject[@"error"]]] animated:YES completion:NULL];

//        [self presentViewController:[ErrorAlertController alertControllerWithMessage:@"Time entry was not successfully saved. Check your network connection and try again."] animated:YES completion:NULL];
    }];
}

-(void)finishSessionAndShouldRemoveSession:(BOOL)removeSession session:(Session *)session {
    [_locationManager stopUpdatingLocation];
    if (removeSession == YES) {
        [_currentUser removeSessionWithID:session.idNumber];
    } else {
        [_currentUser overWriteSessionWithID:session.idNumber withSession:session];
    }
    _currentUser = [User currentUser];
    if (_currentUser.signableSessions.count == 0) {
        self.collectionView.userInteractionEnabled = YES;
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
//        [self.collectionView reloadData];
        [self sendNextTimesheet];
    }
    
}

#pragma mark - UIImage Processing (For Signature Screenshot)

-(UIImage *)getFinalImageFromSignatureView {
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIView *viewToUse = _signatureView;
    if (iPad) {
        viewToUse = _iPadSignatureView;
    }
    [viewToUse.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
//    CGRect rect = [viewToUse convertRect:viewToUse.frame toView:self.view];
    CGRect rect = viewToUse.frame;
    rect.origin.x = rect.origin.x + 1;
    rect.origin.y = rect.origin.y + 6 + 64;
    if (iPad) {
        rect.origin.y = rect.origin.y;
    }
    rect.size.height = rect.size.height - 12;
    rect.size.width = rect.size.width - 2;
    UIImage *secondImage = [self processImage:image];
    return [self cropImage:secondImage rect:rect];
}

-(UIImage *)cropImage:(UIImage *)image rect:(CGRect)cropRect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return img;
}

-(UIImage *)rotateImage:(UIImage *)image {
    return [[UIImage alloc] initWithCGImage:image.CGImage scale:1.0 orientation:UIImageOrientationRight];
}

- (UIImage*) processImage :(UIImage*) inputImage
{
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, 1.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRef rawImageRef = viewImage.CGImage;
    const CGFloat colorMasking[6] = {255.0f, 255.0f, 255.0f, 255.0f, 255.0f, 255.0f};
    UIGraphicsBeginImageContext(viewImage.size);
    CGImageRef maskedImageRef = CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, viewImage.size.height);
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, viewImage.size.width, viewImage.size.height), maskedImageRef);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - PDF Methods

-(BOOL)string:(NSString *)urlSchemeString containsCaseInsensitiveString:(NSString *)string {
    if ([urlSchemeString rangeOfString:string options:NSCaseInsensitiveSearch].location == NSNotFound)
    {
        //        NSLog(@"return NO");
        return NO;
    }
    return YES;
}

-(NSData *)generatePDF {
    
    //    NSLog(@"currentSession:%@", currentSession);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filename = [NSString stringWithFormat:@"Time entry for %@ %@.pdf", _currentUser.currentSession.pca.firstName, _currentUser.currentSession.pca.lastName];
    
    filename = [self _sanitizeFileNameString:filename];
    
    NSString *newDocumentPath = [documentsDirectory stringByAppendingPathComponent:filename];
    NSString *templatePath = [[NSBundle mainBundle] pathForResource:[self pdfNameForService:_currentSession.service.serviceName] ofType:@"pdf"];
    
    if ([fileManager fileExistsAtPath:newDocumentPath]) {
        [[NSFileManager defaultManager] removeItemAtPath: newDocumentPath error: &error];
    }
    
    [fileManager copyItemAtPath:templatePath toPath:newDocumentPath error:&error];
    
    NSURL *documentURL = [NSURL fileURLWithPath:newDocumentPath];
    
    CGPDFDocumentRef ourPDF = CGPDFDocumentCreateWithURL((CFURLRef)documentURL);
    
    
    // const size_t numberOfPages = CGPDFDocumentGetNumberOfPages(ourPDF);
    NSMutableData* data = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(data, CGRectZero, nil);
    
    
    CGPDFPageRef pdfPage = CGPDFDocumentGetPage(ourPDF, 1);
    const CGRect pageFrame = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
    
    UIGraphicsBeginPDFPageWithInfo(pageFrame, nil);
    
    // Draw the page (flipped)
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -pageFrame.size.height);
    CGContextDrawPDFPage(ctx, pdfPage);
    CGContextRestoreGState(ctx);
    
    //Top of the PDF ================================================================================
    
    
    
    NSString *checkMarkString = @"\u2713";
//    [[UIColor blackColor] set];
//    if ([self string:_currentSession.service.serviceName containsCaseInsensitiveString:@"Personal Care Service"]) {
//        [checkMarkString drawAtPoint:CGPointMake(182, 92) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:18]}];
//    } else if ([self string:_currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
//        [checkMarkString drawAtPoint:CGPointMake(328, 92) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:18]}];
//    }
    
    
    [_currentUser.agency.name drawAtPoint:CGPointMake(120, 122) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    [_currentUser.agency.phone drawAtPoint:CGPointMake(442, 122) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    
    //Change back to above if we make this more widely available.
    
    if (_currentSession.dateEnding) {
        //        NSLog(@"pdf dateEnding:%@", currentSession.dateEnding);
        [[self dateEndingString] drawAtPoint:CGPointMake(208, 160) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:16]}];
    }
    
    
    // Activities Part of the PDF
    
    
    long activitiesCount = _currentUser.possibleCareOptions.count;
    NSMutableArray *dayActivitiesArray = [NSMutableArray arrayWithCapacity:activitiesCount];
    for (int i = 0; i < activitiesCount; i ++) {
        
        CareOption *possibleOption = (CareOption *)_currentUser.possibleCareOptions[i];
        if (_currentSession.currentTimecard.careOptionsArray.count > 0) {
            long lastIndex = _currentSession.currentTimecard.careOptionsArray.count - 1;
            for (int j = 0; j < _currentSession.currentTimecard.careOptionsArray.count; j++) {
                NSNumber *optionID = _currentSession.currentTimecard.careOptionsArray[j];
                if ([possibleOption.idNumber isEqualToNumber:optionID]) {
                    [dayActivitiesArray insertObject:@"1" atIndex:i];
                    if (i < 10) {
                        long x = 264;
                        if ([self string:_currentSession.service.serviceName containsCaseInsensitiveString:@"Homemaking"]) {
                            x = 498;
                        }
                        int y = 206 + (28.1 * i);
                        [checkMarkString drawAtPoint:CGPointMake(x, y) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:32]}];
                    } else {
                        int y = 206 + (28.1 * (i - 10));
                        [checkMarkString drawAtPoint:CGPointMake(498, y) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:32]}];
                    }
                    break;
                } else {
                    if (j == lastIndex) {
                        [dayActivitiesArray insertObject:@"0" atIndex:i];
                    }
                }
            }
        } else {
            [dayActivitiesArray insertObject:@"0" atIndex:i];
        }
    }
    
    // Total Hours
    NSDateFormatter *shortFormatter = [[NSDateFormatter alloc] init];
    [shortFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc] init];
    [shortDateFormatter setDateStyle:NSDateFormatterShortStyle];
    [shortDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSDateFormatter *medFormatter = [[NSDateFormatter alloc] init];
    [medFormatter setTimeStyle:NSDateFormatterMediumStyle];
    NSString *timeInString;
    if (_currentSession.timeIn.timeIn) {
        timeInString = [shortFormatter stringFromDate:_currentSession.timeIn.timeIn];
        //        NSLog(@"timeInString:%@", timeInString);
        bool isAm;
        NSArray *stringAr = [timeInString componentsSeparatedByString:@" "];
        NSString *one = [stringAr objectAtIndex:0];
        [one drawAtPoint:CGPointMake(454, 404.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:20]}];
        if (stringAr.count > 1) {
            NSString *two = [stringAr objectAtIndex:1];
            if ([two isEqualToString:@"AM"]) {
                isAm = YES;
            } else {
                isAm = NO;
            }
            
            CGRect circleRect;
            if (isAm) {
                circleRect = CGRectMake(511.2, 403, 22, 10);
            } else {
                circleRect = CGRectMake(512, 414.4, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        } else {
            NSArray *timeStringArray = [one componentsSeparatedByString:@":"];
            //            NSLog(@"timeStringArray[0]:%@", timeStringArray[0]);
            if ([timeStringArray[0] longLongValue] < 12) {
                isAm = YES;
            } else {
                isAm = NO;
            }
            CGRect circleRect;
            if (isAm) {
                circleRect = CGRectMake(511.2, 403, 22, 11);
            } else {
                circleRect = CGRectMake(512, 414.4, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        }
    } else {
        timeInString = @"na";
    }
    
    NSString *timeOutString;
    if (_currentSession.timeOut.timeOut) {
        //        NSDate *convertDate = [medFormatter dateFromString:currentSession.timeOut.timeOut];
        timeOutString = [shortFormatter stringFromDate:_currentSession.timeOut.timeOut];
        bool isAM;
        NSArray *stringAr = [timeOutString componentsSeparatedByString:@" "];
        NSString *one = [stringAr objectAtIndex:0];
        [one drawAtPoint:CGPointMake(454, 432) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:20]}];
        if (stringAr.count > 1) {
            NSString *two = [stringAr objectAtIndex:1];
            if ([two isEqualToString:@"AM"]) {
                isAM = YES;
            } else {
                isAM = NO;
            }
            
            CGRect circleRect;
            if (isAM) {
                circleRect = CGRectMake(511.2, 430, 22, 11);
            } else {
                circleRect = CGRectMake(512, 441.5, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        } else {
            NSArray *timeStringArray = [one componentsSeparatedByString:@":"];
            if ([timeStringArray[0] longLongValue] < 12) {
                isAM = YES;
            } else {
                isAM = NO;
            }
            CGRect circleRect;
            if (isAM) {
                circleRect = CGRectMake(511.2, 430, 22, 11);
            } else {
                circleRect = CGRectMake(512, 441.5, 20, 10);
            }
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:circleRect];
            [path stroke];
        }
    } else {
        timeOutString = @"na";
    }
    
    CGRect dayCircle;
    if ([_currentSession.ratio.ratio isEqualToString:@"1:1"]) {
        dayCircle = CGRectMake(439.5, 336, 28, 26);
    } else if ([_currentSession.ratio.ratio isEqualToString:@"1:2"]) {
        dayCircle = CGRectMake(476, 336, 28, 26);
    } else if ([_currentSession.ratio.ratio isEqualToString:@"1:3"]) {
        dayCircle = CGRectMake(507, 336, 28, 26);
    } else {
        dayCircle = CGRectZero;
    }
    
    UIBezierPath *monPath = [UIBezierPath bezierPathWithOvalInRect:dayCircle];
    [[UIColor blackColor] setStroke];
    [monPath stroke];
    
    if (![timeInString isEqualToString:@"na"]) {
        //        currentTimecard.sharedCareLocation = @"Community";
        if ([self string:_currentSession.careLocation.sharedCareLocation containsCaseInsensitiveString:@"Home"]) {
            [_currentSession.careLocation.sharedCareLocation drawAtPoint:CGPointMake(454, 370) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:24]}];
        } else if ([self string:_currentSession.careLocation.sharedCareLocation containsCaseInsensitiveString:@"Community"]) {
            [_currentSession.careLocation.sharedCareLocation drawAtPoint:CGPointMake(442.25, 376) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:15]}];
        } else {
            [_currentSession.careLocation.sharedCareLocation drawAtPoint:CGPointMake(467, 370) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:24]}];
        }
        
    }
    
    NSString *totalHoursString = [self convertSecondsToString:_currentSession.timeOut.billableHours];
    [totalHoursString drawAtPoint:CGPointMake(450, 465.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:20]}];
    
    //GPS Locations
    
    if (_currentSession.timeIn.address) {
        [_currentSession.timeIn.address drawAtPoint:CGPointMake(135, 554.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    }
    
    if (_currentSession.timeOut.address) {
        [_currentSession.timeOut.address drawAtPoint:CGPointMake(149, 588.5) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    }
    
    //Bottom of the PDF
    
    if (_currentSession.currentTimecard.recipient.firstName || _currentSession.currentTimecard.recipient.lastName) {
        NSString *fullRecName = [NSString stringWithFormat:@"%@ %@",_currentSession.currentTimecard.recipient.firstName, _currentSession.currentTimecard.recipient.lastName];
        [fullRecName drawAtPoint:CGPointMake(70, 680) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:15]}];
    }
    if (_currentSession.currentTimecard.recipient.maNumber) {
        [_currentSession.currentTimecard.recipient.maNumber drawAtPoint:CGPointMake(238, 680) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:16]}];
    }
    if (_currentUser.currentSession.pca.firstName || _currentUser.currentSession.pca.lastName) {
        NSString *fullPCAName = [NSString stringWithFormat:@"%@ %@", _currentUser.currentSession.pca.firstName, _currentUser.currentSession.pca.lastName];
        [fullPCAName drawAtPoint:CGPointMake(70, 708) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:15]}];
    }
    if (_currentUser.currentSession.pca.umpi) {
        [_currentUser.currentSession.pca.umpi drawAtPoint:CGPointMake(233, 710) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:12]}];
    }
    if (_currentSession.currentTimecard.file.recipientSignature && !_currentSession.currentTimecard.recipientSignature.signatureImageData) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", _currentSession.fileURL, _currentSession.currentTimecard.file.recipientSignature]];
        _currentSession.currentTimecard.recipientSignature.signatureImageData = [NSData dataWithContentsOfURL:url];
        [_currentUser overWriteSessionWithID:_currentSession.idNumber withSession:_currentSession];
    }
    
    if (_currentSession.currentTimecard.recipientSignature.signatureImageData) {
        UIImage *image = [UIImage imageWithData:_currentSession.currentTimecard.recipientSignature.signatureImageData];
        if (!iPad) {
            image = [self rotateImage:image];
            [image drawInRect:CGRectMake(339, 672, 131, 26.5)];
        } else {
            [image drawInRect:CGRectMake(339, 674, 131, 26.5)];
        }
        
        if (_currentSession.currentTimecard.recipientSignature.timeStamp) {
            NSString *timeStampString = [shortDateFormatter stringFromDate:_currentSession.currentTimecard.recipientSignature.timeStamp];
            [timeStampString drawAtPoint:CGPointMake(487, 680) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:13]}];
        }
    }
    
    //    NSLog(@"currentSession.currentTimecard.file.pcaSignature:%@", currentSession.currentTimecard.file.pcaSignature);
    if (_currentSession.currentTimecard.file.pcaSignature && !_currentSession.currentTimecard.pcaSignature.signatureImageData) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", _currentSession.fileURL, _currentSession.currentTimecard.file.pcaSignature]];
        _currentSession.currentTimecard.pcaSignature.signatureImageData = [NSData dataWithContentsOfURL:url];
    }
    if (_currentSession.currentTimecard.pcaSignature.signatureImageData) {
        UIImage *image = [UIImage imageWithData:_currentSession.currentTimecard.pcaSignature.signatureImageData];
        if (!iPad) {
            image = [self rotateImage:image];
            [image drawInRect:CGRectMake(339, 705, 131, 26.5)];
        } else {
            [image drawInRect:CGRectMake(339, 702.5, 131, 26.5)];
        }
        if (_currentSession.currentTimecard.pcaSignature.timeStamp) {
            NSString *timeStampString = [shortDateFormatter stringFromDate:_currentSession.currentTimecard.pcaSignature.timeStamp];
            [timeStampString drawAtPoint:CGPointMake(487, 710) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPSMT" size:13]}];
        }
    }
    
    
    
    
    //Create CSV File from currentTimecard Vars - WeekendingDate, dates, activities, total time on date, total time week, names, dob, date signed.
    
    
    // @"inpatient_incarcerated", @"start_date_inpatient_incarcerated", @"end_date_inpatient_incarcerated",
    NSArray *fieldsArray = [NSArray arrayWithObjects:@"state", @"service", @"agency_name", @"phone_number", @"day_of_service", @"date_of_service", @"recipient_first_name", @"recipient_last_name", @"recipient_ma_number", @"recipient_company_assigned_id", @"recipient_date_signed", @"pca_first_name", @"pca_last_name", @"pca_npi_umpi", @"pca_company_assigned_id", @"pca_date_signed", @"dressing", @"grooming", @"bathing", @"eating", @"transfers", @"mobility", @"positioning", @"toileting", @"health_related", @"behavior", @"light_housekeeping", @"laundry", @"other", @"visit_one_ratio", @"visit_one_location", @"visit_one_time_in", @"visit_one_time_in_address", @"visit_one_time_out", @"visit_one_time_out_address", @"recipient_signature_image_timestamp", @"pca_signature_image_timestamp", @"daily_total", nil];
    
    NSMutableString *csvString = [[NSMutableString alloc] init];
    [csvString appendString:[fieldsArray componentsJoinedByString:@","]];
    [csvString appendString:@"\n"];
    
    _currentUser.agency.state = @"Minnesota";
    NSString *pcaDateString;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    if (_currentSession.currentTimecard.pcaSignature.timeStamp) {
        pcaDateString = [dateFormat stringFromDate:_currentSession.currentTimecard.pcaSignature.timeStamp];
    } else {
        pcaDateString = @"na";
    }
    NSString *recipDateString;
    if (_currentSession.currentTimecard.recipientSignature.timeStamp) {
        recipDateString = [dateFormat stringFromDate:_currentSession.currentTimecard.recipientSignature.timeStamp];
    } else {
        recipDateString = @"na";
    }
    
    if (!_currentSession.currentTimecard.recipientSignature.address) {
        _currentSession.currentTimecard.recipientSignature.address = @"n/a";
    }
    if (!_currentSession.currentTimecard.pcaSignature.address) {
        _currentSession.currentTimecard.pcaSignature.address = @"n/a";
    }
    
    if (!_currentSession.timeIn.address) {
        _currentSession.timeIn.address = @"na";
        
    }
    
    if (!_currentSession.timeOut.address) {
        _currentSession.timeOut.address = @"na";
    }
    if (dayActivitiesArray.count != 13) {
        dayActivitiesArray = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", @"0", nil];
    }
    [csvString appendString:[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@\n", _currentUser.agency.state, _currentSession.service.serviceName, _currentUser.agency.name, _currentUser.agency.phone, [self weekday], _currentSession.dateEnding, _currentSession.currentTimecard.recipient.firstName, _currentSession.currentTimecard.recipient.lastName, _currentSession.currentTimecard.recipient.maNumber, _currentSession.currentTimecard.recipient.companyAssignedID, recipDateString, _currentUser.currentSession.pca.firstName, _currentUser.currentSession.pca.lastName, _currentUser.currentSession.pca.umpi, _currentUser.currentSession.pca.companyAssignedID, pcaDateString, dayActivitiesArray[0], dayActivitiesArray[1], dayActivitiesArray[2], dayActivitiesArray[3], dayActivitiesArray[4], dayActivitiesArray[5], dayActivitiesArray[6], dayActivitiesArray[7], dayActivitiesArray[8], dayActivitiesArray[9], dayActivitiesArray[10], dayActivitiesArray[11], dayActivitiesArray[12], _currentSession.ratio.ratio, _currentSession.careLocation.sharedCareLocation, timeInString, _currentSession.timeIn.address, timeOutString, _currentSession.timeOut.address, _currentSession.currentTimecard.recipientSignature.timeStamp, _currentSession.currentTimecard.pcaSignature.timeStamp, [NSString stringWithFormat:@"%@", totalHoursString]]];
    csvDataString = [csvString dataUsingEncoding:NSUTF8StringEncoding];
    
    
    UIGraphicsEndPDFContext();
    
    CGPDFDocumentRelease(ourPDF);
    ourPDF = nil;
    [data writeToURL:documentURL atomically:YES];
    return data;
}

-(NSString *)weekday {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateStyle:NSDateFormatterShortStyle];
    
    NSDate *dateEnding = [format dateFromString:_currentSession.dateEnding];
    [format setDateFormat:@"EEEE"];
    return [format stringFromDate:dateEnding];
    
}

-(NSString *)pdfNameForService:(NSString *)service
{
    if ([self string:service containsCaseInsensitiveString:@"Personal Care Service"]) {
        return @"PCA";
    } else if ([self string:service containsCaseInsensitiveString:@"Homemaking"]) {
        return @"Homemaking";
    } else if ([self string:service containsCaseInsensitiveString:@"Respite"]) {
        return @"Respite";
    } else if ([self string:service containsCaseInsensitiveString:@"Personal Assistance"]) {
        return @"Personal Assistance";
    } else if ([self string:service containsCaseInsensitiveString:@"Personal Support"]) {
        return @"Personal Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Self-Direction Support"]) {
        return @"Self-Direction Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Treatment and Training"]) {
        return @"Treatment and Training";
    } else if ([self string:service containsCaseInsensitiveString:@"Caregiving Expense"]) {
        return @"Caregiving Expense";
    } else if ([self string:service containsCaseInsensitiveString:@"Consumer Support"]) {
        return @"Consumer Support";
    } else if ([self string:service containsCaseInsensitiveString:@"Environmental Modifications"]) {
        return @"Environmental Modifications";
    }
    return @"Time and Activity 12.2.15";
}

- (NSString *)_sanitizeFileNameString:(NSString *)fileName {
    // Mad props to http://stackoverflow.com/questions/1281576/how-to-make-an-nsstring-path-file-name-safe
    
    NSCharacterSet* illegalFileNameCharacters = [NSCharacterSet characterSetWithCharactersInString:@"/\\?%*|\"<>"];
    return [[fileName componentsSeparatedByCharactersInSet:illegalFileNameCharacters] componentsJoinedByString:@""];
}

-(NSString *)convertSecondsToString:(NSNumber *)kseconds {
    long hours, minutes, seconds;
    long accumulatedSeconds = [kseconds doubleValue];
    hours = accumulatedSeconds/3600;
    minutes = (accumulatedSeconds % 3600) / 60;
    seconds = (accumulatedSeconds % 3600) % 60;
    return [NSString stringWithFormat:@"%2ld:%02ld:%02ld", hours, minutes, seconds];
}

-(NSString *)dateEndingString {
    //        NSLog(@"pdf dateEnding:%@", currentSession.dateEnding);
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    NSDate *stringDate = [dateformat dateFromString:_currentSession.dateEnding];
    //        NSLog(@"dateEnding:%@", currentSession.dateEnding);
    
    //        NSLog(@"stringDate:%@", stringDate);
    NSDateFormatter *shortFormat = [NSDateFormatter new];
    [shortFormat setDateStyle:NSDateFormatterShortStyle];
    [shortFormat setTimeStyle:NSDateFormatterNoStyle];
    NSString *dateString = [shortFormat stringFromDate:stringDate];
    //        NSLog(@"dateString:%@", dateString);
    if (!stringDate) {
        return _currentSession.dateEnding;
    } else {
        return dateString;
    }
    
}

@end
