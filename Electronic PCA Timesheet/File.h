//
//  File.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/30/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface File : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSNumber *idNumber;
@property (nonatomic, strong) NSString *pdf;
@property (nonatomic, strong) NSString *csv;
@property (nonatomic, strong) NSString *recipientSignature;
@property (nonatomic, strong) NSString *pcaSignature;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
