//
//  Agency.h
//  Homecare Timesheet Demo
//
//  Created by Scott Mahonis on 3/16/15.
//  Copyright (c) 2015 The Jed Mahonis Group, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Agency : NSObject <NSCopying, NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSNumber *IDNumber;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSArray *servicesArray;
@property (nonatomic, strong) NSNumber *stateIdNumber;
@property (nonatomic, strong) NSURL *logoURL;


-(instancetype)initWithDictionary:(NSDictionary *)dictionary;


@end
